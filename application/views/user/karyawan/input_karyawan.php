<div id="simpan" class="flash-data" data-flashdata="<?= $this->session->flashdata('flash')?>"></div>
<div id="update" class="flash-data" data-flashdata="<?= $this->session->flashdata('flash')?>"></div>
<div class="page-body">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-block">
					<h4 class="sub-title">Input Data Karyawan</h4>
					<form id="formact" action="<?= base_url()?>user/c_karyawan/simpan" method="POST" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Kode Karyawan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="kode_karyawan" class="form-control" placeholder="Kode Karyawan" required>
                                        <input type="hidden" name="id">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Nama Karyawan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="tempat_lahir" class="form-control" placeholder="Kota Kelahiran" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-8">
                                        <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Jabatan</label>
                                    <div class="col-sm-8">
                                        <select name="jabatan" class="form-control" required>
                                            <option value="">--Pilih Jabatan--</option>
                                            <?php foreach ($dataJab as $row) {?>
                                            <option value="<?=$row->id;?>"><?=$row->jabatan;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="col-form-label">Alamat</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" cols="5" rows="5" required></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="col-form-label">No Hp</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" name="no_hp" class="form-control" placeholder="No Handphone" required>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-4">
                                <div class="mb-3">
                                    <img src="<?=base_url();?>assets/images/blank.png" id="preview" width="200" alt="Preview Gambar" class="rounded mx-auto d-block img-thumbnail">
                                </div>
                                <div>
                                    <input type="file" class="form-control" accept="image/*" onchange="tampilkanPreview(this,'preview')" name="gambar"/>
                                    <input type="text" name="hidden_foto" id="hidden_foto" hidden>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-sm btn-info" name="btn_simpan" id="btn_simpan"><i class="fa fa-save"></i>SIMPAN</button>
                                <button type="submit" class="btn btn-sm btn-warning" name="btn_update" id="btn_update"><i class="fa fa-edit"></i>UPDATE</button>
                                <button type="button" class="btn btn-sm btn-success" name="btn_reset" id="btn_reset"><i class="fa fa-refresh"></i>BATAL</button>
                            </div>
                        </div>

					</form>

					<div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap dataTable">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Kode Karyawan</th>
                            <th>Nama</th>
                            <th>Kota Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Jabatan</th>
                            <th>Alamat</th>
                            <th>No Hp</th>
                            <th>Foto</th>
                            <th style="text-align:center; width:200px;">AKSI</th>
                            </tr>
                        </thead>
                        <tbody id="show_data">
                            <?php $no = 1;foreach ($viewData as $row) {?>
                            <tr>
                            <td><?=$no++?></td>
                            <td><?=$row->kode_karyawan;?></td>
                            <td><?=$row->nama;?></td>
                            <td><?=$row->tempat_lahir;?></td>
                            <td><?=$row->tanggal_lahir;?></td>
                            <td><?=$row->id_jabatan;?></td>
                            <td><?=$row->alamat;?></td>
                            <td><?=$row->no_hp;?></td>
                            <td><img src="<?=base_url();?>assets\images\karyawan\<?=$row->foto;?>" width="50" alt="Preview Gambar" class="img-thumbnail"></td>
                            <td style="text-align:center; width:200px;">
                                <button id="btn_getEdit" class="btn btn-sm btn-warning btn-xs item_edit" data="<?=$row->id;?>"><i class="fa fa-edit"></i>EDIT</button>
                                <!-- <button id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" data="<?=$row->id;?>"><i class="fa fa-edit"></i>HAPUS</button> -->
                                <a href="<?= site_url('user/c_karyawan/hapusData/').$row->id ?>" id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');"><i class="fa fa-trash"></i>HAPUS</a>
                            </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        </table>
                    </div> <!-- END DIV TABLE -->

				</div> <!-- END DIV CARD BLOCK -->
			</div> <!-- END DIV CARD -->
		</div> <!-- END DIV COL-SM-12 -->
	</div> <!-- END DIV ROW -->
</div> <!-- END DIV PAGE BODY -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/app-scripts/user/karyawan.js"></script>