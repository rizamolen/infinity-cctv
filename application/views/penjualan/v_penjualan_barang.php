<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                <h4 class="sub-title"><i class="fa fa-shopping-cart"></i> Input Penjualan Barang</h4>
                    <form>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Faktur</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="no_faktur" id="no_faktur" class="form-control" readonly value="<?= $no_faktur;?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Tanggal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="date" id="tanggal_beli" name="tanggal_beli" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Customer</label>
                                </div>
                                <div class="col-sm-8">
                                <select id="id_customer" name="id_customer" class="form-control">
                                <option value=" ">Pilih Customer</option>
                                    <?php foreach($pilihan_customer as $row) {?>
                                    <option value="<?= $row->id_customer;?>"><?= $row->nama_customer;?></option>
                                    <?php }?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Jenis Customer</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="jenis_customer" name="jenis_customer" class="form-control"readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Telpon</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="no_telpon" name="no_telpon" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Alamat</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control" readonly></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="">Pilih Barang</label>
                                </div>
                                <div class="col-sm-9">
                                    <select  class="form-control" id="pilih_barang" name="pilih_barang">
                                        <option value=" ">Pilih Barang</option>
                                        <?php foreach($pilihan_barang as $row) {?>
                                        <option value="<?= $row->id_barang;?>"><?= $row->id_barang;?> - <?= $row->nama_barang;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <div class="modal fade" id="Modal-Barang" tabindex="-1" role="dialog">
                                        <div class="modal-dialog col-sm-8" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Barang</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Kode Barang</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="id_barang" name="id_barang" hidden>
                                                                    <input type="text" id="kode_barang" name="kode_barang" class="form-control" readonly >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Nama Barang</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Satuan Beli</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="satuan_beli" id="satuan_beli" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Harga satuan (Rp.)</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number_format" class="form-control" name="harga_satuan" readonly placeholder="0" autocomplete="off" id="input-rupiah">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                    <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Qty</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number" id="qty" name="qty" class="form-control reset" autocomplete="off" placeholder="0">
                                                                <!-- <input type="number" id="get_nila_satuan" name="get_nila_satuan" class="form-control reset" autocomplete="off" placeholder="0"> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Disc(%)</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number"  id="discon" name="discon" class="form-control reset" autocomplete="off" placeholder="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                        <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Detail</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <textarea cols="30" rows="5" class="form-control" name="detail_barang" id="detail_barang" readonly></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                        <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Total (Rp.)</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="text" style="font-size:20px; font-weight:bold;" class="form-control" id="total" name="total" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Gambar Barang</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="text" class="form-control" onchange="tampilkanPreview(this,'preview')" name="filefoto"/>
                                                                <img src="" id="preview" width="200" alt="Preview Gambar" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Detail</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <textarea cols="30" rows="5" class="form-control" name="detail_barang" id="detail_barang" readonly></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal"><i class="fa fa-cancel"></i> Batal</button>
                                                    <button type="button" id="btn_simpan" class="btn btn-primary waves-effect waves-light ">Tambah</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        <button type="button" id="pembayaran" class="btn btn-success waves-effect pull-right" data-toggle="modal" data-target="#default-Modal"><i class="fa fa-save"></i> <I>Pembayaran</I></button>
                        <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Pembayaran Pemebelian</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Total Pemeblian (Rp.)</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="id_bayar_hasil" id="id_bayar_hasil" class="form-control" placeholder="0" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Pajak (Rp. )</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="bayar_pajak" id="bayar_pajak" class="form-control" placeholder="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Grand Total (Rp. )</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="total_akhir" id="total_akhir" class="form-control" placeholder="0" value="0" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Pembayaran</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="dibayar" id="dibayar" class="form-control" placeholder="0" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Sisa Bayar</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="sisa_bayar" id="sisa_bayar" class="form-control" placeholder="0" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Jatuh Tempo</label>
                                            <div class="col-sm-8">
                                                <input type="date" name="jatuh_tempo" id="jatuh_tempo" class="form-control" placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batal</button>
                                        <button type="button" id="pembayaran_akhir" class="btn btn-primary waves-effect waves-light "> <i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    </form>
                        <div class="dt-responsive table-responsive">
                            <table class="table table-striped table-bordered nowrap dataTable" id="tabel_barang">
                                <thead style="font-size: small;" >
                                <tr>
                                    <!-- <th width="20">NO</th> -->
                                    <th style="text-align:center;" width="100">ITEM</th>
                                    <th style="text-align:center;" width="50">HARGA</th>
                                    <!-- <th width="40">SATUAN</th> -->
                                    <th style="text-align:center;" width="20">QTY</th>
                                    <th style="text-align:center;" width="20">DISC(%)</th>
                                    <th style="text-align:center;" width="30">TOTAL</th>
                                    <th style="text-align:center;" width="30">AKSI</th>
                                </tr>
                                </thead>
                                <tbody id="data_beli" style="font-size: small;" >

                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="3" ></td>
                                        <td ><b>Total (Rp.)</b></td>
                                        <td colspan="2"><h2 id="grand_total"></h2> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/penjualan/penjualan.js"></script>