<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size:12px;
}

#customers td, #customers th {
  padding: 8px;
}


#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #f2f2f2;
  color: #34343c;
  
}
</style>
</head>
<body>
<img src="<?= base_url()?>assets/images/logo.png" alt="" height="45px">
<p style="font-size:12px;">Jl. Simpang Pulau Laut Taman Sari No.2 Samping (SPBU S.Parman) Banjarmasin</p>
<p style="font-size:12px;">No. Telepon : 0511-337671</p>
<p style="font-size:12px;">Email : inifinitybanjarmasin@gmail.com</p>
<p style="font-size:12px;">Customer : </p>
</br>
<p style="font-size:14px;text-align:center"><b>FAKTUR PENJUALAN</b></p>
<p style="font-size:12px;">No. Faktur : ARS/INF/03/2020/0010</p>
<table id="customers">
  <thead>
      <tr>
          <th style="text-align:center;" width="5%">No</th>
          <th style="text-align:center;" width="35%">Item</th>
          <th style="text-align:center;" width="10%">Qty</th>
          <th style="text-align:center;" width="15%">Harga</th>
          <th style="text-align:center;" width="10%">Disc %</th>
          <th style="text-align:center;" width="15%">Total</th>
      </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Outdoor Infinity 2MP DBC-2C-T4f</td>
      <td>2 Pcs</td>
      <td>47.000</td>
      <td>30</td>
      <td style="text-align:right;">569.800,00</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Indoor Infinity 2MP DBC-2C-T4f</td>
      <td>2 Pcs</td>
      <td>47.000</td>
      <td>30</td>
      <td style="text-align:right;">569.800,00</td>
    </tr>
  </tbody>
</table>
<hr/>
<table width="100%" style='font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; font-size:12px;'>
<tr>
    <td width="55%"><b>*Note</b></td>
    <td widtd="25%">Total Sebelum Discon</td>
    <td widtd="20%" style="text-align:right;">32.520.200,00</td>
</tr>
<tr>
    <td width="55%">DP Sebelum pemasangan</td>
    <td widtd="25%">Discon (%)</td>
    <td widtd="20%" style="text-align:right;">1.980.100,00</td>
</tr>
<tr>
    <td width="55%">Sisa Pembayaran + PPN Setelah pemasangan selesai</td>
    <td widtd="25%">Total Setelah Discon</td>
    <td widtd="20%" style="text-align:right;">1.980.100,00</td>
</tr>
<tr>
    <td width="55%"></td>
    <td widtd="25%">Ppn</td>
    <td widtd="20%" style="text-align:right;">1.980.100,00</td>
</tr>
<tr>
    <td width="55%"></td>
    <td widtd="25%">Grand Total</td>
    <td widtd="20%" style="text-align:right;"><b>1.980.100,00</b></td>
</tr>
</table>
    
</body>
</html>
