<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title">Basic Inputs</h4>
                    <form>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kode Barcode</label>
                            <div class="col-sm-4">
                                <input type="text" name="id_group" class="form-control" hidden>
                                <input type="text" id="cek_group" name="kode_group" autofocus class="form-control" require>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                            <button type="button" class="btn btn-info" name="btn_simpan" id="btn_simpan">Bayar (F3)</button>
                            </div>
                        </div>
                    </form>
                        <div class="dt-responsive table-responsive">
                            <table class="table table-striped table-bordered nowrap">
                                <thead style="font-size: x-small;" >
                                <tr>
                                    <th>NO</th>
                                    <th>ID BARCODE</th>
                                    <th>NAMA BARANG</th>
                                    <th>HARGA</th>
                                    <th>SATUAN</th>
                                    <th>QTY</th>
                                    <th>DISC(%)</th>
                                    <th>TOTAL</th>
                                    <th>AKSI</th>
                                </tr>
                                </thead>
                                <tbody id="dynamic">

                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="5" ></td>
                                        <td ><b>Total</b></td>
                                        <td colspan="3"><h2 id="grand_total">Rp.0.00-</h2></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/penjualan/faktur.js"></script>