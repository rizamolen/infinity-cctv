<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class supplier extends CI_Controller {

    public function index()
    {
        $data['content'] = 'master_data/supplier/input';
        $this->load->view('overview',$data);
    }

}