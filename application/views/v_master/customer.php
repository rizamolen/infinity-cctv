<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_setup/m_group_barang');
	}

	public function index() {
		$data['group'] = $this->m_group_barang->getData();
		// echo json_encode($data); die;
		// print_r($data);
		$data['content'] = 'master_data/customer/input';
		$this->load->view('overview', $data);
	}
}