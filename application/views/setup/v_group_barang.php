<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title"><i class="fa fa-pencil"></i> Input Data Group Barang</h4>
                    <form>
                    <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kode Group</label>
                            <div class="col-sm-4">
                                <input type="text" id="cek_group" name="kode_group" autofocus class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Group</label>
                            <div class="col-sm-4">
                                <input type="text" id="cek_group" name="nama_group" autofocus class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                            <button type="button" class="btn btn-sm btn-info" name="btn_simpan" id="btn_simpan"><i class="fa fa-save"></i>SIMPAN</button>
                            <button type="button" class="btn btn-sm btn-warning" name="btn_update" id="btn_update"><i class="fa fa-edit"></i>UPDATE</button>
                            <button type="button" class="btn btn-sm btn-success" name="btn_reset" id="btn_reset"><i class="fa fa-refresh"></i>BATAL</button>
                            </div>
                        </div>
                    </form>
                        <div>
                        <table id="mydata" class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>KODE</th>
                                    <th>NAMA GROUP</th>
                                    <th style="text-align:center; width:200px;">AKSI</th>
                                </tr>
                            </thead>
                            <tbody id="show_data">
                                <?php $no=1; foreach($data_group as $row) {?>
                                <tr>
                                    <td><?= $no++?></td>
                                    <td><?= $row->kode;?></td>
                                    <td><?= $row->nama_group;?></td>
                                    <td style="text-align:center; width:200px;">
                                    <button id="get" class="btn btn-sm btn-warning btn-xs item_edit" data="<?= $row->kode;?>"><i class="fa fa-edit"></i>EDIT</button>
                                    <button id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" data="<?= $row->kode;?>"><i class="fa fa-trash"></i>HAPUS</button>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/setup/group-barang.js"></script>