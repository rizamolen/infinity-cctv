<table id="mydata" class="table table-bordered table-striped table-hover js-basic-example dataTable">
    <thead>
        <tr>
            <th>NO</th>
            <th>NAMA GROUP BARANG</th>
            <th style="text-align:center; width:200px;">AKSI</th>
        </tr>
    </thead>
    <tbody id="show_data">
        <?php $no=1; foreach($data_group as $row) {?>
        <tr>
            <td><?= $no++?></td>
            <td><?= $row->nama;?></td>
            <td style="text-align:center; width:200px;">
            <button id="get" class="btn btn-sm btn-warning btn-xs item_edit" data="<?= $row->id;?>"><i class="fa fa-edit"></i>EDIT</button>
            <button id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" data="<?= $row->id;?>"><i class="fa fa-trash"></i>HAPUS</button>
            </td>
        </tr>
        <?php }?>
    </tbody>
</table>