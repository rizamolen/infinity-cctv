<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size:12px;
}

#customers td, #customers th {
  padding: 6px;
}


#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #66A5AD;
  color: #fff;

}

.column{
  float: left;
  width: 49%;
  padding: 5px;
}

.row:after{
  content: "";
  display:table;
  clear: both;
}
</style>
</head>
<body>
<img src="<?=base_url()?>assets/images/logo.png" alt="" height="35px">
<p style="font-size:12px; line-height:0.5">CV. ARSILA SHALZA</p>
<p style="font-size:12px; line-height:0.5">Jl. Simpang Pulau Laut Taman Sari No.2 Samping (SPBU S.Parman) Banjarmasin</p>
<p style="font-size:12px; line-height:0.5">No. Telepon : 0511-337671</p>
<p style="font-size:12px; line-height:0.5">Email : inifinitybanjarmasin@gmail.com</p>
<p style="font-size:14px;text-align:center"><b>LAPORAN DATA BARANG</b></p>
  <?php $query = $this->db->query("SELECT * FROM barang $data");?>
  <table id="customers">
    <thead>
      <tr>
        <th style="text-align:center;">No</th>
        <th style="text-align:center;">Gambar</th>
        <th style="text-align:center;">Kode</th>
        <th style="text-align:center;">Nama</th>
        <th style="text-align:center;">Stok</th>
        <th style="text-align:center;">Detail</th>
        <th style="text-align:center;">Harga eu</th>
        <th style="text-align:center;">Harga in</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1;foreach ($query->result() as $row) {?>
        <tr>
          <td style="text-align:center;"><?=$no++;?></td>
          <td style="text-align:center;"><img src="<?php echo base_url(); ?>assets/images/barang/<?=$row->foto_barang?>" width="80" alt=""></td>
          <td style="text-align:center;"><?=$row->kode_barang;?></td>
          <td style="text-align:center;"><?=$row->nama_barang;?></td>
          <td style="text-align:center;"><?=$row->stok_barang;?></td>
          <td style="text-align:center;"><?=$row->detail_barang;?></td>
          <td style="text-align:center;"><?=$row->harga_enduser;?></td>
          <td style="text-align:center;"><?=$row->harga_installer;?></td>
        </tr>
      <?php }?>
    </tbody>
  </table>
</body>
</html>