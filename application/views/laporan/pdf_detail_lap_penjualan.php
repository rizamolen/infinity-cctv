<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size:12px;
}

#customers td, #customers th {
  padding: 6px;
}


#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #66A5AD;
  color: #fff;
  
}

.column{
  float: left;
  width: 49%;
  padding: 5px;
}

.row:after{
  content: "";
  display:table;
  clear: both;
}
</style>
</head>
<body>
<img src="<?= base_url()?>assets/images/logo.png" alt="" height="35px">
<p style="font-size:12px; line-height:0.5">CV. ARSILA SHALZA</p>
<p style="font-size:12px; line-height:0.5">Jl. Simpang Pulau Laut Taman Sari No.2 Samping (SPBU S.Parman) Banjarmasin</p>
<p style="font-size:12px; line-height:0.5">No. Telepon : 0511-337671</p>
<p style="font-size:12px; line-height:0.5">Email : inifinitybanjarmasin@gmail.com</p>
<p style="font-size:14px;text-align:center"><b>LAPORAN DETAIL PENJUALAN</b></p>
  <tbody>
  <?php 
      $main_query= $this->db->query("SELECT * FROM penjualan p left join customer c on p.id_customer=c.id_customer $data")->result();
      foreach ($main_query as $main) {
          $sub_menu = $this->db->query("SELECT * FROM detail_penjualan dp left join barang b on dp.id_barang=b.id_barang where no_faktur = '$main->no_faktur'");
          if ($sub_menu->num_rows()>0) { ?>
            <div class="row">
              <div class="column">
                <p style="font-size:12px;">Customer : <b><?= $main->nama_customer?></b> &nbsp; (<?php echo date('d M Y', strtotime($main->tanggal_beli))?>)</p>
              </div>
              <div class="column" style="font-size:12px; text-align: right;">
                <p style="font-size:12px; tex-align:right;">No. Faktur : <b><?= $main->no_faktur;?></b></p>
              </div>
            </div>
              <table id="customers">
              <thead>
                  <tr>
                      <th style="text-align:center;" width="5%">No</th>
                      <th style="text-align:center;" width="45%">Item</th>
                      <th style="text-align:center;" width="15%">Harga</th>
                      <th style="text-align:center;" width="10%">qty</th>
                      <th style="text-align:center;" width="10%">Discon</th>
                      <th style="text-align:center;" width="15%">Total</th>
                  </tr>
              </thead>
              <tbody>
              <?php $total_sblmm = 0; $no=1; foreach($sub_menu->result() as $sub){ $total_sblm = $sub->harga; $total_qty = $total_sblm* $sub->qty ;$total_sblmm += $total_qty;?>
                  <tr>
                    <td><?= $no++;?></td>
                    <td><b>[<?= $sub->kode_barang?>]</b> <?= $sub->nama_barang?></td>
                    <td style="text-align:center;"><?= 'Rp.'.number_format($sub->harga,2,',','.')?></td>
                    <td style="text-align:center;"><?= $sub->qty?></td>
                    <td style="text-align:center;"><?= $sub->discon?></td>
                    <td style="text-align:right;"><?= 'Rp.'.number_format($sub->total,2,',','.')?></td>
                  </tr>
             <?php } ?>
              </tbody>
              </table>
              <hr/>
              <table width="100%" style='font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; font-size:12px;'>
              <tr>
                  <td width="55%"><b><I>Note*</I></b></td>
                  <td widtd="25%">Total Sebelum Discon</td>
                  <td widtd="20%" style="text-align:right;"><?= 'Rp.'.number_format($total_sblmm,2,',','.');?></td>
              </tr>
              <tr>
                  <td width="55%"><?php if($main->sisa_bayar=='0.00'){ echo "Status : <b><label style='color:#039906;'>Lunas</label></b>";}else{echo "Status : <b><label style='color:#ec0505;'>Belum Lunas</label></b>";}?></td>
                  <td widtd="25%">Discon (%)</td>
                  <td widtd="20%" style="text-align:right;"><?php $discon = $total_sblmm -  $main->total_bayar; echo 'Rp.'.number_format($discon,2,',','.');?></td>
              </tr>
              <tr>
                  <td width="55%"><?php if($main->sisa_bayar=='0.00'){ echo "";}else{echo "Sisa Pembayaran : <b> Rp.".number_format($main->sisa_bayar,2,',','.')."</b>";}?></td>
                  <td widtd="25%">Total Setelah Discon</td>
                  <td widtd="20%" style="text-align:right;"><?= 'Rp.'.number_format($main->total_bayar,2,',','.');?></td>
              </tr>
              <tr>
                  <td width="55%"></td>
                  <td widtd="25%">Ppn (10%)</td>
                  <td widtd="20%" style="text-align:right;"><?= 'Rp.'.number_format($main->pajak,2,',','.');?></td>
              </tr>
              <tr>
                  <td width="55%"></td>
                  <td widtd="25%">Grand Total</td>
                  <td widtd="20%" style="text-align:right;"><b><?= 'Rp.'.number_format($main->grand_total,2,',','.');?></b></td>
              </tr>
              </table>
              </br>
              <div style="border-bottom: 2px dashed #808080;"></div>
         <?php }else{ ?>
              <li>
              
              </li>
          <?php }
      }
  ?> 

</body>
</html>