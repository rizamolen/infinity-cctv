<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title"><i class="fa fa-filter"></i> Filter Data Barang Pinjaman</h4>
                    <ul class="nav nav-tabs md-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab" aria-expanded="true">Detail</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab" aria-expanded="false">Rekap</a>
                            <div class="slide"></div>
                        </li>
                    </ul>
                    <div class="tab-content card-block">
                        <div class="tab-pane active" id="home3" role="tabpanel">
                        <form id="form" method="post" action="<?= base_url();?>barangPin_pdf" target="_blank">
                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Grup Barang</label>
                                    <div class="col-sm-4">
                                        <select name="masterBarang" id="masterBarang" class="form-control">
                                        <option value="">ALL</option>
                                        <?php foreach($pilih_groupBarang as $row){?>
                                            <option value="<?= $row->kode?>"><?= $row->kode?> - <?= $row->nama_group?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Type File</label>
                                    <div class="col-sm-4">
                                    <div class="form-radio m-b-30">
                                        <div class="radio radiofill radio-danger radio-inline">
                                            <label>
                                                <input type="radio" name="radio" id="pdf" checked="checked">
                                                <i class="helper"></i>PDF
                                            </label>
                                        </div>
                                        <div class="radio radiofill radio-success radio-inline">
                                            <label>
                                                <input type="radio" name="radio" id="excel">
                                                <i class="helper"></i>EXCEL
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                    <button type="submit" class="btn btn-sm btn-success pull-right" name="btn_simpan" id="btn_simpan"><i class="fa fa-file-o"></i>Export</button>
                                    <!-- <button type="button" class="btn btn-sm btn-success pull-right" name="btn_simpan" id="btn_simpan"><i class="fa fa-file-excel-o"></i>EXCEL FILE</button> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="profile3" role="tabpanel">
                            <p class="m-0">2.Cras consequat in enim ut efficitur. Nulla posuere elit quis auctor interdum praesent sit amet nulla vel enim amet. Donec convallis tellus neque, et imperdiet felis amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#masterBarang").select2();
    $("#excel").on('click', function(){
        $('#form').attr('action','barangPin_excel');
    });
    $("#pdf").on('click', function(){
        $('#form').attr('action','barangPin_pdf');
    });
});
</script>