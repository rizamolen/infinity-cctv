<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                <h4 class="sub-title"><i class="fa fa-shopping-cart"></i> Pelunasan Pembelian Barang</h4>
                    <form>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Faktur</label>
                                </div>
                                <div class="col-sm-8">
                                <select id="no_faktur" name="no_faktur" class="form-control">
                                <option value=" ">Pilih No Faktur</option>
                                    <?php foreach($no_faktur as $row) {?>
                                    <option value="<?= $row->no_faktur;?>"><?= $row->no_faktur;?> - <?= $row->no_invoice;?></option>
                                    <?php }?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Invoice Pembelian</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text"  name="no_invoice" id="no_invoice" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Supplier</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text"  name="supplier" id="supplier" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Tanggal Jatuh Tempo</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="date" id="jatuh_tempo" name="jatuh_tempo" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                <label for="">Total Pembelian</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="grand_total" name="grand_total" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Sisa Pembayaran</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="sisa_bayar" name="sisa_bayar" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                <label for="">Jumlah Pembayaran</label>  
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="pembayaran" name="pembayaran" class="form-control" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Sisa Pembayaran Akhir</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="sisa_akhir" name="sisa_akhir" class="form-control" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                               
                                </div>
                                <div class="col-sm-8">
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    
                                </div>
                                <div class="col-sm-8">
                                <button type="button" id="pelunasan" class="btn btn-success waves-effect pull-right"><i class="fa fa-save"></i> <I>Pembayaran</I></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />          
                    </form>
                        <div class="dt-responsive table-responsive">
                            <table class="table table-striped table-bordered nowrap dataTable" id="tabel_barang">
                                <thead style="font-size: small;" >
                                <tr>
                                    <th width="20">NO</th>
                                    <th width="50">KODE BARANG</th>
                                    <th width="50">NAMA BARANG</th>
                                    <th width="50">HARGA</th>
                                    <!-- <th width="40">SATUAN</th> -->
                                    <th width="20">QTY</th>
                                    <th width="20">DISC(%)</th>
                                    <th width="30">TOTAL</th>
                                </tr>
                                </thead>
                                <tbody id="data_beli" style="font-size: small;" >

                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/pembelian/pelunasan_pembelian.js"></script>