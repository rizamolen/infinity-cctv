<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                <h4 class="sub-title"><i class="fa fa-shopping-cart"></i> Input Pembelian Barang Oprasional Kantor</h4>
                    <form>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Faktur</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="no_faktur" id="no_faktur" class="form-control" readonly value="<?= $no_faktur;?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Nota</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text"  name="nota_beli" id="nota_beli" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Toko / Supplier</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="supplier" name="supplier" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Tanggal Pembelian</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="date" id="tanggal_beli" name="tanggal_beli" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                <button type="button" id="pembayaran" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#Modal-Barang"><i class="fa fa-plus"></i> <I>Tambah Barang</I></button>
                                </div>
                                <div class="col-sm-9">
                                
                                </div>
                                <div class="col-sm-2">
                                    <div class="modal fade" id="Modal-Barang" tabindex="-1" role="dialog">
                                        <div class="modal-dialog col-sm-8" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Barang Pembelian</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Item</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="text" id="id_edit_barang" name="id_edit_barang" hidden>
                                                                    <input type="text" id="nama_barang" name="nama_barang" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Harga</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number_format" class="form-control" name="harga_barang" id="harga_barang" placeholder="0" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Satuan Beli</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <select name="satuan_beli" id="satuan_beli" class="form-control">
                                                                    <option value="">Pilih Satuah</option>
                                                                    <?php foreach($pilih_satuan as $row) {?>
                                                                        <option value="<?= $row->id?>"><?= $row->satuan?></option>
                                                                    <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Qty</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number_format" id="qty" name="qty" class="form-control reset" autocomplete="off" value="1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                    <div class="col-sm-6">
                                                            <!-- <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for="">Discon (%)</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                <input type="number_format" id="discon" name="discon" class="form-control reset" autocomplete="off" value="0">
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group row">
                                                                <div class="col-sm-4">
                                                                    <label for=""><b>Total (Rp.)</b></label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="number_format" style="font-size:20px; font-weight:bold;" class="form-control" id="total" name="total" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal"><i class="fa fa-cancel"></i> Batal</button>
                                                    <button type="button" id="btn_simpan" class="btn btn-primary waves-effect waves-light ">Tambah</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        <button type="button" id="pembayaran" class="btn btn-success waves-effect pull-right" data-toggle="modal" data-target="#default-Modal"><i class="fa fa-save"></i> <I>Pembayaran</I></button>
                        <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Pembayaran Pemebelian</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Total Pembelian (Rp.)</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="grand_total_bayar" id="grand_total_bayar" class="form-control" placeholder="0" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Pembayaran</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="dibayar" id="dibayar" class="form-control" placeholder="0" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Sisa Bayar</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="sisa_bayar" id="sisa_bayar" class="form-control" placeholder="0" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batal</button>
                                        <button type="button" id="pembayaran_akhir" class="btn btn-primary waves-effect waves-light "> <i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    </form>
                        <div class="dt-responsive table-responsive">
                            <table class="table table-striped table-bordered nowrap dataTable" id="tabel_barang">
                                <thead style="font-size: small;" >
                                <tr>
                                    <th width="20">NO</th>
                                    <th width="50">NAMA BARANG</th>
                                    <th width="50">HARGA</th>
                                    <th width="20">QTY</th>
                                    <th width="30">TOTAL</th>
                                    <th style="text-align:center;" width="30">HAPUS</th>
                                </tr>
                                </thead>
                                <tbody id="data_beli" style="font-size: small;" >

                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="3" ></td>
                                        <td ><b>Total (Rp.)</b></td>
                                        <td colspan="2"><h2 id="grand_total"></h2> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/pembelian/oprasional_kantor.js"></script>