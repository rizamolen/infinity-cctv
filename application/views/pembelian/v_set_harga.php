<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title"><i class="fa fa-cog"></i> Setting Harga Barang Beli</h4>
                    <form>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Koda Barang</label>
                            <div class="col-sm-6">
                                <select name="pilih_barang" id="pilih_barang" class="form-control">
                                <option value="">Pilih Barang</option>
                                <?php foreach($pilihan_barang as $row) {?>
                                    <option value="<?= $row->id_barang?>"><?= $row->kode_barang?> - <?= $row->nama_barang?></option>
                                <?php }?>
                                </select>
                            </div>
                            <h3 id="satuan"></h3>
                        </div>
                        <hr/>
                        <legend>Harga Lama</legend>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Harga Enduser (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="lama_enduser" id="lama_enduser" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Harga Installer (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text"  name="lama_installer" id="lama_installer" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <legend>Harga Modal</legend>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Harga Modal (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="modal_harga" id="modal_harga" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Pajak (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text"  name="pajak" id="pajak" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Ongkos Kirim (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="ongkir" id="ongkir" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Total Modal (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" style="font-weight:bold;"  name="total_modal" id="total_modal" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <legend>Set Harga Baru</legend>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Harga Enduser (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="harga_end_baru" id="harga_end_baru" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Harga Installer (Rp.)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text"  name="harga_ins_baru" id="harga_ins_baru" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                            <button type="button" class="btn btn-sm btn-info pull-right" name="btn_simpan" id="btn_simpan"><i class="fa fa-save"></i>SIMPAN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/pembelian/set_harga.js"></script>