<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                <h4 class="sub-title"><i class="fa fa-truck"></i> Input Jasa Expedisi Pengiriman Barang</h4>
                    <form>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Faktur</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="no_faktur" id="no_faktur" class="form-control" readonly value="<?= $no_faktur?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Faktur Pembelian</label>
                                </div>
                                <div class="col-sm-8">
                                <select name="faktur_pembelian" id="faktur_pembelian" class="form-control">
                                    <option value="">Pilih Faktur Pembelian</option>
                                    <?php foreach($faktur_pembelian as $row){?>
                                        <option value="<?= $row->no_faktur?>"><?= $row->no_faktur?> ( <?= $row->no_invoice ?> )</option>
                                    <?php }?>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">No. Nota Expedisi</label>
                                </div>
                                <div class="col-sm-8">
                                <input type="text" id="nota_expedisi" name="nota_expedisi" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="">Expedisi</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="nama_expedisi" name="nama_expedisi" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                     <label for="">Biaya Jasa  (Rp.)</label>
                                </div>
                                <div class="col-sm-8">
                                     <input type="text" id="biaya_jasa" name="biaya_jasa" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    
                                </div>
                                <div class="col-sm-8">
                                <button type="button" class="btn btn-sm btn-info" name="btn_simpan" id="btn_simpan"><i class="fa fa-save"></i>SIMPAN</button>
                                <button type="button" class="btn btn-sm btn-warning" name="btn_update" id="btn_update"><i class="fa fa-edit"></i>UPDATE</button>
                                <button type="button" class="btn btn-sm btn-success" name="btn_reset" id="btn_reset"><i class="fa fa-refresh"></i>BATAL</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />                 
                    </form>
                        <div class="dt-responsive table-responsive">
                            <table class="table table-striped table-bordered nowrap dataTable" id="tabel_barang">
                                <thead style="font-size: small;" >
                                <tr>
                                    <th width="20">NO</th>
                                    <th width="50">NO FAKTUR</th>
                                    <th width="50">FAKTUR PEMBELIAN</th>
                                    <th width="20">NO. NOTA JASA</th>
                                    <th width="30">EXPEDISI</th>
                                    <th width="30">BIAYA</th>
                                    <th style="text-align:center;" width="30">AKSI</th>
                                </tr>
                                </thead>
                                <tbody id="data_beli" style="font-size: small;" >
                                    <?php $no=1; foreach($datajasa as $row){?>
                                    <tr>
                                        <td><?= $no++;?></td>
                                        <td><?= $row->no_faktur?></td>
                                        <td><?= $row->no_faktur_pembelian?></td>
                                        <td><?= $row->no_jasa?></td>
                                        <td><?= $row->expedisi?></td>
                                        <td><?= $row->biaya?></td>
                                        <td>
                                        <button id="get" class="btn btn-sm btn-warning btn-xs item_edit" data="<?= $row->no_faktur;?>"><i class="fa fa-edit"></i>EDIT</button>
                                        <button id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" data="<?= $row->no_faktur;?>"><i class="fa fa-trash"></i>HAPUS</button>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/pembelian/jasa_expedisi.js"></script>