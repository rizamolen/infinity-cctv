<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash')?>"></div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-block">
                <h4 class="sub-title"><i class="fa fa-pencil"></i> Input Data Master Barang</h4>
                <form id="formact" action="<?= base_url()?>master/c_barang/simpan" method="POST" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Group Barang</label>
                            <div class="col-sm-10">
                                <div class="form-group row">
                                    <div class="col-sm-8">
                                        <select id="group_barang" name="group_barang" class="form-control" required>
                                            <option value="">Pilih Group Barang</option>
                                            <?php foreach($pilihan_group as $row) {?>
                                            <option value="<?= $row->kode;?>"><?= $row->kode;?> - <?= $row->nama_group;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-success btn-sm m-b-0 waves-effect" id="create_kode_barang"> <i class="fa fa-refresh"></i> Create Data Barang</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Kode Barang</label>
                                <div class="col-sm-8">
                                    <input type="text" name="kode_barang" id="kode_barang" class="form-control" placeholder="Kode Barang" required readonly>
                                    <input type="text" name="id_barang" id="id_barang" hidden>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <button type="button" class="btn btn-success btn-sm m-b-0 waves-effect" data-toggle="modal" data-target="#default-Modal"> <i class="fa fa-search"></i> Edit Data Barang</button>
                                        <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
                                            <div class="modal-dialog col-sm-7" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Data Barang</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <table id="tabel_barang" class="table table-bordered table-striped table-hover js-basic-example dataPenawaran">
                                                        <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Kode Barang</th>
                                                            <th>Nama Barang</th>
                                                            <th>Satuan</th>
                                                            <th>Edit</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $no=1; foreach($data_barang as $row) {?>
                                                                <tr>
                                                                    <td><?= $no++?></td>
                                                                    <td><?= $row->kode_barang?></td>
                                                                    <td><?= $row->nama_barang?></td>
                                                                    <td><?= $row->satuan?></td>
                                                                    <td> <button id="get" class="btn btn-sm btn-warning btn-xs item_edit" data="<?= $row->id_barang;?>"><i class="fa fa-edit"></i></button></td>
                                                                </tr>
                                                            <?php }?>
                                                        </tbody>
                                                    </table>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary waves-effect " data-dismiss="modal">Batal</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Barang</label>
                            <div class="col-sm-10">
                                <input type="text" id="nama_barang" name="nama_barang" class="form-control" placeholder="Nama Barang" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Detail Barang</label>
                            <div class="col-sm-10">
                                <textarea name="detail_barang" id="detail_barang" class="form-control" rows="5" ></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Satuan Barang</label>
                            <div class="col-sm-10">
                                <select id="satuan_barang" name="satuan_barang" class="form-control" required>
                                    <option value="">Pilih Satuan</option>
                                    <?php foreach($pilihan_satuan as $row) {?>
                                    <option value="<?= $row->id;?>"><?= $row->satuan;?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                <label class="col-sm-4 col-form-label" >Stok Barang</label>
                                <div class="col-sm-8">
                                <input type="text" id="stok_barang" name="stok_barang" class="form-control" placeholder="0">
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                <label class="col-sm-4 col-form-label" >Minimum Stok</label>
                                <div class="col-sm-8">
                                <input type="text" id="minimum_stok" name="minimum_stok" class="form-control" placeholder="0" required>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                <label class="col-sm-4 col-form-label" >Harga Enduser  (Rp.)</label>
                                <div class="col-sm-8">
                                <input type="text" id="harga_enduser" name="harga_enduser" class="form-control" placeholder="0">
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                <label class="col-sm-4 col-form-label" >Harga Installer (Rp.)</label>
                                <div class="col-sm-8">
                                <input type="text" id="harga_installer" name="harga_installer" class="form-control" placeholder="0">
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Upload Foto Barang</label>
                            <div class="col-sm-10">
                            <input type="file" class="form-control" accept="image/*"  onchange="tampilkanPreview(this,'preview')" name="filefoto"/>
                            <input type="text" name="hidden_foto" id="hidden_foto" hidden>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Foto Barang</label>
                            <div class="col-sm-10">
                            <img src="<?php echo base_url();?>assets/images/barang/<?= $foto_barang;?>" id="preview" width="200" alt="Preview Gambar" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" id="update_data" class="btn btn-warning m-b-0 pull-right"> <i class="fa fa-edit"></i> UPDATE</button>
                                <button type="submit" id="simpan_data" class="btn btn-primary m-b-0 pull-right"> <i class="fa fa-save"></i> SIMPAN</button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/app-scripts/master_data/barang.js"></script>