<div class="page-body">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-block">
					<h4 class="sub-title">Input Nama Paket Kamera CCTV</h4>
					<form id="form">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Paket</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama_paket" class="form-control" placeholder="Isi Nama Paket" required>
                                <input type="hidden" name="id">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="text" id="hargaP" name="harga" class="form-control" placeholder="Harga Paket" required>
                            </div>
                        </div>

                        <div class="form-group row">
							<div class="col-sm-10">
								<button type="button" class="btn btn-sm btn-info" name="btn_simpan" id="btn_simpan"><i class="fa fa-save"></i>SIMPAN</button>
								<button type="button" class="btn btn-sm btn-warning" name="btn_update" id="btn_update"><i class="fa fa-edit"></i>UPDATE</button>
								<button type="button" class="btn btn-sm btn-success" name="btn_reset" id="btn_reset"><i class="fa fa-refresh"></i>BATAL</button>
							</div>
						</div>

					</form>

					<div class="dt-responsive table-responsive">
                        <table id="simpletable" class="table table-striped table-bordered nowrap dataTable">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <th>Harga</th>
                            <th style="text-align:center; width:200px;">AKSI</th>
                            </tr>
                        </thead>
                        <tbody id="show_data">
                            <?php $no = 1;foreach ($viewData as $row) {?>
                            <tr>
                            <td><?=$no++?></td>
                            <td><?=$row->nama_paket;?></td>
                            <td><?=$row->harga;?></td>
                            <td style="text-align:center; width:200px;">
                                <button id="btn_getEdit" class="btn btn-sm btn-warning btn-xs item_edit" data="<?=$row->id;?>"><i class="fa fa-edit"></i>EDIT</button>
                                <button id="btn_hapus" class="btn btn-sm btn-danger btn-xs item_delete" data="<?=$row->id;?>"><i class="fa fa-trash"></i>HAPUS</button>
                            </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        </table>
                    </div> <!-- END DIV TABLE -->

				</div> <!-- END DIV CARD BLOCK -->
			</div> <!-- END DIV CARD -->
		</div> <!-- END DIV COL-SM-12 -->
	</div> <!-- END DIV ROW -->
</div> <!-- END DIV PAGE BODY -->

<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/app-scripts/master_data/paket.js"></script>