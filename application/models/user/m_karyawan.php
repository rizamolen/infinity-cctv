<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_karyawan extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('karyawan');
		$data = $this->db->get();
		return $data->result();
    }
    
    function insert($data){
        $hasil=$this->db->insert("karyawan",$data);
        return $hasil;
    }

    public function getDataJab()
    {
		$this->db->select('*');
		$this->db->from('jabatan');
		$data = $this->db->get();
		return $data->result();
    }
    
    public function getDataby($id)
	{
        $hsl=$this->db->query("SELECT * FROM karyawan WHERE id='$id'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'               => $data ->id,
                    'kode_karyawan'    => $data ->kode_karyawan,
                    'nama'             => $data ->nama,
                    'tempat_lahir'     => $data ->tempat_lahir,
                    'tanggal_lahir'    => $data ->tanggal_lahir,
                    'jabatan'          => $data ->id_jabatan,
                    'alamat'           => $data ->alamat,
                    'no_hp'            => $data ->no_hp,
                    'gambar'           => $data ->foto
                );
            }
        }
        return $hasil;
    }

    function update($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update("karyawan",$data);
        return $query;
    }

    function delete($id){
        $hasil=$this->db->query("DELETE FROM karyawan WHERE id='$id'");
        return $hasil;
    }

}