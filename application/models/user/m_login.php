<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_login extends CI_Model{

	public function cek_akses($log)
	{
		$qy = $this->db->query("select id from b_akses ".$log);
		return $qy;
    }
    
	public function llogin($where)
	{
		$cek = $this->db->query ("select * from user_management".$where);
		return $cek;
	}

	public function login($where)
	{
		$this->db->select('karyawan.id, karyawan.nama, karyawan.foto');
		$this->db->from('user_app');
		$this->db->join('karyawan','user_app.id_karyawan=karyawan.id','left');
		$this->db->where($where);
		$data=$this->db->get();
		return $data;
	}

}