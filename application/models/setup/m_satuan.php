<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_satuan extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('satuan');
		$data = $this->db->get();
		return $data->result();
	}
	
	public function getDataby($kode)
	{
        $hsl=$this->db->query("SELECT * FROM satuan WHERE id='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'   => $data->id,
                    'satuan' => $data->satuan,
                    );
            }
        }
        return $hasil;
    }

    public function getKode($satuan)
	{
        $hsl=$this->db->query("SELECT * FROM satuan WHERE satuan='$satuan'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'     => $data->id,
                    'satuan' => $data->satuan,
                    );
            }
        }
        return $hasil;
    }



    function insert($data){
        $hasil=$this->db->insert("satuan",$data);
        return $hasil;
    }

    function update_barang($data , $id){
            $this->db->where('id',$id);
            $query = $this->db->update("satuan",$data);
            return $query;
    }

    function hapus_barang($id){
        $hasil=$this->db->query("DELETE FROM satuan WHERE id='$id'");
        return $hasil;
    }

}