<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_group_barang extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('grup_barang');
		$data = $this->db->get();
		return $data->result();
	}
    
	public function getDataby($kode)
	{
        $hsl=$this->db->query("SELECT * FROM grup_barang WHERE kode='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'kode'       => $data->kode,
                    'nama_group' => $data->nama_group,
                    );
            }
        }
        return $hasil;
    }

    public function getKode($kode)
	{
        $hsl=$this->db->query("SELECT * FROM grup_barang WHERE kode='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'kode'       => $data->kode,
                    'nama_group' => $data->nama_group,
                    );
            }
        }
        return $hasil;
    }

    public function getBarang($kode)
	{
        $hsl=$this->db->query("SELECT * FROM barang WHERE id_barang='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'      => $data->id_barang,
                    'nama_barang'    => $data->nama_barang,
                    'harga_beli'     => $data->harga_beli,
                    );
            }
        }
        return $hasil;
    }


    function insert($data){
        $hasil=$this->db->insert("grup_barang",$data);
        return $hasil;
    }

    function update_barang($data , $kode){
            $this->db->where('kode',$kode);
            $query = $this->db->update("grup_barang",$data);
            return $query;
    }

    function hapus_barang($kode){
        $hasil=$this->db->query("DELETE FROM grup_barang WHERE kode='$kode'");
        return $hasil;
    }

}