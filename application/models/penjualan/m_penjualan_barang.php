<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_penjualan_barang extends CI_Model{

    public function tampilJual($where)
    {
		$this->db->select('detail_penjualan.id, detail_penjualan.id_barang, barang.kode_barang, barang.nama_barang, detail_penjualan.harga, detail_penjualan.qty, satuan.satuan, detail_penjualan.discon, detail_penjualan.total');
        $this->db->from('detail_penjualan');
        $this->db->join('barang','detail_penjualan.id_barang=barang.id_barang','left');
        $this->db->join('satuan','barang.id_satuan=satuan.id','left');
        $this->db->where($where);
		$data = $this->db->get();
		return $data->result();
    }

    public function getCustomer()
    {
		$this->db->select('*');
        $this->db->from('customer');
		$data = $this->db->get();
		return $data->result();
    }


    public function penjualan($where)
	{
        $hasil=$this->db->query("SELECT no_faktur FROM penjualan WHERE $where");
        return $hasil;
    }

    public function detailCustomer($id_customer)
	{
        $hsl=$this->db->query("SELECT * FROM customer WHERE id_customer='$id_customer'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'no_telp_customer'       => $data->no_telp_customer,
                    'alamat_customer'       => $data->alamat_customer,
                    'jenis_customer'        => $data->jenis_customer,

                    );
            }
        }
        return $hasil;
    }
    
    public function barangBeli($where)
    {
        $hsl=$this->db->query("select b.id_barang, b.kode_barang, b.nama_barang, s.satuan, b.harga_enduser, b.harga_installer, b.detail_barang from barang b left join satuan s on b.id_satuan=s.id where $where");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'         => $data->id_barang,
                    'kode_barang'       => $data->kode_barang,
                    'nama_barang'       => $data->nama_barang,
                    'harga_enduser'     => $data->harga_enduser,
                    'harga_installer'   => $data->harga_installer,
                    'satuan'            => $data->satuan,
                    'detail_barang'     => $data->detail_barang,
                    );
            }
        }
        return $hasil;
    }
    
    public function editBarang($where)
    {
        $hsl=$this->db->query("SELECT pb.id, pb.qty, pb.harga_satuan, pb.disc, pb.total ,b.id_barang, b.nama_barang, b.foto_barang, b.detail_barang,s.satuan satuan_kecil, l.satuan satuan_besar ,s.id id_satuan_kecil, l.id id_satuan_besar FROM detail_penjualan pb left join barang b on pb.id_barang=b.id_barang left join satuan s on b.id_satuan_kecil=s.id left join satuan l on b.id_satuan_besar=l.id where $where");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'                => $data->id,
                    'id_barang'         => $data->id_barang,
                    'nama_barang'       => $data->nama_barang,
                    'satuan_kecil'      => $data->satuan_kecil,
                    'satuan_besar'      => $data->satuan_besar,
                    'id_satuan_kecil'   => $data->id_satuan_kecil,
                    'id_satuan_besar'   => $data->id_satuan_besar,
                    'foto_barang'       => $data->foto_barang,
                    'detail_barang'     => $data->detail_barang,
                    'qty'               => $data->qty,
                    'harga_satuan'      => $data->harga_satuan, 
                    'disc'              => $data->disc,
                    'total'             => $data->total,
                    );
            }
        }
        return $hasil;
    }

    public function no_faktur()
    {
        $day = date("m");
        $q = $this->db->query("SELECT MAX(RIGHT(no_faktur,4)) AS no_max from penjualan WHERE MONTH(tanggal_beli)=$day");
        $no = "";
        if($q->num_rows()>0){
            foreach($q->result() as $no){
                $tmp = ((int)$no->no_max)+1;
                $no = sprintf("%04s",$tmp);
            }
        }else{
            $no = "0001";
        }
        // date_default_timezone_set('Asia/Jakarta');
        return 'ARS/INF/'.date('m/Y').'/'.$no;
    }

    public function hapusBarang($id){
        $hasil=$this->db->query("DELETE FROM detail_penjualan WHERE id='$id'");
        return $hasil;
    }


    public function getBarang($kode){
        $hsl=$this->db->query("SELECT * FROM barang WHERE id_barang='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'      => $data->id_barang,
                    'nama_barang'    => $data->nama_barang,
                    );
            }
            return $hasil;
        }
        
    }

    public function simpanJual($data){
        $hasil=$this->db->insert("detail_penjualan",$data);
        return $hasil;
    }

    public function bayarPenjualan($data){
        $hasil=$this->db->insert("penjualan",$data);
        return $hasil;
    }


    public function tampilBeli($where){
        $this->db->select('detail_penjualan.*, barang.id_barang, barang.nama_barang, satuan.id id_satuan, satuan.satuan');
        $this->db->from('detail_penjualan');
        $this->db->join('barang','detail_penjualan.id_barang=barang.id_barang','left');
        $this->db->join('satuan','detail_penjualan.id_satuan=satuan.id','left');
        $this->db->where($where);
        $data = $this->db->get();
        return $data->result();
    }

}


