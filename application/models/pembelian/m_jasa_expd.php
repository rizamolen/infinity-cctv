<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_jasa_expd extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('jasa_expedisi');
		$data = $this->db->get();
		return $data->result();
	}
    
    public function fakturPembelian()
    {
		$this->db->select('pb.no_faktur, pb.no_invoice');
        $this->db->from('pembelian pb');
        $this->db->join('jasa_expedisi exp','pb.no_faktur=exp.no_faktur_pembelian','left');
		$data = $this->db->get();
		return $data->result();
    }
    
    public function no_faktur()
    {
        $day = date("m");
        $q = $this->db->query("SELECT MAX(RIGHT(no_faktur,4)) AS no_max from jasa_expedisi WHERE substr(no_faktur,5,2)='$day'");
        $no = "";
        if($q->num_rows()>0){
            foreach($q->result() as $no){
                $tmp = ((int)$no->no_max)+1;
                $no = sprintf("%04s",$tmp);
            }
        }else{
            $no = "0001";
        }
        // date_default_timezone_set('Asia/Jakarta');
        return 'EPD/'.date('m/Y').'/'.$no;
    }

    function insert($data){
        $hasil=$this->db->insert("jasa_expedisi",$data);
        return $hasil;
    }

    function updateData($data,$no_faktur){
        $this->db->where('no_faktur',$no_faktur);
        $query = $this->db->update("jasa_expedisi",$data);
        return $query;
    }

    function hapusData($no_faktur){
        $hasil=$this->db->query("DELETE FROM jasa_expedisi WHERE no_faktur='$no_faktur'");
        return $hasil;
    }

    public function getEdit($id)
	{
        $hsl=$this->db->query("SELECT * FROM jasa_expedisi WHERE no_faktur='$id'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'no_faktur'           => $data->no_faktur,
                    'no_faktur_pembelian' => $data->no_faktur_pembelian,
                    'no_jasa'           => $data->no_jasa,
                    'expedisi' => $data->expedisi,
                    'biaya'           => $data->biaya,
                    );
            }
        }
        return $hasil;
    }


}