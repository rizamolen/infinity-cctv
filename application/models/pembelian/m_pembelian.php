<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_pembelian extends CI_Model{

    public function getData($where)
    {
		$this->db->select('detail_pembelian.id,detail_pembelian.no_pembelian, detail_pembelian.id_barang, barang.nama_barang');
        $this->db->from('detail_pembelian');
        $this->db->join('barang','detail_pembelian.id_barang=barang.id_barang','left');
        $this->db->where($where);
		$data = $this->db->get();
		return $data->result();
    }
    
    public function getBeli($where)
    {
        $hsl=$this->db->query("SELECT b.id_barang, b.kode_barang,b.nama_barang, b.detail_barang, b.id_satuan,s.satuan, b.foto_barang FROM barang b left join satuan s on b.id_satuan=s.id where $where");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'         => $data->id_barang,
                    'kode_barang'       => $data->kode_barang,
                    'nama_barang'       => $data->nama_barang,
                    'detail_barang'     => $data->detail_barang,
                    'id_satuan'            => $data->id_satuan,
                    );
            }
        }
        return $hasil;
    }
    
    public function editBarang($where)
    {
        $hsl=$this->db->query("SELECT pb.id, pb.qty, pb.harga_satuan, pb.disc, pb.total ,b.id_barang, b.nama_barang, b.foto_barang, b.detail_barang,s.satuan satuan_kecil, l.satuan satuan_besar ,s.id id_satuan_kecil, l.id id_satuan_besar FROM detail_pembelian pb left join barang b on pb.id_barang=b.id_barang left join satuan s on b.id_satuan_kecil=s.id left join satuan l on b.id_satuan_besar=l.id where $where");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'                => $data->id,
                    'id_barang'         => $data->id_barang,
                    'nama_barang'       => $data->nama_barang,
                    'satuan_kecil'      => $data->satuan_kecil,
                    'satuan_besar'      => $data->satuan_besar,
                    'id_satuan_kecil'   => $data->id_satuan_kecil,
                    'id_satuan_besar'   => $data->id_satuan_besar,
                    'foto_barang'       => $data->foto_barang,
                    'detail_barang'     => $data->detail_barang,
                    'qty'               => $data->qty,
                    'harga_satuan'      => $data->harga_satuan, 
                    'disc'              => $data->disc,
                    'total'             => $data->total,
                    );
            }
        }
        return $hasil;
    }

    public function no_faktur()
    {
        $day = date("m");
        $q = $this->db->query("SELECT MAX(RIGHT(no_faktur,4)) AS no_max from pembelian WHERE MONTH(tanggal_beli)=$day");
        $no = "";
        if($q->num_rows()>0){
            foreach($q->result() as $no){
                $tmp = ((int)$no->no_max)+1;
                $no = sprintf("%04s",$tmp);
            }
        }else{
            $no = "0001";
        }
        // date_default_timezone_set('Asia/Jakarta');
        return 'LPB/'.date('m/Y').'/'.$no;
    }

    public function hapusBarang($id){
        $hasil=$this->db->query("DELETE FROM detail_pembelian WHERE id='$id'");
        return $hasil;
    }

	function get_no_invoice(){
        $thn = date("Y");
        $bln = date("m");
        $q = $this->db->query("SELECT MAX(RIGHT(no_pendaftaran,4)) AS kd_max FROM data_pindah WHERE YEAR(tanggal_permohonan)=$thn ");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        $kmt = "PD";
        //date_default_timezone_set('Asia/Jakarta');
        return $kmt.date('Ymd').$kd;
    }

    public function getBarang($kode){
        $hsl=$this->db->query("SELECT * FROM barang WHERE id_barang='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'      => $data->id_barang,
                    'nama_barang'    => $data->nama_barang,
                    );
            }
            return $hasil;
        }
        
    }

    public function insertBeli($data){
        $hasil=$this->db->insert("detail_pembelian",$data);
        return $hasil;
    }

    public function bayarPembelian($data){
        $hasil=$this->db->insert("pembelian",$data);
        return $hasil;
    }


    public function tampilBeli($where){
        $this->db->select('detail_pembelian.*, barang.id_barang, barang.nama_barang, satuan.id id_satuan, satuan.satuan');
        $this->db->from('detail_pembelian');
        $this->db->join('barang','detail_pembelian.id_barang=barang.id_barang','left');
        $this->db->join('satuan','detail_pembelian.id_satuan=satuan.id','left');
        $this->db->where($where);
        $data = $this->db->get();
        return $data->result();
    }

}


