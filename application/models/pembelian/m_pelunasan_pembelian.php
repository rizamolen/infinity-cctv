<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_pelunasan_pembelian extends CI_Model{

    public function getFaktur()
    {
		$this->db->select('*');
        $this->db->from('pembelian');
        $this->db->where("sisa_bayar!='0.00'");
		$data = $this->db->get();
		return $data->result();
    }

    public function detailFaktur($where)
    {
		// $this->db->select('pembelian.*, supplier.*');
        // $this->db->from('pembelian');
        // $this->db->join('supplier','pembelian.id_supplier=supplier.id','left');
        // $this->db->where($where);
        // $hsl = $this->db->get();
        $hsl = $this->db->query("SELECT pembelian.*, supplier.* FROM pembelian LEFT JOIN supplier ON pembelian.id_supplier=supplier.id WHERE $where");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'no_invoice'   => $data->no_invoice,
                    'supplier'   => $data->nama,
                    'jatuh_tempo'   => $data->jatuh_tempo,
                    'grand_total'   => $data->grand_total,
                    'sisa_bayar'   => $data->sisa_bayar,

                    );
            }
        }
        return $hasil;
    }

    public function updateBayar($data,$no_faktur)
    {
        $this->db->where('no_faktur',$no_faktur);
        $query = $this->db->update("pembelian",$data);
        return $query;
    }

}