<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_oprasional_kantor extends CI_Model{

    public function no_faktur()
    {
        $day = date("m");
        $q = $this->db->query("SELECT MAX(RIGHT(no_faktur,4)) AS no_max from pembelian_oprasional WHERE MONTH(tanggal_beli)=$day ");
        $no = "";
        if($q->num_rows()>0){
            foreach($q->result() as $no){
                $tmp = ((int)$no->no_max)+1;
                $no = sprintf("%04s",$tmp);
            }
        }else{
            $no = "0001";
        }
        // date_default_timezone_set('Asia/Jakarta');
        return 'OPS/'.date('m/Y').'/'.$no;
    }

    public function tambahBarang($data){
        $hasil=$this->db->insert("detail_pembelian_oprasional",$data);
        return $hasil;
    }

    public function simpanBayar($data)
    {
        $hasil=$this->db->insert("pembelian_oprasional",$data);
        return $hasil;
    }

    public function tampilBeli($where){
        $this->db->select('detail_pembelian_oprasional.*, satuan.id id_satuan, satuan.satuan');
        $this->db->from('detail_pembelian_oprasional');
        $this->db->join('satuan','detail_pembelian_oprasional.id_satuan=satuan.id','left');
        $this->db->where($where);
        $data = $this->db->get();
        return $data->result();
    }

    public function hapusBarang($id){
        $hasil=$this->db->query("DELETE FROM detail_pembelian_oprasional WHERE id='$id'");
        return $hasil;
    }

}