<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_set_harga extends CI_Model{

   
    public function tampilHarga($id_barang)
    {
        $hsl=$this->db->query("select dp.id_barang, s.satuan ,dp.no_pembelian,(dp.total/dp.qty) modal_harga, ((10/100)*(dp.total/dp.qty)) pajak, (exp.biaya/pb.total_qty) ongkir, (dp.total/dp.qty)+((10/100)*(dp.total/dp.qty))+(exp.biaya/pb.total_qty) total_modal, b.harga_enduser, b.harga_installer from detail_pembelian dp left join pembelian pb on dp.no_pembelian=pb.no_faktur left join jasa_expedisi exp on pb.no_faktur=exp.no_faktur_pembelian left join satuan s on dp.id_satuan=s.id left join barang b on b.id_barang=dp.id_barang where dp.id_barang = '$id_barang' LIMIT 1");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_barang'         => $data->id_barang,
                    'satuan'             => $data->satuan,
                    'modal_harga'       => $data->modal_harga,
                    'pajak'             => $data->pajak,
                    'ongkir'             => $data->ongkir,
                    'total_modal'       => $data->total_modal,
                    'harga_enduser'       => $data->harga_enduser,
                    'harga_installer'       => $data->harga_installer,
                    );
            }
        }
        return $hasil;
    }

    function updateHarga($data , $pilih_barang){
        $this->db->where('id_barang',$pilih_barang);
        $query = $this->db->update("barang",$data);
        return $query;
    }

}