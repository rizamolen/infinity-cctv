<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_barang extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('barang');
		$data = $this->db->get();
		return $data->result();
    }
    
    public function dataBarang()
    {
        $this->db->select('b.*, s.*, g.*');
        $this->db->from('barang b');
        $this->db->join('satuan s','b.id_satuan=s.id','left');
        $this->db->join('grup_barang g','b.kode_group=g.kode','left');
		$data = $this->db->get();
		return $data->result();
    }

    public function getBarang($id)
    {
        $hsl=$this->db->query("SELECT b.*, s.*, g.* FROM barang b left join satuan s on b.id_satuan=s.id left join grup_barang g on b.kode_group=g.kode WHERE b.id_barang='$id'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'kode_group'    => $data->kode_group,
                    'id_barang'     => $data->id_barang,
                    'kode_barang'   => $data->kode_barang,
                    'nama_barang'   => $data->nama_barang,
                    'detail_barang' => $data->detail_barang,
                    'id_satuan'     => $data->id_satuan,
                    'stok_barang'   => $data->stok_barang,
                    'minimum_stok'  => $data->minimum_stok,
                    'harga_enduser'   => $data->harga_enduser,
                    'harga_installer'  => $data->harga_installer,
                    'foto_barang'   => $data->foto_barang,
                    );
            }
        }
        return $hasil;
    }
	
    function insert($data){
        $hasil=$this->db->insert("barang",$data);
        return $hasil;
    }

    function update($data , $id){
            $this->db->where('id_barang',$id);
            $query = $this->db->update("barang",$data);
            return $query;
    }

    function hapus_barang($id){
        $hasil=$this->db->query("DELETE FROM grup_barang WHERE id='$id'");
        return $hasil;
    }

    public function kode_barang($kode)
    {
        $q = $this->db->query("SELECT MAX(RIGHT(kode_barang,4)) AS no_max from barang WHERE substring(kode_barang,1,3)='$kode'");
        $no = "";
        if($q->num_rows()>0){
            foreach($q->result() as $no){
                $tmp = ((int)$no->no_max)+1;
                $no = sprintf("%04s",$tmp);
            }
        }else{
            $no = "0001";
        }
        // date_default_timezone_set('Asia/Jakarta');
        return $kode.$no;
    }

}