<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_supplier extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('supplier');
		$data = $this->db->get();
		return $data->result();
	}

	public function getDataby($id_sup)
	{
        $hsl=$this->db->query("SELECT * FROM supplier WHERE id='$id_sup'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_sup'        => $data ->id,
                    'nama_sup'      => $data ->nama,
                    'alamat_sup'    => $data ->alamat,
                    'notelp_sup'    => $data ->telpon,
                    'email_sup'     => $data ->email
                    );
            }
        }
        return $hasil;
    }

	function insert($data){
        $hasil=$this->db->insert("supplier",$data);
        return $hasil;
	}
	
	function update($data, $id_sup){
        $this->db->where('id', $id_sup);
        $query = $this->db->update("supplier",$data);
        return $query;
	}
	
	function delete($id_sup){
        $hasil=$this->db->query("DELETE FROM supplier WHERE id='$id_sup'");
        return $hasil;
    }

}