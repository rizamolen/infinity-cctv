<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_paket extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('paket');
		$data = $this->db->get();
		return $data->result();
    }
    
    function insert($data){
        $hasil=$this->db->insert("paket",$data);
        return $hasil;
    }
    
    public function getDataby($id)
	{
        $hsl=$this->db->query("SELECT * FROM paket WHERE id='$id'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'            => $data ->id,
                    'nama_paket'   => $data ->nama_paket,
                    'harga'   => $data ->harga
                    );
            }
        }
        return $hasil;
    }

    function update($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update("paket",$data);
        return $query;
    }
    
    function delete($id){
        $hasil=$this->db->query("DELETE FROM akomodasi WHERE id='$id'");
        return $hasil;
    }

}