<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_customer extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('customer');
		$data = $this->db->get();
		return $data->result();
    }

    public function getDataProv()
    {
		$this->db->select('*');
		$this->db->from('provinsi');
		$data = $this->db->get();
		return $data->result();
    }

    function get_kotakabupaten($id){
        $hasil=$this->db->query("SELECT * FROM kotakabupaten WHERE id_provinsi='$id'");
        return $hasil->result();
    }

    public function getDataKota()
    {
		$this->db->select('*');
		$this->db->from('kotakabupaten');
		$data = $this->db->get();
		return $data->result();
    }
    
    function update($data , $id_customer){
        $this->db->where('id_customer',$id_customer);
        $query = $this->db->update("customer",$data);
        return $query;
    }

    function insert($data){
        $hasil=$this->db->insert("customer",$data);
        return $hasil;
    }

    public function getDataby($id_customer)
	{
        $hsl=$this->db->query("SELECT * FROM customer WHERE id_customer='$id_customer'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id_cs'         => $data ->id_customer,
                    'nama_cs'       => $data ->nama_customer,
                    'notelp_cs'     => $data ->no_telp_customer,
                    'nohp_cs'       => $data ->no_hp_customer,
                    'alamat_cs'     => $data ->alamat_customer,
                    'kota_cs'       => $data ->id_kabupaten,
                    'provinsi_cs'   => $data ->id_provinsi,
                    'nofax_cs'      => $data ->fax_customer,
                    'email_cs'      => $data ->email_customer,
                    'jenis_cs'      => $data ->id_jenis_customer
                    );
            }
        }
        return $hasil;
    }

    function delete($id_cs){
        $hasil=$this->db->query("DELETE FROM customer WHERE id_customer='$id_cs'");
        return $hasil;
    }

    public function tampilCs(){
        $this->db->select('customer.*, provinsi.id_provinsi, provinsi.provinsi, kotakabupaten.id, kotakabupaten.nama');
        $this->db->from('customer');
        $this->db->join('provinsi','customer.id_provinsi=provinsi.id_provinsi','left');
        $this->db->join('kotakabupaten','customer.id_kabupaten=kotakabupaten.id','left');
        // $this->db->where($where);
        $data = $this->db->get();
        return $data->result();
    }

}