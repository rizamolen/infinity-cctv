<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_akomodasi extends CI_Model{

    public function getData()
    {
		$this->db->select('*');
		$this->db->from('akomodasi');
		$data = $this->db->get();
		return $data->result();
    }
    
    function insert($data){
        $hasil=$this->db->insert("akomodasi",$data);
        return $hasil;
    }
    
    public function getDataby($id)
	{
        $hsl=$this->db->query("SELECT * FROM akomodasi WHERE id='$id'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'id'            => $data ->id,
                    'kota_tujuan'   => $data ->kota_tujuan,
                    'bbm_roda_2'    => $data ->bbm_roda_2,
                    'bbm_roda_4'    => $data ->bbm_roda_4,
                    'makan'         => $data ->makan,
                    'inap'          => $data ->inap
                    );
            }
        }
        return $hasil;
    }

    function update($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update("akomodasi",$data);
        return $query;
    }
    
    function delete($id){
        $hasil=$this->db->query("DELETE FROM akomodasi WHERE id='$id'");
        return $hasil;
    }

}