<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_lap_piutang_beli extends CI_Model{

    public function getSup()
    {
		$this->db->select('*');
		$this->db->from('supplier');
		$data = $this->db->get();
		return $data->result();
	}
	
	public function dataPembelian()
    {
		$this->db->select('p.*, c.*');
		$this->db->from('penjualan p');
		$this->db->join('customer c','p.id_customer=c.id_customer','left');
		$data = $this->db->get();
		return $data->result();
	}

}