<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_laporan_pembelian extends CI_Model{

    public function getSup()
    {
		$this->db->select('*');
		$this->db->from('supplier');
		$data = $this->db->get();
		return $data->result();
	}
	
	public function dataPembelian()
    {
		$this->db->select('p.*, s.*');
		$this->db->from('pembelian p');
		$this->db->join('supplier s','p.id_supplier=s.id','left');
		$data = $this->db->get();
		return $data->result();
	}

}