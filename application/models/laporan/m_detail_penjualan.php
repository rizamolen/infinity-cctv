<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_detail_penjualan extends CI_Model{

    public function getCustomer()
    {
		$this->db->select('*');
		$this->db->from('customer');
		$data = $this->db->get();
		return $data->result();
	}
	
	public function dataPenjualan()
    {
		$this->db->select('p.*, c.*');
		$this->db->from('penjualan p');
		$this->db->join('customer c','p.id_customer=c.id_customer','left');
		$data = $this->db->get();
		return $data->result();
	}

}