<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_barangpinjaman extends CI_Model{

    public function getgroupBarang()
    {
		$this->db->select('*');
		$this->db->from('grup_barang');
		$data = $this->db->get();
		return $data->result();
	}

	public function b_pinjaman()
    {
		$this->db->select('*');
		$this->db->from('barang_pinjaman');
		$data = $this->db->get();
		return $data->result();
	}

}