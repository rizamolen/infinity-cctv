<?php defined('BASEPATH') OR exit('No direct script access allowed');

class m_masterbarang extends CI_Model{

    public function getgroupBarang()
    {
		$this->db->select('*');
		$this->db->from('grup_barang');
		$data = $this->db->get();
		return $data->result();
	}

	public function dataBarang()
    {
		$this->db->select('*');
		$this->db->from('barang');
		$data = $this->db->get();
		return $data->result();
	}

}