<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_penjualan_barang extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model(array('setup/m_group_barang','master/m_supplier','penjualan/m_penjualan_barang','master/m_barang'));
        $this->load->library(array('form_validation','Mypdf'));
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['no_faktur']              = $this->m_penjualan_barang->no_faktur();
        $data['pilihan_barang']         = $this->m_barang->getData();
        $data['pilihan_customer']       = $this->m_penjualan_barang->getCustomer();
        $data['content']                = 'penjualan/v_penjualan_barang';
        $this->load->view('overview',$data);
    }

    public function detailCustomer()
    {
        $id_customer    = $this->input->post('id_customer');
        $data=$this->m_penjualan_barang->detailCustomer($id_customer);
        echo json_encode($data);
    }

    public function barangBeli()
    {
        $id_barang   =$this->input->post('id');
        $where      = "b.id_barang = '$id_barang'";
        $data       = $this->m_penjualan_barang->barangBeli($where);
        echo json_encode($data);
    }

    public function simpanJual()
    {
        $no_faktur           = $this->input->post('no_faktur');
        $id_barang           = $this->input->post('id_barang');
        $harga_satuan        = $this->input->post('harga_satuan');
        $qty                 = $this->input->post('qty');
        $disc                = $this->input->post('discon');
        $total               = $this->input->post('total');
        $data       = array(
            'no_faktur'      => $no_faktur,
            'id_barang'      => $id_barang,
            'harga'          => $harga_satuan,
            'qty'            => $qty,
            'discon'         => $disc,
            'total'          => $total
        );
        $data=$this->m_penjualan_barang->simpanJual($data);
        echo json_encode($data);
    }

    public function tampilJual()
    {
        $no_faktur  = $this->input->post('no_faktur');
        $where      = "detail_penjualan.no_faktur = '$no_faktur'";
        $data       = $this->m_penjualan_barang->tampilJual($where);
        echo json_encode($data);
    }
    
    public function bayarPenjualan()
    {
        $no_faktur                  = $this->input->post('no_faktur');
        $id_customer                = $this->input->post('id_customer');
        $tanggal_beli               = $this->input->post('tanggal_beli');
        $id_bayar_hasil             = $this->input->post('id_bayar_hasil');
        $bayar_pajak                = $this->input->post('bayar_pajak');
        $total_akhir                = $this->input->post('total_akhir');
        $dibayar                    = $this->input->post('dibayar');
        $sisa_bayar                 = $this->input->post('sisa_bayar');
        $jatuh_tempo                = $this->input->post('jatuh_tempo');
        $id_karyawan_log        	= $this->session->userdata('id_karyawan_log');
        $data       = array(
            'no_faktur'         => $no_faktur,
            'id_customer'       => $id_customer,
            'tanggal_beli'      => $tanggal_beli,
            'total_bayar'       => $id_bayar_hasil,
            'pajak'             => $bayar_pajak,
            'grand_total'       => $total_akhir,
            'pembayaran'        => $dibayar,
            'sisa_bayar'        => $sisa_bayar,
            'jatuh_tempo'       => $jatuh_tempo,
            'id_karyawan'       => $id_karyawan_log

        );
        $data=$this->m_penjualan_barang->bayarPenjualan($data);
        echo json_encode($data);   
    }

    public function cetak()
    {
        $where      = "no_faktur='ARS/INF/03/2020/0001'";
        $data['penjualan']    = $this->m_penjualan_barang->penjualan($where);
        // $data['detail_penjualan']     = $this->m_penjualan_barang->detail_penjualan();
        $file_name='faktur_penjualan';
        $paper='A4';
        $orientation='potrait';
        $this->mypdf->generate('penjualan/cetak_faktur',$data,$file_name,$paper,$orientation);
    }
    
}