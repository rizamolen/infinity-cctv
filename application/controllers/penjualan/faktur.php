<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class faktur extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('m_setup/m_group_barang');
        $this->load->library('form_validation');
    }

    public function index(){
        $data['content']       = 'v_penjualan/v_faktur';
        $this->load->view('overview',$data);
    }
    
    public function get_barang()
    {
        $kode=$this->input->get('kode');
        $data=$this->m_group_barang->getBarang($kode);
        echo json_encode($data);

    }

}