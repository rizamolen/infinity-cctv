<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_oprasional_kantor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('pembelian/m_oprasional_kantor','setup/m_satuan'));
        $this->load->library('form_validation');
    }

    
    public function index()
    {
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['no_faktur']              = $this->m_oprasional_kantor->no_faktur();
        $data['pilih_satuan']           = $this->m_satuan->getData();
        $data['content']                = 'pembelian/v_oprasional_kantor';
        $this->load->view('overview',$data);
    }

    public function tampilBeli()
    {
        $no_faktur   =$this->input->post('no_faktur');
        $where      = "detail_pembelian_oprasional.no_faktur = '$no_faktur'";
        $data       = $this->m_oprasional_kantor->tampilBeli($where);
        echo json_encode($data); 
    }

    public function tambahBarang()
    {
        $no_faktur        = $this->input->post('no_faktur');
        $nama_barang      = $this->input->post('nama_barang');
        $harga_barang     = $this->input->post('harga_barang');
        $satuan_beli      = $this->input->post('satuan_beli');
        $qty              = $this->input->post('qty');
        $total            = $this->input->post('total');
        $data       = array(
            'no_faktur'      => $no_faktur,
            'nama_barang'    => $nama_barang,
            'id_satuan'      => $satuan_beli,
            'harga_barang'   => $harga_barang,
            'qty'            => $qty,
            'total'          => $total
        );
        $data=$this->m_oprasional_kantor->tambahBarang($data);
        echo json_encode($data);
    }

    public function simpanBayar()
    {
        $no_faktur          = $this->input->post('no_faktur');
        $nota_beli          = $this->input->post('nota_beli');
        $supplier           = $this->input->post('supplier');
        $tanggal_beli       = $this->input->post('tanggal_beli');
        $grand_total_bayar  = $this->input->post('grand_total_bayar');
        $dibayar            = $this->input->post('dibayar');
        $sisa_bayar         = $this->input->post('sisa_bayar');
        $data       = array(
            'no_faktur'      => $no_faktur,
            'nota_beli'      => $nota_beli,
            'supplier'       => $supplier,
            'tanggal_beli'   => $tanggal_beli,
            'grand_total'    => $grand_total_bayar,
            'dibayar'        => $dibayar,
            'sisa_bayar'     => $sisa_bayar
        );
        $data=$this->m_oprasional_kantor->simpanBayar($data);
        echo json_encode($data);
    }

    public function hapusBarang(){
        $id=$this->input->post('id');
        $data=$this->m_oprasional_kantor->hapusBarang($id);
        echo json_encode($data);
    }
}