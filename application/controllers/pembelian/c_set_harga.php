<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_set_harga extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('master/m_barang','pembelian/m_set_harga'));
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['content']                = 'pembelian/v_set_harga';
        $data['pilihan_barang']         = $this->m_barang->getData();
        $this->load->view('overview',$data);
    }

    public function tampilHarga()
    {
        $id_barang   = $this->input->post('id_barang');
        $data       = $this->m_set_harga->tampilHarga($id_barang);
        echo json_encode($data);

    }

    public function updateHarga()
    {
        $pilih_barang       = $this->input->post('pilih_barang');
        $harga_end_baru     = $this->input->post('harga_end_baru');
        $harga_ins_baru     = $this->input->post('harga_ins_baru');
        $data   = array(
            'id_barang'      => $pilih_barang,
            'harga_enduser'    => $harga_end_baru,
            'harga_installer'    => $harga_ins_baru,
        );
        $act    = $this->m_set_harga->updateHarga($data,$pilih_barang);
        echo json_encode($act);
    }

}