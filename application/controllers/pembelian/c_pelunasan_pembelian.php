<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_pelunasan_pembelian extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model(array('pembelian/m_pelunasan_pembelian'));
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['no_faktur']              = $this->m_pelunasan_pembelian->getFaktur();
        $data['content']                = 'pembelian/v_pelunasan_pembelian';
        $this->load->view('overview',$data);
    }

    public function detailFaktur()
    {
        $no_faktur  = $this->input->post('no_faktur');
        $where      = "pembelian.no_faktur='$no_faktur'";
        $data       = $this->m_pelunasan_pembelian->detailFaktur($where);
        echo json_encode($data);
    }

    public function updateBayar(){
        $no_faktur  = $this->input->post('no_faktur');
        $pembayaran  = $this->input->post('pembayaran');
        $sisa_akhir  = $this->input->post('sisa_akhir');
        $data   = array(
            'pembayaran'    => $pembayaran,
            'sisa_bayar'    => $sisa_akhir
        );
        $data   = $this->m_pelunasan_pembelian->updateBayar($data,$no_faktur);
        echo json_encode($data);
    }
    
}