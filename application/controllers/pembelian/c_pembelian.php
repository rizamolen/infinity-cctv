<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_pembelian extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model(array('setup/m_group_barang','master/m_supplier','pembelian/m_pembelian','master/m_barang', 'setup/m_satuan'));
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['no_faktur']              = $this->m_pembelian->no_faktur();
        $data['pilihan_satuan']         = $this->m_satuan->getData();
        $data['pilihan_barang']         = $this->m_barang->getData();
        $data['pilihan_supplier']       = $this->m_supplier->getData();
        $data['content']                = 'pembelian/v_pembelian';
        $this->load->view('overview',$data);
    }
    
    public function barangBeli()
    {
        $id_barang      = $this->input->post('id_barang');
        $where          = "b.id_barang = $id_barang ";
        $data           = $this->m_pembelian->getBeli($where);
        echo json_encode($data);
    }

    public function editBarang()
    {
        $id         = $this->input->post('id');
        $where      = "pb.id = '$id'";
        $data       = $this->m_pembelian->editBarang($where);
        echo json_encode($data);
    }

    public function hapusBarang(){
        $id=$this->input->post('id');
        $data=$this->m_pembelian->hapusBarang($id);
        echo json_encode($data);
    }

    public function tampilData()
    {
        $no_faktur  = $this->input->post('no_faktur');
        $where      = "detail_pembelian.no_pembelian = '$no_faktur'";
        $data       = $this->m_pembelian->getData($where);
        echo json_encode($data);
    }

    public function simpanBeli()
    {
        $no_faktur           = $this->input->post('no_faktur');
        $id_barang           = $this->input->post('id_barang');
        $harga_satuan        = $this->input->post('harga_satuan');
        $satuan              = $this->input->post('satuan');
        $qty                 = $this->input->post('qty');
        $disc                = $this->input->post('discon');
        $total               = $this->input->post('total');
        $data       = array(
            'no_pembelian'   => $no_faktur,
            'id_barang'      => $id_barang,
            'id_satuan'      => $satuan,
            'harga_satuan'   => $harga_satuan,
            'qty'            => $qty,
            'disc'           => $disc,
            'total'          => $total
        );
        $data=$this->m_pembelian->insertBeli($data);
        echo json_encode($data);
    }

    public function bayarPembelian(){

        $no_faktur                  = $this->input->post('no_faktur');
        $invoice_beli               = $this->input->post('invoice_beli');
        $total_qty                  = $this->input->post('total_qty');
        $id_supplier                = $this->input->post('id_supplier');
        $tanggal_beli               = $this->input->post('tanggal_beli');
        $id_bayar_hasil             = $this->input->post('id_bayar_hasil');
        $bayar_pajak                = $this->input->post('bayar_pajak');
        $total_akhir                = $this->input->post('total_akhir');
        $dibayar                    = $this->input->post('dibayar');
        $sisa_bayar                 = $this->input->post('sisa_bayar');
        $jatuh_tempo                = $this->input->post('jatuh_tempo');
        $id_karyawan_log        	= $this->session->userdata('id_karyawan_log');
        $data       = array(
            'no_faktur'         => $no_faktur,
            'no_invoice'        => $invoice_beli,
            'total_qty'         => $total_qty,
            'id_supplier'       => $id_supplier,
            'tanggal_beli'      => $tanggal_beli,
            'total_bayar'       => $id_bayar_hasil,
            'pajak'             => $bayar_pajak,
            'grand_total'       => $total_akhir,
            'pembayaran'        => $dibayar,
            'sisa_bayar'        => $sisa_bayar,
            'jatuh_tempo'       => $jatuh_tempo,
            'id_karyawan'       => $id_karyawan_log

        );
        $data=$this->m_pembelian->bayarPembelian($data);
        echo json_encode($data);                
    }

    public function tampilBeli(){
        $no_faktur   =$this->input->post('no_faktur');
        $where      = "detail_pembelian.no_pembelian = '$no_faktur'";
        $data       = $this->m_pembelian->tampilBeli($where);
        echo json_encode($data);
    }

}