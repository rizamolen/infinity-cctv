<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_jasa_expd extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembelian/m_jasa_expd');
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['content']                = 'pembelian/v_jasa_expd';
        $data['datajasa']               = $this->m_jasa_expd->getData();
        $data['faktur_pembelian']       = $this->m_jasa_expd->fakturPembelian();
        $data['no_faktur']              = $this->m_jasa_expd->no_faktur();
        $this->load->view('overview',$data);
    }

    public function simpanData()
    {
        $no_faktur           = $this->input->post('no_faktur');
        $faktur_pembelian    = $this->input->post('faktur_pembelian');
        $nota_expedisi       = $this->input->post('nota_expedisi');
        $nama_expedisi       = $this->input->post('nama_expedisi');
        $biaya_jasa          = $this->input->post('biaya_jasa');
        $id_karyawan         = $this->session->userdata('id_karyawan_log');
        $data                = array(
            'no_faktur'             => $no_faktur,
            'no_faktur_pembelian'   => $faktur_pembelian,
            'no_jasa'               => $nota_expedisi,
            'expedisi'              => $nama_expedisi,
            'biaya'                 => $biaya_jasa,
            'id_karyawan'           => $id_karyawan,
        );
        $act=$this->m_jasa_expd->insert($data);
        echo json_encode($act);
    }

    public function getEdit()
    {
        $id=$this->input->get('id');
        $data=$this->m_jasa_expd->getEdit($id);
        echo json_encode($data);
    }

    public function updateData()
    {
        $no_faktur           = $this->input->post('no_faktur');
        $faktur_pembelian    = $this->input->post('faktur_pembelian');
        $nota_expedisi       = $this->input->post('nota_expedisi');
        $nama_expedisi       = $this->input->post('nama_expedisi');
        $biaya_jasa          = $this->input->post('biaya_jasa');
        $id_karyawan         = $this->session->userdata('id_karyawan_log');
        $data                = array(
            'no_faktur'             => $no_faktur,
            'no_faktur_pembelian'   => $faktur_pembelian,
            'no_jasa'               => $nota_expedisi,
            'expedisi'              => $nama_expedisi,
            'biaya'                 => $biaya_jasa,
            'id_karyawan'           => $id_karyawan,
        );
        $act=$this->m_jasa_expd->updateData($data,$no_faktur);
        echo json_encode($act);
    }

    public function hapusData()
    {
        $no_faktur=$this->input->post('id');
        $data=$this->m_jasa_expd->hapusData($no_faktur);
        echo json_encode($data);
    }

}