<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // load Session Library
        $this->load->library('session');
         
        // load url helper
        $this->load->helper('url');
        $this->load->model('user/m_login');
    }

    public function index()
    {
        $this->load->view("user/v_login");
    }

	function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = "user_app.username ='".$username."' and user_app.password = md5('".$password."')";
		$cek = $this->m_login->login($where);
		if ($cek->num_rows() == 1) {
			foreach ($cek->result() as $sess) {
				//$sess_data['logged_in'] 	= 'Sudah Loggin';
				$sess_data['id_karyawan_log'] 		= $sess->id;
				$sess_data['nama_karyawan_log'] 	= $sess->nama;
				$sess_data['foto_karyawan_log'] 	= $sess->foto;
				$this->session->set_userdata($sess_data);
			}
			redirect(base_url('dashboard'));
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}


	public function logout() 
	{
		$this->session->unset_userdata('id');
		//$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}
}