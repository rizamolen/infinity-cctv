<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_lap_piutang_beli extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('laporan/m_lap_piutang_beli'));
        $this->load->library(array('form_validation','Newpdf','excel'));
    }
    
    public function index()
    {
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['pilih_sup']       = $this->m_lap_piutang_beli->getSup();
        $data['content']          = 'laporan/v_lap_piutang_beli';
        $this->load->view('overview',$data);
    }

    public function exp_pdf()
    {
        $dari_tanggal   = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $jenis_piutang  = $this->input->post('piutang');
        $id_supp    = $this->input->post('id_supp');
        if($jenis_piutang=="jatempo"){
            $main_query= $this->db->query("SELECT *,DATE_ADD(jatuh_tempo)");
            if($id_supp==null){
                $data['data'] = "WHERE p.tanggal_beli between '$dari_tanggal' and '$sampai_tanggal' ";
                $file_name='faktur_penjualan';
                $paper='A4';
                $orientation='potrait';
                $this->newpdf->generate('laporan/pdf_lap_pembelian',$data,$file_name,$paper,$orientation);
            }else{
                $data['data'] = "WHERE p.id_supplier='$id_supp' AND p.tanggal_beli between '$dari_tanggal' and '$sampai_tanggal'";
                $file_name='faktur_penjualan';
                $paper='A4';
                $orientation='potrait';
                $this->newpdf->generate('laporan/pdf_lap_pembelian',$data,$file_name,$paper,$orientation);
            }
        }else{

        }
        
    }

}