<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_barangpinjaman extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('laporan/m_barangpinjaman'));
        $this->load->library(array('form_validation','Newpdf','excel'));
    }
    
    public function index()
    {
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['pilih_groupBarang']       = $this->m_barangpinjaman->getgroupBarang();
        $data['content']                = 'laporan/v_barangpinjaman';
        $this->load->view('overview',$data);
    }

    public function exp_pdf()
    {
        $masterBarang = $this->input->post('masterBarang');
        if($masterBarang==null){
            $data['data']="";
            $file_name='';
            $paper='A4';
            $orientation='potrait';
            $this->newpdf->generate('laporan/pdf_lap_barangpinjaman',$data,$file_name,$paper,$orientation);
        }else{
            $data['data']="WHERE kode_group='$masterBarang'";
            $file_name='';
            $paper='A4';
            $orientation='potrait';
            $this->newpdf->generate('laporan/pdf_lap_barangpinjaman',$data,$file_name,$paper,$orientation);
        }
    }

    public function exp_excel()
    {
        $masterBarang = $this->input->post('masterBarang');

        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        // $object->getSheet(0)->getStyle('A1:G1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $object->getSheet(0)->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $object->getSheet(0)->getStyle('A1:D1')->getFill()->getStartColor()->setRGB('66A5AD');

        $table_columns = array("Kode Barang", "Nama Barang", "Stok Barang", "Detail");
        $column = 0;

        $object->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('D')->setAutoSize(true);


        foreach($table_columns as $field){
          $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
          $column++;
        }


        $employee_data = $this->m_barangpinjaman->b_pinjaman();
        $excel_row = 2;
        foreach($employee_data as $row){
          $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->kode_barang);
          $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_barang);
          $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->stok_barang);
          $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->detail_barang);
          $excel_row++;
        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan Barang Pinjaman.xls"');
        $object_writer->save('php://output');


    }

}