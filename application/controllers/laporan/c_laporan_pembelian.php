<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_laporan_pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('laporan/m_laporan_pembelian'));
        $this->load->library(array('form_validation','Newpdf','excel'));
    }
    
    public function index()
    {
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['pilih_sup']       = $this->m_laporan_pembelian->getSup();
        $data['content']                = 'laporan/v_laporan_pembelian';
        $this->load->view('overview',$data);
    }

    public function exp_pdf()
    {
        $dari_tanggal   = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $id_supp    = $this->input->post('id_supp');
        if($id_supp==null){
            $data['data'] = "WHERE p.tanggal_beli between '$dari_tanggal' and '$sampai_tanggal' ";
            $file_name='faktur_penjualan';
            $paper='A4';
            $orientation='potrait';
            $this->newpdf->generate('laporan/pdf_lap_pembelian',$data,$file_name,$paper,$orientation);
        }else{
            $data['data'] = "WHERE p.id_supplier='$id_supp' AND p.tanggal_beli between '$dari_tanggal' and '$sampai_tanggal'";
            $file_name='faktur_penjualan';
            $paper='A4';
            $orientation='potrait';
            $this->newpdf->generate('laporan/pdf_lap_pembelian',$data,$file_name,$paper,$orientation);
        }
    }

    public function exp_excel()
    {
        $dari_tanggal   = $this->input->post('dari_tanggal');
        $sampai_tanggal = $this->input->post('sampai_tanggal');
        $id_supp        = $this->input->post('id_supp');

        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $object->getSheet(0)->getStyle('A1:F1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $object->getSheet(0)->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $object->getSheet(0)->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('66A5AD');

        $table_columns = array("No_Faktur", "Tanggal_Beli", "Supplier", "Total", "Pajak" ,"Grand Total");
        $column = 0;

        $object->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
        $object->getSheet(0)->getColumnDimension('F')->setAutoSize(true);


        foreach($table_columns as $field){
          $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
          $column++;
        }


        $employee_data = $this->m_laporan_pembelian->dataPembelian();
        $excel_row = 2;
        foreach($employee_data as $row){
          $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->no_faktur);
          $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, date('d M Y', strtotime($row->tanggal_beli)));
          $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama);
          $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->total_bayar);
          $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pajak);
          $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->grand_total);
          $excel_row++;
          $object->getActiveSheet()->setCellValue('F'.$excel_row,  '=SUM(F2:F'.($excel_row -1).')' );
        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan Detail Pembelian.xls"');
        $object_writer->save('php://output');


    }

}