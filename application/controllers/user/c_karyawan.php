<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_karyawan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('user/m_karyawan');
	}

	public function index() {
		//---------------GET SESSION USER----------------//
		$data['id_karyawan_log'] = $this->session->userdata('id_karyawan_log');
		$data['nama_karyawan_log'] = $this->session->userdata('nama_karyawan_log');
		$data['foto_karyawan_log'] = $this->session->userdata('foto_karyawan_log');
		//------------------FORM CONTENT----------------//
        $data['viewData'] = $this->m_karyawan->getData();
        $data['dataJab'] = $this->m_karyawan->getDataJab();
		// echo json_encode($data); die;
        // print_r($data);
        $data['titleHtml'] = 'Data Karyawan';
		$data['content'] = 'user/karyawan/input_karyawan';
		$this->load->view('overview', $data);
	}

    function simpan(){
        $config['upload_path'] 		= './assets/images/karyawan'; //Folder untuk menyimpan hasil upload
        $config['allowed_types'] 	= 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] 		= '3072'; //maksimum besar file 3M
        $config['max_width'] 		= '5000'; //lebar maksimum 5000 px
        $config['max_height']  		= '5000'; //tinggi maksimu 5000 px
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload("gambar");
        $foto               = $this->upload->data('file_name');
        $kode_karyawan      = $this->input->post('kode_karyawan');
        $nama               = $this->input->post('nama');
        $tempat_lahir       = $this->input->post('tempat_lahir');
        $tanggal_lahir      = $this->input->post('tanggal_lahir');
        $jabatan            = $this->input->post('jabatan');
        $alamat             = $this->input->post('alamat');
        $no_hp              = $this->input->post('no_hp');
        $data = array(
            'kode_karyawan' => $kode_karyawan,
            'nama'          => $nama,
            'tempat_lahir'  => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'id_jabatan'    => $jabatan,
            'alamat'        => $alamat,
            'no_hp'         => $no_hp,
            'foto'          => $foto
        );
        $insert	= $this->m_karyawan->insert($data);
        if($insert)
		{
			$this->session->set_flashdata('flash','berhasil');
			redirect(base_url('karyawan'));
		}else{
			$this->session->set_flashdata('flash','gagal');
			redirect(base_url('karyawan'));
		}
    }
    
    public function getData()
    {
        $id=$this->input->get('id');
        $data=$this->m_karyawan->getDataBy($id);
        echo json_encode($data);
    }
    
    public function update(){
        $new_foto = $_FILES['gambar']['name'];
        if($new_foto){
            $kode_karyawan              = $this->input->post('kode_karyawan');
            $config['upload_path'] 		= './assets/images/karyawan'; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] 	= 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] 		= '3072'; //maksimum besar file 3M
            $config['max_width'] 		= '5000'; //lebar maksimum 5000 px
            $config['max_height']  		= '5000'; //tinggi maksimu 5000 px
            $this->load->library('upload');
            $this->upload->initialize($config);
            $this->upload->do_upload("gambar");
            $id			        = $this->input->post('id');
            $foto               = $this->upload->data('file_name');
            $nama               = $this->input->post('nama');
            $tempat_lahir       = $this->input->post('tempat_lahir');
            $tanggal_lahir      = $this->input->post('tanggal_lahir');
            $jabatan            = $this->input->post('jabatan');
            $alamat             = $this->input->post('alamat');
            $no_hp              = $this->input->post('no_hp');
            $data = array(
                'kode_karyawan' => $kode_karyawan,
                'nama'          => $nama,
                'tempat_lahir'  => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'id_jabatan'    => $jabatan,
                'alamat'        => $alamat,
                'no_hp'         => $no_hp,
                'foto'          => $foto
            );
            $insert	= $this->m_karyawan->update($data , $id);
            if($insert){
                $this->session->set_flashdata('flash','berhasil');
                redirect(base_url('karyawan'));
            }else{
                $this->session->set_flashdata('flash','gagal');
			    redirect(base_url('karyawan'));
            }
        }else{
            $id			        = $this->input->post('id');
            $kode_karyawan      = $this->input->post('kode_karyawan');
            $foto               = $this->input->post('hidden_foto');
            $nama               = $this->input->post('nama');
            $tempat_lahir       = $this->input->post('tempat_lahir');
            $tanggal_lahir      = $this->input->post('tanggal_lahir');
            $jabatan            = $this->input->post('jabatan');
            $alamat             = $this->input->post('alamat');
            $no_hp              = $this->input->post('no_hp');
            $data = array(
                'kode_karyawan' => $kode_karyawan,
                'nama'          => $nama,
                'tempat_lahir'  => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'id_jabatan'    => $jabatan,
                'alamat'        => $alamat,
                'no_hp'         => $no_hp,
                'foto'          => $foto
            );
            $update	= $this->m_karyawan->update($data , $id);
            if($update)
            {
                $this->session->set_flashdata('flash','berhasil');
                redirect(base_url('karyawan'));
            }else{
                $this->session->set_flashdata('flash','gagal');
                redirect(base_url('karyawan'));
            }
        }
    }

    // public function hapusData() {
	// 	$id = $this->input->post('id');
	// 	$data = $this->m_karyawan->delete($id);
	// 	echo json_encode($data);
    // }
    
    public function hapusData($id){
        $_id = $this->db->get_where('karyawan',['id' => $id])->row();
        $query = $this->db->delete('karyawan',['id'=>$id]);
        if($query){
            unlink("assets/images/karyawan/".$_id->foto);
        }
        redirect(base_url('karyawan'));
    }
	
}