<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_customer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('master/m_customer');
	}

	public function index() {
		//---------------GET SESSION USER----------------//
		$data['id_karyawan_log'] = $this->session->userdata('id_karyawan_log');
		$data['nama_karyawan_log'] = $this->session->userdata('nama_karyawan_log');
		$data['foto_karyawan_log'] = $this->session->userdata('foto_karyawan_log');
		//------------------FORM CONTENT----------------//
		$data['viewData'] = $this->m_customer->tampilCs();
		$data['dataProv'] = $this->m_customer->getDataProv();
		// $data['dataKota'] = $this->m_customer->getDataKota();
		// echo json_encode($data); die;
		// print_r($data);
		$data['content'] = 'master_data/customer/input';
		$this->load->view('overview', $data);
	}

	function get_kotakabupaten(){
        $id=$this->input->post('id');
        $data=$this->m_customer->get_kotakabupaten($id);
        echo json_encode($data);
    }

	public function getData()
    {
        $id_customer=$this->input->get('id');
        $data=$this->m_customer->getDataBy($id_customer);
        echo json_encode($data);
	}
	
	function update_cs(){
		$id_cs = $this->input->post('id_cs');
        $nama_cs = $this->input->post('nama_cs');
		$notelp_cs = $this->input->post('notelp_cs');
		$nohp_cs = $this->input->post('nohp_cs');
		$alamat_cs = $this->input->post('alamat_cs');
		$provinsi_cs = $this->input->post('provinsi_cs');
		$kota_cs = $this->input->post('kota_cs');
		$nofax_cs = $this->input->post('nofax_cs');
		$email_cs = $this->input->post('email_cs');
		$jenis_cs = $this->input->post('jenis_cs');
        $data   = array(
            'id_customer' => $id_cs,
            'nama_customer' => $nama_cs,
			'no_telp_customer' => $notelp_cs,
			'no_hp_customer' => $nohp_cs,
			'alamat_customer' => $alamat_cs,
			'id_kabupaten' => $kota_cs,
			'id_provinsi' => $provinsi_cs,
			'fax_customer' => $nofax_cs,
			'email_customer' => $email_cs,
			'id_jenis_customer' => $jenis_cs,
        ); 
        $data = $this->m_customer->update($data,$id_cs);
        echo json_encode($data);
    }

	function simpan_cs(){
		$nama_cs 		= $this->input->post('nama_cs');
		$notelp_cs 		= $this->input->post('notelp_cs');
		$nohp_cs 		= $this->input->post('nohp_cs');
		$alamat_cs 		= $this->input->post('alamat_cs');
		$kota_cs 		= $this->input->post('kota_cs');
		$provinsi_cs 	= $this->input->post('provinsi_cs');
		$nofax_cs 		= $this->input->post('nofax_cs');
		$email_cs 		= $this->input->post('email_cs');
		$jenis_cs 		= $this->input->post('jenis_cs');
        $data = array(
            'nama_customer'     => $nama_cs,
			'no_telp_customer' 	=> $notelp_cs,
			'no_hp_customer'   	=> $nohp_cs,
			'alamat_customer'   => $alamat_cs,
			'id_kabupaten'     	=> $kota_cs,
			'id_provinsi'    	=> $provinsi_cs,
			'fax_customer'  	=> $nofax_cs,
			'email_customer'    => $email_cs,
			'id_jenis_customer' => $jenis_cs,
        );
        $data=$this->m_customer->insert($data);
		echo json_encode($data);
	}
	
	function hapusData() {
		$id_cs = $this->input->post('id_cs');
		$data = $this->m_customer->delete($id_cs);
		echo json_encode($data);
	}
}