<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_akomodasi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('master/m_akomodasi');
	}

	public function index() {
		//---------------GET SESSION USER----------------//
		$data['id_karyawan_log'] = $this->session->userdata('id_karyawan_log');
		$data['nama_karyawan_log'] = $this->session->userdata('nama_karyawan_log');
		$data['foto_karyawan_log'] = $this->session->userdata('foto_karyawan_log');
		//------------------FORM CONTENT----------------//
		$data['viewData'] = $this->m_akomodasi->getData();
		// echo json_encode($data); die;
		// print_r($data);
		$data['content'] = 'master_data/akomodasi/input';
		$this->load->view('overview', $data);
	}

    function simpan(){
		$kota_tujuan = $this->input->post('kota_tujuan');
		$bbm_roda_2  = $this->input->post('bbm_roda_2');
		$bbm_roda_4  = $this->input->post('bbm_roda_4');
        $makan       = $this->input->post('makan');
        $inap 	     = $this->input->post('inap');
        $data = array(
            'kota_tujuan'   => $kota_tujuan,
			'bbm_roda_2'    => $bbm_roda_2,
			'bbm_roda_4'    => $bbm_roda_4,
            'makan'         => $makan,
            'inap'          => $inap
        );
        $data=$this->m_akomodasi->insert($data);
		echo json_encode($data);
    }
    
    public function getData()
    {
        $id=$this->input->get('id');
        $data=$this->m_akomodasi->getDataBy($id);
        echo json_encode($data);
    }
    
    function edit(){
		$id          = $this->input->post('id');
        $kota_tujuan = $this->input->post('kota_tujuan');
		$bbm_roda_2  = $this->input->post('bbm_roda_2');
		$bbm_roda_4  = $this->input->post('bbm_roda_4');
        $makan       = $this->input->post('makan');
        $inap 	     = $this->input->post('inap');
        $data   = array(
            'id'            => $id,
            'kota_tujuan'   => $kota_tujuan,
			'bbm_roda_2'    => $bbm_roda_2,
			'bbm_roda_4'    => $bbm_roda_4,
            'makan'         => $makan,
            'inap'          => $inap
        ); 
        $data = $this->m_akomodasi->update($data,$id);
        echo json_encode($data);
    }

    function hapusData() {
		$id = $this->input->post('id');
		$data = $this->m_akomodasi->delete($id);
		echo json_encode($data);
	}
	
}