<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model(array('setup/m_group_barang','setup/m_satuan','master/m_barang'));
    }

    public function index()
    {
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['pilihan_group']          = $this->m_group_barang->getData();
        $data['pilihan_satuan']         = $this->m_satuan->getData();
        $data['data_barang']            = $this->m_barang->dataBarang();
        $data['foto_barang']            = 'default.jpg';
        $data['content']                = 'master_data/barang/tambah_barang';
        $this->load->view('overview',$data);
    }

    public function get_kode()
    {
        $kode       = $this->input->get('kode');
        $data       = $this->m_barang->kode_barang($kode);
        echo json_encode($data);
    }


    public function simpan()
    {
        $kode_barang				= $this->input->post('kode_barang');
        $config['upload_path'] 		= './assets/images/barang'; //Folder untuk menyimpan hasil upload
        $config['allowed_types'] 	= 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] 		= '3072'; //maksimum besar file 3M
        $config['max_width'] 		= '5000'; //lebar maksimum 5000 px
        $config['max_height']  		= '5000'; //tinggi maksimu 5000 px
	    $config['file_name']		= $kode_barang;
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->upload->do_upload("filefoto");
        $foto_barang				= $this->upload->data('file_name');
        $kode_barang				= $this->input->post('kode_barang');
        $kode_group			    	= $this->input->post('group_barang');
        $nama_barang				= $this->input->post('nama_barang');
        $detail_barang				= $this->input->post('detail_barang');
        $satuan_barang				= $this->input->post('satuan_barang');
        $stok_barang				= $this->input->post('stok_barang');
        $minimum_stok				= $this->input->post('minimum_stok');
        $harga_enduserr				= $this->input->post('harga_enduser');
        $harga_enduser				= str_replace(array(".",","),array("","."),$harga_enduserr);
        $harga_installerr			= $this->input->post('harga_installer');
        $harga_installer			= str_replace(array(".",","),array("","."),$harga_installerr);
        $data = array(
            'kode_barang'			=> $kode_barang,
            'kode_group'			=> $kode_group,
			'nama_barang'			=> $nama_barang,
			'detail_barang'	        => $detail_barang,
            'id_satuan'	    	    => $satuan_barang,
			'stok_barang'		    => $stok_barang,
            'minimum_stok'		    => $minimum_stok,
            'harga_enduser'		    => $harga_enduser,
			'harga_installer'		=> $harga_installer,
			'foto_barang'			=> $foto_barang,
		);
		
        $insert	= $this->m_barang->insert($data);
        if($insert)
		{
			$this->session->set_flashdata('flash','berhasil');
			redirect(base_url('masterBarang'));
		}else{
			$this->session->set_flashdata('flash','gagal');
			redirect(base_url('masterBarang'));
		}

    }


    public function update()
    {
        $new_foto		= $_FILES['filefoto']['name'];
		if($new_foto){
            $kode_barang				= $this->input->post('kode_barang');
            $config['upload_path'] 		= './assets/images/barang'; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] 	= 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] 		= '3072'; //maksimum besar file 3M
            $config['max_width'] 		= '5000'; //lebar maksimum 5000 px
            $config['max_height']  		= '5000'; //tinggi maksimu 5000 px
            $config['file_name']		= $kode_barang;
            $this->load->library('upload');
            $this->upload->initialize($config);
            $this->upload->do_upload("filefoto");
            $id			            	= $this->input->post('id_barang');
            $kode_barang				= $this->input->post('kode_barang');
            $kode_group			    	= $this->input->post('group_barang');
            $nama_barang				= $this->input->post('nama_barang');
            $detail_barang				= $this->input->post('detail_barang');
            $satuan_barang				= $this->input->post('satuan_barang');
            $stok_barang				= $this->input->post('stok_barang');
            $minimum_stok				= $this->input->post('minimum_stok');
            $harga_enduserr				= $this->input->post('harga_enduser');
            $harga_enduser				= str_replace(array(".",","),array("","."),$harga_enduserr);
            $harga_installerr			= $this->input->post('harga_installer');
            $harga_installer			= str_replace(array(".",","),array("","."),$harga_installerr);
            $foto_barang				= $this->upload->data('file_name');
            $cek_foto				    = $this->input->post('filefoto');
            $data = array(
                    'kode_barang'			=> $kode_barang,
                    'kode_group'			=> $kode_group,
                    'nama_barang'			=> $nama_barang,
                    'detail_barang'	        => $detail_barang,
                    'id_satuan'	    	    => $satuan_barang,
                    'stok_barang'		    => $stok_barang,
                    'minimum_stok'		    => $minimum_stok,
                    'harga_enduser'		    => $harga_enduser,
                    'harga_installer'		=> $harga_installer,
                    'foto_barang'			=> $foto_barang,
                );
            $insert	= $this->m_barang->update($data , $id);
            if($insert)
            {
                $this->session->set_flashdata('flash','berhasil');
                redirect(base_url('masterBarang'));
            }else{
                $this->session->set_flashdata('flash','gagal');
                redirect(base_url('masterBarang'));
            }
        }else{
            $id			            	= $this->input->post('id_barang');
            $kode_barang				= $this->input->post('kode_barang');
            $kode_group			    	= $this->input->post('group_barang');
            $nama_barang				= $this->input->post('nama_barang');
            $detail_barang				= $this->input->post('detail_barang');
            $satuan_barang				= $this->input->post('satuan_barang');
            $stok_barang				= $this->input->post('stok_barang');
            $minimum_stok				= $this->input->post('minimum_stok');
            $harga_enduserr				= $this->input->post('harga_enduser');
            $harga_enduser				= str_replace(array(".",","),array("","."),$harga_enduserr);
            $harga_installerr			= $this->input->post('harga_installer');
            $harga_installer			= str_replace(array(".",","),array("","."),$harga_installerr);
            $foto_barang				= $this->input->post('hidden_foto');
            $data = array(
                    'kode_barang'			=> $kode_barang,
                    'kode_group'			=> $kode_group,
                    'nama_barang'			=> $nama_barang,
                    'detail_barang'	        => $detail_barang,
                    'id_satuan'	    	    => $satuan_barang,
                    'stok_barang'		    => $stok_barang,
                    'minimum_stok'		    => $minimum_stok,
                    'harga_enduser'		    => $harga_enduser,
                    'harga_installer'		=> $harga_installer,
                    'foto_barang'			=> $foto_barang,
                );
            $insert	= $this->m_barang->update($data , $id);
            if($insert)
            {
                $this->session->set_flashdata('flash','berhasil');
                redirect(base_url('masterBarang'));
            }else{
                $this->session->set_flashdata('flash','gagal');
                redirect(base_url('masterBarang'));
            }
        }

    }


    public function getBarang()
    {
        $id=$this->input->post('id');
        $data=$this->m_barang->getBarang($id);
        echo json_encode($data);
    }

}