<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_paket extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('master/m_paket');
	}

	public function index() {
		//---------------GET SESSION USER----------------//
		$data['id_karyawan_log'] = $this->session->userdata('id_karyawan_log');
		$data['nama_karyawan_log'] = $this->session->userdata('nama_karyawan_log');
		$data['foto_karyawan_log'] = $this->session->userdata('foto_karyawan_log');
		//------------------FORM CONTENT----------------//
		$data['viewData'] = $this->m_paket->getData();
		// echo json_encode($data); die;
		// print_r($data);
		$data['content'] = 'master_data/paket/input';
		$this->load->view('overview', $data);
	}

    function simpan(){
        $nama_paket = $this->input->post('nama_paket');
        $harga = $this->input->post('harga');
        $data = array(
            'nama_paket'   => $nama_paket,
            'harga'   => $harga
        );
        $data=$this->m_paket->insert($data);
		echo json_encode($data);
    }
    
    public function getData()
    {
        $id=$this->input->get('id');
        $data=$this->m_paket->getDataBy($id);
        echo json_encode($data);
    }
    
    function edit(){
		$id          = $this->input->post('id');
        $nama_paket = $this->input->post('nama_paket');
        $harga = $this->input->post('harga');
        $data   = array(
            'id'            => $id,
            'nama_paket'   => $nama_paket,
            'harga'   => $harga
        ); 
        $data = $this->m_paket->update($data,$id);
        echo json_encode($data);
    }

    function hapusData() {
		$id = $this->input->post('id');
		$data = $this->m_akomodasi->delete($id);
		echo json_encode($data);
	}
	
}