<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_supplier extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('master/m_supplier');
	}

	public function index() {
		//---------------GET SESSION USER----------------//
		$data['id_karyawan_log'] = $this->session->userdata('id_karyawan_log');
		$data['nama_karyawan_log'] = $this->session->userdata('nama_karyawan_log');
		$data['foto_karyawan_log'] = $this->session->userdata('foto_karyawan_log');
		//------------------FORM CONTENT----------------//
		$data['viewData'] = $this->m_supplier->getData();
		// echo json_encode($data); die;
		// print_r($data);
		$data['content'] = 'master_data/supplier/input';
		$this->load->view('overview', $data);
	}

	public function getData()
    {
        $id_sup=$this->input->get('id');
        $data=$this->m_supplier->getDataBy($id_sup);
        echo json_encode($data);
	}
	
	function update_sup(){
		$id_sup = $this->input->post('id_sup');
        $nama_sup = $this->input->post('nama_sup');
		$alamat_sup = $this->input->post('alamat_sup');
		$notelp_sup = $this->input->post('notelp_sup');
		$email_sup = $this->input->post('email_sup');
        $data   = array(
            'id' => $id_sup,
            'nama' => $nama_sup,
			'alamat' => $alamat_sup,
			'telpon' => $notelp_sup,
			'email' => $email_sup
        ); 
        $data = $this->m_supplier->update($data,$id_sup);
        echo json_encode($data);
    }

	function simpan_supp(){
		$nama_sup 		= $this->input->post('nama_sup');
		$alamat_sup 	= $this->input->post('alamat_sup');
		$notelp_sup 	= $this->input->post('notelp_sup');
		$email_sup 		= $this->input->post('email_sup');
        $data = array(
            'nama'   => $nama_sup,
			'alamat' => $alamat_sup,
			'telpon' => $notelp_sup,
			'email'  => $email_sup
        );
        $data=$this->m_supplier->insert($data);
		echo json_encode($data);
	}
	
	function hapusData() {
		$id_sup = $this->input->post('id_sup');
		$data = $this->m_supplier->delete($id_sup);
		echo json_encode($data);
	}
}