<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_group_barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setup/m_group_barang');
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['content']       = 'setup/v_group_barang';
        $data['data_group']    = $this->m_group_barang->getData();
        $this->load->view('overview',$data);
    }

    public function tampilData()
    {
        $list       = $this->m_group_barang->getData();
        $data       = array();
        $no         = $_POST['start'];
        foreach ($list as $person){
            $no++;
            $row = array();
            $row[] = $person->id;
            $row[] = $person->nama;
            $row[] = $person->nama;

            $data[] = $row;
        }
        $output = array(
            "draw"  => $_POST['draw'],
        );
        echo json_encode($output);
    }

    public function getData()
    {
        $kode=$this->input->get('id');
        $data=$this->m_group_barang->getDataBy($kode);
        echo json_encode($data);

    }
    
    function update_barang(){
        $kode           = $this->input->post('kode');
        $nama_group     = $this->input->post('nama_group');
        $data   = array(
            'kode'          => $kode,
            'nama_group'    => $nama_group,

        ); 
        $data   = $this->m_group_barang->update_barang($data,$kode);
        echo json_encode($data);
    }

    public function cek_kode()
    {
        $kode=$this->input->get('kode_group');
        $data=$this->m_group_barang->getKode($kode);
        echo json_encode($data);

    }

    function simpan_barang(){
        $kode           = $this->input->post('kode');
        $nama_group     = $this->input->post('nama_group');
        $data       = array(
            'kode'          => $kode,
            'nama_group'    => $nama_group,
        );
        $data=$this->m_group_barang->insert($data);
        echo json_encode($data);
    }

    function hapusData(){
        $kode=$this->input->post('kode');
        $data=$this->m_group_barang->hapus_barang($kode);
        echo json_encode($data);
    }


}