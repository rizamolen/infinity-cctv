<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_satuan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('setup/m_satuan');
        $this->load->library('form_validation');
    }

    public function index(){
        //---------------GET SESSION USER----------------//
        $data['id_karyawan_log']		= $this->session->userdata('id_karyawan_log');
        $data['nama_karyawan_log'] 	    = $this->session->userdata('nama_karyawan_log');
        $data['foto_karyawan_log'] 		= $this->session->userdata('foto_karyawan_log');
        //------------------FORM CONTENT----------------//
        $data['content']                = 'setup/v_satuan';
        $data['data_satuan']            = $this->m_satuan->getData();
        $this->load->view('overview',$data);
    }

    public function tampilData()
    {
        $data       = $this->m_satuan->getData();
        echo json_encode($data);
    }

    public function getData()
    {
        $kode=$this->input->get('id');
        $data=$this->m_satuan->getDataBy($kode);
        echo json_encode($data);

    }
    
    function update_barang(){
        $id         = $this->input->post('id');
        $satuan     = $this->input->post('satuan');
        $data   = array(
            'id '  => $id    ,
            'satuan'  => $satuan,
        ); 
        $data   = $this->m_satuan->update_barang($data,$id);
        echo json_encode($data);
    }

    public function cek_kode()
    {
        $kode=$this->input->get('satuan');
        $data=$this->m_satuan->getKode($kode);
        echo json_encode($data);

    }

    function simpan_barang(){
        $satuan       =$this->input->post('satuan');
        $data       = array(
            'satuan'  => $satuan
        );
        $data=$this->m_satuan->insert($data);
        echo json_encode($data);
    }

    function hapusData(){
        $id=$this->input->post('id');
        $data=$this->m_satuan->hapus_barang($id);
        echo json_encode($data);
    }


}