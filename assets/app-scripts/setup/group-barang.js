$(document).ready(function(){
    
    $('.dataTable').DataTable();

    $('#btn_update').attr('hidden',true); 

    //VALIDASI KODE GROUP
    var cek_g = document.getElementById('cek_group');
    cek_g.addEventListener('keyup', function() {
        var kode = $('[name="kode_group"]').val();
        $.ajax({
            type : 'GET',
            url : 'setup/c_group_barang/cek_kode',
            async : true,
            dataType : 'json',
            data : {kode_group:kode},
            success : function(data){
                Swal.fire(
                    'Kode Group Sudah Ada!',
                    'tutup informasi',
                    'warning'
                    );
            }
        });
    });

    //MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
    var kode        = $('[name="kode_group"]').val();
    var nama_group  = $('[name="nama_group"]').val();
    if(kode==""){
        Swal.fire(
            'Kode Group Tidak Boleh Kosong!',
            'tutup informasi',
            'warning'
        );
    }else if(nama_group==""){
        Swal.fire(
            'Nama Group Tidak Boleh Kosong!',
            'tutup informasi',
            'warning'
        );
    }else{
        $.ajax({
            type : "POST",
            url : 'setup/c_group_barang/simpan_barang',
            dataType : "JSON",
            data : {kode:kode, nama_group:nama_group},
            success: function(data){
                Swal.fire(
                    'Datas Bershasil Disimpan!',
                    'tutup informasi',
                    'success'
                    );
                    $('[name="nama_group"]').val("");
                    $('[name="kode_group"]').val(""); 
                    $('#btn_simpan').attr('hidden',false);
                    $('#btn_update').attr('hidden',true);
                    $('.dataTable').load(" .dataTable ");
            }
        });
        return false;
    }
    });

    ////MEMBUAT FUNGSI GET DATA
    $('.dataTable').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'setup/c_group_barang/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="nama_group"]').val(data.nama_group);
                    $('[name="kode_group"]').val(data.kode);
                    $('[name="kode_group"]').attr('readonly',true);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                    $
                });
            }
        });
        return false;
    });

    //Reset Input
    $('#btn_reset').on('click', function(){
        $('[name="nama_group"]').val("");
        $('[name="kode_group"]').val("");
        $('[name="kode_group"]').attr('readonly',false); 
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });


    //Update Barang
    $('#btn_update').on('click',function(){

        var nama_group = $('[name="nama_group"]').val();
        var kode = $('[name="kode_group"]').val();
        $.ajax({
            type : "POST",
            url  : "setup/c_group_barang/update_barang",
            dataType : "JSON",
            data : {kode:kode , nama_group:nama_group},
            success: function(data){
                Swal.fire(
                    'Datas Bershasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="nama_group"]').val("");
                $('[name="kode_group"]').val("");
                $('[name="kode_group"]').attr('readonly',false);  
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('.dataTable').load(" .dataTable ");
            }
        });
        return false;
    });
     
    ////MEMBUAT FUNGSI DELETE DATA
    $('.dataTable').on('click','.item_delete',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin akan menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var kode=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'setup/c_group_barang/hapusData',
                    dataType : "JSON",
                    data : {kode:kode},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('.dataTable').load(" .dataTable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

});
