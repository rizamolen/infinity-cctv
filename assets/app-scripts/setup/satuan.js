$(document).ready(function(){
    
    var tabelData = $('#mydata').dataTable({
        
    });

    $('#btn_update').attr('hidden',true); 

    //VALIDASI KODE GROUP
    var cek_g = document.getElementById('nama_satuan');
    cek_g.addEventListener('keyup', function() {
        var satuan = $('[name="nama_satuan"]').val();
        $.ajax({
            type : 'GET',
            url : 'setup/c_satuan/cek_kode',
            async : true,
            dataType : 'json',
            data : {satuan:satuan},
            success : function(data){
                Swal.fire(
                    'Satuan Barang Sudah Ada!',
                    'tutup informasi',
                    'warning'
                    );
            }
        });
    });

    //MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
    var satuan = $('[name="nama_satuan"]').val();
    if(satuan==""){
        Swal.fire(
            'Satuan Barang Tidak Boleh Kosong!',
            'tutup informasi',
            'warning'
        );
    }else{
        $.ajax({
            type : "POST",
            url : 'setup/c_satuan/simpan_barang',
            dataType : "JSON",
            data : {satuan:satuan},
            success: function(data){
                Swal.fire(
                    'Datas Bershasil Disimpan!',
                    'tutup informasi',
                    'success'
                    );
                    $('[name="nama_satuan"]').val("");
                    $('#btn_simpan').attr('hidden',false);
                    $('#btn_update').attr('hidden',true);
                    $('.dataTable').load(" .dataTable ");
            }
        });
        return false;
    }
    });

    ////MEMBUAT FUNGSI GET DATA
    $('.dataTable').on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'setup/c_satuan/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id_satuan"]').val(data.id);
                    $('[name="nama_satuan"]').val(data.satuan);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                });
            }
        });
        return false;
    });

    //Reset Input
    $('#btn_reset').on('click', function(){
        $('[name="id_satuan"]').val("");
        $('[name="nama_satuan"]').val(""); 
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });


    //Update Barang
    $('#btn_update').on('click',function(){
        var id = $('[name="id_satuan"]').val();
        var satuan = $('[name="nama_satuan"]').val();
        $.ajax({
            type : "POST",
            url  : "setup/c_satuan/update_barang",
            dataType : "JSON",
            data : {id:id , satuan:satuan},
            success: function(data){
                Swal.fire(
                    'Datas Bershasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="id_satuan"]').val("");
                $('[name="nama_satuan"]').val("");  
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('.dataTable').load(" .dataTable ");
            }
        });
        return false;
    });
     
    ////MEMBUAT FUNGSI DELETE DATA
    $('.dataTable').on('click','.item_delete',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin akan menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var id=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'setup/c_satuan/hapusData',
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('.dataTable').load(" .dataTable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

});
