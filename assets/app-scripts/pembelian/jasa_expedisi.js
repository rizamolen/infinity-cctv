$(document).ready(function(){
    
$('.dataTable').DataTable();
$('#faktur_pembelian').select2();
$('#btn_update').attr('hidden',true);

//Reset Input
$('#btn_reset').on('click', function(){
    $('[name="faktur_pembelian"]').val("");
    $('[name="nota_expedisi"]').val("");
    $('[name="nama_expedisi"]').val("");
    $('[name="biaya_jasa"]').val("");
    $('#btn_simpan').attr('hidden',false);
    $('#btn_update').attr('hidden',true);  
});


$('#btn_simpan').on('click', function(){
    var no_faktur           = $('[name="no_faktur"]').val();
    var faktur_pembelian    = $('[name="faktur_pembelian"]').val();
    var nota_expedisi       = $('[name="nota_expedisi"]').val();
    var nama_expedisi       = $('[name="nama_expedisi"]').val();
    var biaya_jasa          = $('[name="biaya_jasa"]').val();
    $.ajax({
		type : "POST",
		url : 'pembelian/c_jasa_expd/simpanData',
		dataType : "JSON",
		data : {no_faktur:no_faktur,faktur_pembelian:faktur_pembelian ,nota_expedisi:nota_expedisi, nama_expedisi:nama_expedisi, biaya_jasa:biaya_jasa},
		success: function(data){
			Swal.fire(
				'Data Pembelian Bershasil Disimpan!',
				'tutup informasi',
				'success'
				);
			location.reload(true);
		}        
    });
    return false;
});

////MEMBUAT FUNGSI GET DATA
$('.dataTable').on('click','.item_edit',function(){
    var id=$(this).attr('data');
    $.ajax({
        type : "GET",
        url : 'pembelian/c_jasa_expd/getEdit',
        dataType : "JSON",
        data : {id:id},
        success: function(data){
            $.each(data,function(){
                $('[name="no_faktur"]').val(data.no_faktur);
                $('[name="faktur_pembelian"]').val(data.no_faktur_pembelian).trigger('change');
                $('[name="nota_expedisi"]').val(data.no_jasa);
                $('[name="nama_expedisi"]').val(data.expedisi);
                $('[name="biaya_jasa"]').val(data.biaya);
                $('#btn_update').attr('hidden',false); 
                $('#btn_simpan').attr('hidden',true); 
                $
            });
        }
    });
    return false;
});


//Update Barang
$('#btn_update').on('click',function(){
    var no_faktur           = $('[name="no_faktur"]').val();
    var faktur_pembelian    = $('[name="faktur_pembelian"]').val();
    var nota_expedisi       = $('[name="nota_expedisi"]').val();
    var nama_expedisi       = $('[name="nama_expedisi"]').val();
    var biaya_jasa          = $('[name="biaya_jasa"]').val();
    $.ajax({
        type : "POST",
        url  : "pembelian/c_jasa_expd/updateData",
        dataType : "JSON",
        data : {no_faktur:no_faktur,faktur_pembelian:faktur_pembelian ,nota_expedisi:nota_expedisi, nama_expedisi:nama_expedisi, biaya_jasa:biaya_jasa},
        success: function(data){
            Swal.fire(
                'Datas Bershasil Diupdate!',
                'tutup informasi',
                'success'
            );
            location.reload(true);
        }
    });
    return false;
});


////MEMBUAT FUNGSI DELETE DATA
$('.dataTable').on('click','.item_delete',function(){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      swalWithBootstrapButtons.fire({
        title: 'Anda yakin akan menghapus?',
        text: "data akan hilang permanen dari sistem!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
            var id=$(this).attr('data');
            $.ajax({
                type : "POST",
                url : 'pembelian/c_jasa_expd/hapusData',
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    swalWithBootstrapButtons.fire(
                        'Di Hapus!',
                        'Data telah dihapus dari aplikasi',
                        'success'
                      )
                      $('.dataTable').load(" .dataTable ");
                }
            });      
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Batal',
            'Data batal dihapus :)',
            'error'
          )
        }
      })
});


});