$(document).ready(function(){
    
$('#pilih_barang').select2();

$('#pilih_barang').on('change',function(){
    var id_barang = $('[name="pilih_barang"]').val();
    $.ajax({
        type : "POST",
        url : 'pembelian/c_set_harga/tampilHarga',
        dataType : "JSON",
        data : {id_barang:id_barang},
        success : function(data){
            $.each(data,function(){
                $('[name="lama_enduser"]').val(convertToRupiah(parseFloat(data.harga_enduser).toFixed(2)));
                $('[name="lama_installer"]').val(convertToRupiah(parseFloat(data.harga_installer).toFixed(2)));
                $('[name="modal_harga"]').val(convertToRupiah(parseFloat(data.modal_harga).toFixed(2)));
                $('[name="pajak"]').val(convertToRupiah(parseFloat(data.pajak).toFixed(2)));
                $('[name="ongkir"]').val(convertToRupiah(parseFloat(data.ongkir).toFixed(2)));
                $('[name="total_modal"]').val(convertToRupiah(parseFloat(data.total_modal).toFixed(2)));
                $('#satuan').text(data.satuan);
            });
        }
    });
});

});


function convertToRupiah(angka)
{
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');   
    for(var i = 0; i < angkarev.length; i++) 
      if(i%3 == 0) rupiah += (angkarev.substr(i,3)+'.').replace('..',',');
    return rupiah.split('',rupiah.length-1).reverse().join('');

}

function formatRupiah(angka)
{
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split	= number_string.split(','),
		sisa 	= split[0].length % 3,
		rupiah 	= split[0].substr(0, sisa),
		ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	
	return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	// return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

var kecil_end_baru = document.getElementById('harga_end_baru');
kecil_end_baru.addEventListener('keyup', function(e)
{
	kecil_end_baru.value = formatRupiah(this.value);
});

var besar_end_baru = document.getElementById('harga_ins_baru');
besar_end_baru.addEventListener('keyup', function(e)
{
	besar_end_baru.value = formatRupiah(this.value);
});


$('#btn_simpan').on('click', function(){
    var pilih_barang = $('[name="pilih_barang"]').val();
    var harga_end_baru = $('[name="harga_end_baru"]').val().replace(".", "").replace(".", "").replace(",", ".");;
    var harga_ins_baru = $('[name="harga_ins_baru"]').val().replace(".", "").replace(".", "").replace(",", ".");;
    $.ajax({
        type : "POST",
        url  : "pembelian/c_set_harga/updateHarga",
        dataType : "JSON",
        data : {pilih_barang:pilih_barang , harga_end_baru:harga_end_baru, harga_ins_baru:harga_ins_baru},
        success: function(data){
            Swal.fire(
                'Datas Bershasil Diupdate!',
                'tutup informasi',
                'success'
            );
            location.reload(true);

        }
    });
});
