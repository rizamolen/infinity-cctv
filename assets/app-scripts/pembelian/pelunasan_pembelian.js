function formatRupiah(angka)
{
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split	= number_string.split(','),
		sisa 	= split[0].length % 3,
		rupiah 	= split[0].substr(0, sisa),
		ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	
	return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	// return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}


function convertToRupiah(angka)
{

    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    
    for(var i = 0; i < angkarev.length; i++) 
      if(i%3 == 0) rupiah += (angkarev.substr(i,3)+'.').replace('..',',');

    
    return rupiah.split('',rupiah.length-1).reverse().join('');

}

var tanpa_rupiah = document.getElementById('pembayaran');
tanpa_rupiah.addEventListener('keyup', function(e)
{
	tanpa_rupiah.value = formatRupiah(this.value);
});


var pembayaran = document.getElementById('pembayaran');
pembayaran.addEventListener('keyup', function() {
    var sisa_bayar = $("#sisa_bayar").val().replace(".", "").replace(".", "").replace(",", ".");;
    var pembayaran = $("#pembayaran").val().replace(".", "").replace(".", "").replace(",", ".");
    if(pembayaran > sisa_bayar){
        Swal.fire(
            'Pembayaran Terlalu Banyak!',
            'tutup informasi',
            'warning'
            );
            $("#pembayaran").val("0");    
    }
    var total = sisa_bayar-pembayaran;
    var convertTot = convertToRupiah(parseFloat(total).toFixed(2));
    $("#sisa_akhir").val(convertTot);
});

$(document).ready(function () {
    $("#no_faktur").select2();

    $('#no_faktur').on('change',function(){
        var no_faktur = $('[name="no_faktur"]').val();
        $.ajax({
            type : "POST",
            url : 'pembelian/c_pelunasan_pembelian/detailFaktur',
            dataType : "JSON",
            data : {no_faktur:no_faktur},
            success : function(data){
                $.each(data,function(){
                    $('[name="no_invoice"]').val(data.no_invoice);
                    $('[name="supplier"]').val(data.supplier);
                    $('[name="jatuh_tempo"]').val(data.jatuh_tempo);
                    $('[name="grand_total"]').val(convertToRupiah(data.grand_total));
                    $('[name="sisa_bayar"]').val(convertToRupiah(data.sisa_bayar));
                });
                dataPembelian();
            }
        });
    });
});

function dataPembelian(){
    var no_faktur = $('[name="no_faktur"]').val();
    var no =0;
    $.ajax({
        type : "POST",
        url : 'pembelian/c_pembelian/tampilBeli',
        dataType : "JSON",
        data : {no_faktur:no_faktur},
        success: function(data){
            
            var html = '';
            var i;
            var total = '';
            for(i=0; i<data.length; i++){
                no++;
                html += '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+data[i].id_barang+'</td>'+
                        '<td>'+data[i].nama_barang+'</td>'+
                        '<td>'+convertToRupiah(data[i].harga_satuan)+'</td>'+
                        '<td>'+data[i].qty+'  '+data[i].satuan+'</td>'+
                        // '<td>'+data[i].satuan+'</td>'+
                        '<td>'+(data[i].disc).replace('.',',')+'</td>'+
                        '<td>'+convertToRupiah(data[i].total)+'</td>'+
                        '<td hidden class="count-me">'+data[i].total+'</td>'+
                        '</tr>';
            }
           
            $('#data_beli').html(html);
        }
    });


    $('#pelunasan').on('click', function(){
        var no_faktur =  $('[name="no_faktur"]').val();
        var pembayaran = $('[name="pembayaran"]').val();
        var sisa_akhir = $('[name="sisa_akhir"]').val();
        $.ajax({
            type : "POST",
            url  : "pembelian/c_pelunasan_pembelian/updateBayar",
            dataType : "JSON",
            data : {no_faktur:no_faktur , pembayaran:pembayaran, sisa_akhir:sisa_akhir},
            success: function(data){
                Swal.fire(
                    'Data Pelunasan Pembelian Bershasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                location.reload(true);
            }
        });
        return false;
    });

}