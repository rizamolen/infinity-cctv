tampilDataBeli();

function convertToRupiah(angka)
{
    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) 
      if(i%3 == 0) rupiah += (angkarev.substr(i,3)+'.').replace('..',',');    
    return rupiah.split('',rupiah.length-1).reverse().join('');
}

function formatRupiah(angka)
{
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split	= number_string.split(','),
		sisa 	= split[0].length % 3,
		rupiah 	= split[0].substr(0, sisa),
		ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
}

function number_format(angka){
	var number_string = angka.replace(/[^,\d]/g, '').toString();
	return number_string;
}


var dibayar = document.getElementById('dibayar');
dibayar.addEventListener('keyup', function(e)
{
	dibayar.value = formatRupiah(this.value);
});

var harga = document.getElementById('harga_barang');
harga.addEventListener('keyup', function(e)
{
	harga.value = formatRupiah(this.value);
});

var qry_total = document.getElementById('qty');
qry_total.addEventListener('keyup', function(e){
    var harga = $('[name="harga_barang"]').val().replace(".", "").replace(".", "").replace(",", ".");
    var qty = $('[name="qty"]').val();
	var total = harga*qty;
	var hasil = convertToRupiah(parseFloat(total).toFixed(2));
    $("#total").val(hasil);

});

var qry_harga = document.getElementById('harga_barang');
qry_harga.addEventListener('keyup', function(e){
    var harga = $('[name="harga_barang"]').val().replace(".", "").replace(".", "").replace(",", ".");
    var qty = $('[name="qty"]').val();
    var total = harga*qty;
    $("#total").val(convertToRupiah(total));

});


var dibayar = document.getElementById('dibayar');
dibayar.addEventListener('keyup', function(e){
    var grand_total_bayar = $('[name="grand_total_bayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
    var dibayar = $('[name="dibayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
    var total = grand_total_bayar-dibayar;
    $("#sisa_bayar").val(convertToRupiah(total));

});

// var qry_discon = document.getElementById('discon');
// qry_discon.addEventListener('keyup', function(e){
//     var harga = $('[name="harga_barang"]').val().replace(".", "").replace(".", "").replace(",", ".");
// 	var qty = $('[name="qty"]').val();
// 	var discon = $('[name="discon"]').val();
// 	var total = harga*qty;
// 	var hasil_discon = total-discon;
//     $("#total").val(convertToRupiah(hasil_discon));

// });

$('#pembayaran_akhir').on('click', function(){
	var no_faktur 			= $('[name="no_faktur"]').val();
	var nota_beli 			= $('[name="nota_beli"]').val();
	var supplier 			= $('[name="supplier"]').val();
	var tanggal_beli 		= $('[name="tanggal_beli"]').val();
	var grand_total_bayar 	= $('[name="grand_total_bayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
	var dibayar 			= $('[name="dibayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
	var sisa_bayar 			= $('[name="sisa_bayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
	$.ajax({
		type : "POST",
		url : 'pembelian/c_oprasional_kantor/simpanBayar',
		dataType : "JSON",
		data : {no_faktur:no_faktur,nota_beli:nota_beli ,supplier:supplier, tanggal_beli:tanggal_beli, grand_total_bayar:grand_total_bayar, dibayar:dibayar,sisa_bayar:sisa_bayar},
		success: function(data){
			$('#default-Modal').modal('hide');   
			Swal.fire(
				'Data Pembelian Bershasil Disimpan!',
				'tutup informasi',
				'success'
				);
			location.reload(true);
		}
	});
	return false;
});

$('#btn_simpan').on('click',function(){
	var no_faktur 		= $('[name="no_faktur"]').val();
	var nama_barang 	= $('[name="nama_barang"]').val();
	var harga_barang 	= $('[name="harga_barang"]').val().replace(".", "").replace(".", "").replace(",", ".");;
	var satuan_beli 	= $('[name="satuan_beli"]').val();
	var qty 			= $('[name="qty"]').val();
	var total 			= $('[name="total"]').val().replace(".", "").replace(".", "").replace(",", ".");
	$.ajax({
		type : "POST",
		url : 'pembelian/c_oprasional_kantor/tambahBarang',
		dataType : "JSON",
		data : {no_faktur:no_faktur ,nama_barang:nama_barang , harga_barang:harga_barang, satuan_beli:satuan_beli, qty:qty, total:total},
		success: function(data){
			$('[name="nama_barang"]').val("");
			$('[name="harga_barang"]').val("");
			$('[name="satuan_beli"]').val("");
			$('[name="qty"]').val("0");
			$('[name="total"]').val("");
			$('#Modal-Barang').modal('hide');
			tampilDataBeli();
		}
	});
	return false;
});

function tampilDataBeli()
{
	var no_faktur       = $('[name="no_faktur"]').val();
	var no =0;
	$.ajax({
		type : "POST",
		url : 'pembelian/c_oprasional_kantor/tampilBeli',
		dataType : "JSON",
		data : {no_faktur:no_faktur},
		success: function(data){
			no++;
			var html = '';
			var i;
			var total = '';
			for(i=0; i<data.length; i++){
				no++;
				html += '<tr>'+
						'<td>'+no+'</td>'+
						'<td>'+data[i].nama_barang+'</td>'+
						'<td>'+convertToRupiah(data[i].harga_barang)+'</td>'+
						'<td>'+data[i].qty+'  '+data[i].satuan+'</td>'+
						'<td>'+convertToRupiah(data[i].total)+'</td>'+
						'<td hidden class="count-me">'+data[i].total+'</td>'+
						'<td style="text-align:center;">'+
							'<a href="javascript:;" class="btn btn-sm btn-danger btn-xs item_delete" data="'+data[i].id+'">'+'<i class="fa fa-trash"></i>'+'</a>'+
						'</td>'+
						'</tr>';
			}
		   
			$('#data_beli').html(html);
			var tds = document.getElementById('tabel_barang').getElementsByTagName('td');
			var sum = 0;
			for(var i = 0; i < tds.length; i ++) {
				if(tds[i].className == 'count-me') {
					sum += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
				}
			}
			$('#grand_total').html(convertToRupiah(parseFloat(sum).toFixed(2)));
			$('#grand_total_bayar').val(convertToRupiah(parseFloat(sum).toFixed(2)));
		}
		
	});

}


$('.dataTable').on('click','.item_delete',function(){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Anda yakin akan menghapus?',
        text: "data akan hilang permanen dari sistem!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
            var id=$(this).attr('data');
            $.ajax({
                type : "POST",
                url : 'pembelian/c_oprasional_kantor/hapusBarang',
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    swalWithBootstrapButtons.fire(
                        'Di Hapus!',
                        'Data telah dihapus dari aplikasi',
                        'success'
                      )
                      tampilDataBeli();
                }
                
            });      
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Batal',
            'Data batal dihapus :)',
            'error'
          )
        }
      })
});


