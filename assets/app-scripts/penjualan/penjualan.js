
tampilDataBeli();

$(document).ready(function () {
    $("#id_supplier").select2();
    $("#pilih_barang").select2();

    $('#pilih_barang').on('change',function(){
        var id = $('[name="pilih_barang"]').val();
        var jenis_customer = $('[name="jenis_customer"]').val();
        $.ajax({
            type : "POST",
            url : 'penjualan/c_penjualan_barang/barangBeli',
            dataType : "JSON",
            data : {id:id},
            success : function(data){
                $.each(data,function(){
                    $('[name="id_barang"]').val(data.id_barang);
                    $('[name="kode_barang"]').val(data.kode_barang);
                    $('[name="nama_barang"]').val(data.nama_barang);
                    if(jenis_customer=="enduser"){
                        $('[name="harga_satuan"]').val(convertToRupiah(parseFloat(data.harga_enduser).toFixed(2)));
                    }else{
                        $('[name="harga_satuan"]').val(convertToRupiah(parseFloat(data.harga_installer).toFixed(2)));
                    }
                    $('[name="satuan_beli"]').val(data.satuan);
                    $('[name="detail_barang"]').val(data.detail_barang);
                    $('#Modal-Barang').modal('show');
                    $('[name="pilih_barang"]').val(" ").trigger('change');
                });
            }
        });
    });

    $('#id_customer').on('change', function(){
        var id_customer = $('[name="id_customer"]').val();
        $.ajax({
            type : "POST",
            url : 'penjualan/c_penjualan_barang/detailCustomer',
            dataType : "JSON",
            data : {id_customer:id_customer},
            success : function(data){
                $.each(data,function(){
                    $('[name="jenis_customer"]').val(data.jenis_customer);
                    $('[name="no_telpon"]').val(data.no_telp_customer);
                    $('[name="alamat"]').val(data.alamat_customer);
                });
            }
        });

    });

});


function convertToRupiah(angka)
{

    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    
    for(var i = 0; i < angkarev.length; i++) 
      if(i%3 == 0) rupiah += (angkarev.substr(i,3)+'.').replace('..',',');

    
    return rupiah.split('',rupiah.length-1).reverse().join('');

}


/* Tanpa Rupiah */
var tanpa_rupiah = document.getElementById('input-rupiah');
tanpa_rupiah.addEventListener('keyup', function(e)
{
	tanpa_rupiah.value = formatRupiah(this.value);
});

/* Tanpa Rupiah */
var bayar_pajak = document.getElementById('bayar_pajak');
bayar_pajak.addEventListener('keyup', function(e)
{
	bayar_pajak.value = formatRupiah(this.value);
});dibayar

/* Tanpa Rupiah */
var bayar_akhir = document.getElementById('dibayar');
bayar_akhir.addEventListener('keyup', function(e)
{
	bayar_akhir.value = formatRupiah(this.value);
});

// $('#tanggal_beli').datepicker({
//     uiLibrary: 'bootstrap4',
//     format: 'dd-mm-yyyy',
//     size: 'small',
//     icons: {
//          rightIcon: '<span class="glyphicon glyphicon-calendar"></span>'
//      }
//     });


/* Fungsi */
function formatRupiah(angka)
{
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split	= number_string.split(','),
		sisa 	= split[0].length % 3,
		rupiah 	= split[0].substr(0, sisa),
		ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	
	return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	// return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}


var cek_total = document.getElementById('qty');
cek_total.addEventListener('keyup', function() {
    var qty = $("#qty").val();
    var harga = $("#input-rupiah").val().replace(".", "").replace(".", "").replace(",", ".");
    var total = qty*harga;
    var convertTot = convertToRupiah(parseFloat(total).toFixed(2));
    $("#total").val(convertTot);
});



var cek_discon = document.getElementById('discon');
cek_discon.addEventListener('keyup', function() {
    var qty = $("#qty").val();
    var harga = $("#input-rupiah").val().replace(".", "").replace(".", "").replace(",", ".");
    var discon = $("#discon").val().replace(",", ".");
    var total = qty*parseFloat(harga);
    var total_disc = parseFloat(discon)/100;
    var total_disc_a = parseFloat(total)*parseFloat(total_disc);
    var total_disc_b = parseFloat(total)-parseFloat(total_disc_a);
    var total_disc_c = convertToRupiah(parseFloat(total_disc_b).toFixed(2))
    $("#total").val(total_disc_c);
});



var bayar_pajak = document.getElementById('bayar_pajak');
bayar_pajak.addEventListener('keyup', function() {
    var total_beli = $("#id_bayar_hasil").val().replace(".", "").replace(".", "").replace(",", ".");
    var total_bayar = $("#bayar_pajak").val().replace(".", "").replace(".", "").replace(",", ".");
    var totalpajak = parseFloat(total_beli)+parseFloat(total_bayar);
    $("#total_akhir").val(convertToRupiah(parseFloat(totalpajak).toFixed(2)));
});


var dibayar = document.getElementById('dibayar');
dibayar.addEventListener('keyup', function() {
    var total_beli = $("#total_akhir").val().replace(".", "").replace(".", "").replace(",", ".");
    var total_bayar = $("#dibayar").val().replace(".", "").replace(".", "").replace(",", ".");
    var totalpajak = parseFloat(total_beli)-parseFloat(total_bayar);
    $("#sisa_bayar").val(convertToRupiah(parseFloat(totalpajak).toFixed(2)));
});

// var cek_total = document.getElementById('qty');
// cek_total.addEventListener('keyup', function() {
//     $('#total').maskMoney({
//         thousands:'.', 
//         decimal:',', 
//         precision:2,
//         affixesStay: true
//     });
// });


// function initmaskMoney(selector){
    // $(selector).maskMoney({
        
    //     thousands:'.', 
    //     decimal:',', 
    //     precision:2,
    //     affixesStay: true
    // });
// }

//MEMBUAT FUNGSI MENYIMPAN DATA
$('#btn_simpan').on('click',function(){
    var no_faktur       = $('[name="no_faktur"]').val();
    var id_barang       = $('[name="id_barang"]').val();
    var harga_satuan    = $('[name="harga_satuan"]').val().replace(".", "").replace(".", "").replace(",", ".");;
    var qty             = $('[name="qty"]').val();
    var discon          = $('[name="discon"]').val().replace(",", ".");;
    var total           = $('[name="total"]').val().replace(".", "").replace(".", "").replace(",", ".");;
    if(harga_satuan==""){
        Swal.fire(
            'Kode Group Tidak Boleh Kosong!',
            'tutup informasi',
            'warning'
        );
    }else if(qty==""){
        Swal.fire(
            'Nama Group Tidak Boleh Kosong!',
            'tutup informasi',
            'warning'
        );
    }else{
        $.ajax({
            type : "POST",
            url : 'penjualan/c_penjualan_barang/simpanJual',
            dataType : "JSON",
            data : {no_faktur:no_faktur,id_barang:id_barang,harga_satuan:harga_satuan,qty:qty,discon:discon,total:total},
            success: function(data){
                // Swal.fire(
                //     'Datas Bershasil Disimpan!',
                //     'tutup informasi',
                //     'success'
                //     );
                    $('[name="satuan_beli"]').val("");
                    $('[name="harga_satuan"]').val("");
                    $('[name="qty"]').val("");
                    $('[name="discon"]').val("");
                    $('[name="total"]').val("");
                    $('#Modal-Barang').modal('hide');
                    tampilDataBeli();
                   
            }
        });
        return false;
    }
    });


    $('#pembayaran_akhir').on('click',function(){
        var no_faktur       = $('[name="no_faktur"]').val();
        var id_customer     = $('[name="id_customer"]').val();
        var tanggal_beli    = $('[name="tanggal_beli"]').val().replace("/", "-");
        var id_bayar_hasil  = $('[name="id_bayar_hasil"]').val().replace(".", "").replace(".", "").replace(",", ".");
        var bayar_pajak     = $('[name="bayar_pajak"]').val().replace(".", "").replace(".", "").replace(",", ".");
        var total_akhir     = $('[name="id_bayar_hasil"]').val().replace(".", "").replace(".", "").replace(",", ".");
        var dibayar         = $('[name="dibayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
        var sisa_bayar      = $('[name="sisa_bayar"]').val().replace(".", "").replace(".", "").replace(",", ".");
        var jatuh_tempo     = $('[name="jatuh_tempo"]').val().replace(".", "").replace(".", "").replace(",", ".");
        if(id_customer==" "){
            Swal.fire(
                'Supplier Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
            $('#default-Modal').modal('hide');
        }else if(tanggal_beli==""){
            Swal.fire(
                'Setting tanggal pembelian',
                'tutup informasi',
                'warning'
            );
            $('#default-Modal').modal('hide');
        }else{
            $.ajax({
                type : "POST",
                url : 'penjualan/c_penjualan_barang/bayarPenjualan',
                dataType : "JSON",
                data : {no_faktur:no_faktur,id_customer:id_customer,tanggal_beli:tanggal_beli,id_bayar_hasil:id_bayar_hasil,bayar_pajak:bayar_pajak,total_akhir:total_akhir,dibayar:dibayar,sisa_bayar:sisa_bayar,jatuh_tempo:jatuh_tempo},
                success: function(data){
                    $('#default-Modal').modal('hide');   
                    Swal.fire(
                        'Data Pembelian Bershasil Disimpan!',
                        'tutup informasi',
                        'success'
                        );
                    location.reload(true);
                    cetakFaktur();
                    }
                });
                
            }            
        });   

        function cetakFaktur(){
            window.open('/infinity-cctv/penjualan/c_penjualan_barang/cetak','_blank');
        }
    

    function tampilDataBeli()
    {
        var no_faktur       = $('[name="no_faktur"]').val();
        var no =0;
        $.ajax({
            type : "POST",
            url : 'penjualan/c_penjualan_barang/tampilJual',
            dataType : "JSON",
            data : {no_faktur:no_faktur},
            success: function(data){
                no++;
                var html = '';
                var i;
                var total = '';
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                            // '<td>'+no+'</td>'+
                            '<td>'+'<b>['+data[i].kode_barang+']</b>'+' '+data[i].nama_barang+'</td>'+
                            '<td>'+convertToRupiah(data[i].harga)+'</td>'+
                            '<td>'+data[i].qty+' '+data[i].satuan+'</td>'+
                            // '<td>'+data[i].satuan+'</td>'+
                            '<td>'+(data[i].discon).replace('.',',')+'</td>'+
                            '<td>'+convertToRupiah(data[i].total)+'</td>'+
                            '<td hidden class="count-me">'+data[i].total+'</td>'+
                            '<td style="text-align:center;">'+
                                // '<a href="javascript:;" id="get" class="btn btn-sm btn-warning btn-xs item_edit" data="'+data[i].id+'">'+'<i class="fa fa-pencil"></i>'+'</a>'+' '+
                                '<a href="javascript:;" class="btn btn-sm btn-danger btn-xs item_delete" data="'+data[i].id+'">'+'<i class="fa fa-trash"></i>'+'</a>'+
                            '</td>'+
                            '</tr>';
                }
               
                $('#data_beli').html(html);
                var tds = document.getElementById('tabel_barang').getElementsByTagName('td');
                var sum = 0;
                for(var i = 0; i < tds.length; i ++) {
                    if(tds[i].className == 'count-me') {
                        sum += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
                    }
                }
                $('#grand_total').html(convertToRupiah(parseFloat(sum).toFixed(2)));
                $('#id_bayar_hasil').val(convertToRupiah(parseFloat(sum).toFixed(2)));
            }
            
        });

    }

//     $('.dataTable').on('click','.item_edit',function(){
//     var id=$(this).attr('data');
//     $.ajax({
//         type : "GET",
//         url : 'pembelian/c_pembelian/editBarang',
//         dataType : "JSON",
//         data : {id:id},
//         success: function(data){
//             $.each(data,function(){
//                 $('[name="id_edit_barang"]').val(data.id);
//                 $('[name="id_barang"]').val(data.id_barang);
//                 $('[name="nama_barang"]').val(data.nama_barang);
//                 $('[id="satuan_beli"]').html('<option value="'+data.id_satuan_kecil+'">'+data.satuan_kecil+'</option><option value="'+data.id_satuan_besar+'">'+data.satuan_besar+'</option>');
//                 $('[id="label_satuan_besar"]').text(data.satuan_besar);
//                 $('[name="filefoto"]').val(data.foto_barang);
//                 $('[name="qty"]').val(data.qty);
//                 $('[name="harga_satuan"]').val(data.harga_satuan);
//                 $('[name="discon"]').val(data.disc);
//                 $('[name="total"]').val(data.total);
//                 $('#Modal-Barang').modal('show');
//             });
//         }
//     });
//     return false;
// });    

$('.dataTable').on('click','.item_delete',function(){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Anda yakin akan menghapus?',
        text: "data akan hilang permanen dari sistem!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
            var id=$(this).attr('data');
            $.ajax({
                type : "POST",
                url : 'pembelian/c_pembelian/hapusBarang',
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    swalWithBootstrapButtons.fire(
                        'Di Hapus!',
                        'Data telah dihapus dari aplikasi',
                        'success'
                      )
                      tampilDataBeli();
                }
                
            });      
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Batal',
            'Data batal dihapus :)',
            'error'
          )
        }
      })
});

function tampilkanPreview(gambar,idpreview){
    //membuat objek gambar
    var gb = gambar.files;
    //loop untuk merender gambar
    for (var i = 0; i < gb.length; i++){
    //bikin variabel
        var gbPreview = gb[i];
        var imageType = /image.*/;
        var preview=document.getElementById(idpreview);
        var reader = new FileReader();
        if (gbPreview.type.match(imageType)) {
    //jika tipe data sesuai
        preview.file = gbPreview;
        reader.onload = (function(element) {
        return function(e) {
            element.src = e.target.result;
        };
        })(preview);
    //membaca data URL gambar
         reader.readAsDataURL(gbPreview);
        }else{
    //jika tipe data tidak sesuai
        alert("Type file tidak sesuai. Khusus image.");
              }
          }
      }




