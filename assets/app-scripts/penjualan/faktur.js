$(document).ready(function(){

    //VALIDASI KODE GROUP
    var cek_g = document.getElementById('cek_group');
    var no =0;
    cek_g.addEventListener('change', function() {
        var kode = $('[name="kode_group"]').val();
        $.ajax({
            type : 'GET',
            url : 'faktur/get_barang',
            async : true,
            dataType : 'json',
            data : {kode:kode},
            success : function(data){
                no++;
                var qty = 1;
                var harga = data.harga_beli;
                var total = qty*harga;
                $('#barang_jual').append('<tr id="row'+no+'"><td>'+no+'</td>'+
                '<td><input type="text" style="font-size: x-small;" name="name[]" readonly value="'+data.id_barang+'" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" name="name[]" readonly value="'+data.nama_barang+'" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" id="harga_beli'+no+'" name="harga_beli[]" readonly value="" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" name="name[]" readonly value="pcs" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" id="qty'+no+'" name="qty[]" onchange="cek_total('+no+')" value="1" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" name="name[]" value="0" class="form-control" /></td>'+
                '<td><input type="text" style="font-size: x-small;" id="total'+no+'"  name="total[]" readonly value="'+total+'" class="price form-control" /></td>'+
                '<td><button type="button" id="'+no+'" class="btn btn-danger btn-sm btn_remove">X</button></td>'+
                '</tr>');
                $("#cek_group").val("");
                grand_total();
            }
        });

    });

});


function grand_total()
{
    var sum = 0;
    var grand = $(".price").val();
    sum += grand;
    $("#grand_total").text(+sum);
}

function cek_total(no)
{
    var qtyy = $("#qty"+no).val();
    var hargaa = $("#harga_beli"+no).val(); 
    var tott = qtyy*hargaa;
    $("#total"+no).val(tott);
    grand_total();
    $("#cek_group").focus();

}

function cek_disc(no)
{
    var qtyy = $("#qty"+no).val();
    var hargaa = $("#harga_beli"+no).val(); 
    var tott = qtyy*hargaa;
    $("#total"+no).val(tott)
}
