$(document).ready(function(){
    $('#btn_update').attr('hidden',true);

    ////MEMBUAT FUNGSI GET DATA
    $('#simpletable').on('click','#btn_getEdit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'master/c_paket/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id"]').val(data.id);
                    $('[name="nama_paket"]').val(data.nama_paket);
                    $('[name="harga"]').val(data.harga);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                });
            }
        });
        return false;
    });

    ////UPDATE DATA
    $('#btn_update').on('click',function(){
        var id          = $('[name="id"]').val();
        var nama_paket  = $('[name="nama_paket"]').val();
        var harga       = $('[name="harga"]').val();
        $.ajax({
            type : "POST",
            url  : "master/c_paket/edit",
            dataType : "JSON",
            data : {
                id:id,
                nama_paket:nama_paket,
                harga:harga
            },
            success: function(data){
                Swal.fire(
                    'Data Berhasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="id"]').val("");
                $('[name="nama_paket"]').val("");
                $('[name="harga"]').val("");
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('#simpletable').load(" #simpletable ");
            }
        });
        return false;
    });

    ////MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
        var nama_paket = $('[name="nama_paket"]').val();
        if(nama_paket==""){
            Swal.fire(
                'Nama Paket Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else{
            $.ajax({
                type : "POST",
                url : 'master/c_paket/simpan',
                dataType : "JSON",
                data : {
                    nama_paket:nama_paket,
                    harga:harga
                },
                success: function(data){
                    Swal.fire(
                        'Data Berhasil Disimpan!',
                        'tutup informasi',
                        'success'
                        );
                        $('[name="nama_paket"]').val("");
                        $('[name="harga"]').val("");
                        $('#btn_simpan').attr('hidden',false);
                        $('#btn_update').attr('hidden',true);
                        $('#simpletable').load(" #simpletable ");
                }
            });
            return false;
        }
    });

    ////MEMBUAT FUNGSI RESET FORM
    $('#btn_reset').on('click', function(){
        $('[name="nama_paket"]').val("");
        $('[name="harga"]').val("");
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });

    ////MEMBUAT FUNGSI DELETE DATA
    $('#simpletable').on('click','#btn_hapus',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              cancelButton: 'btn btn-danger',
              confirmButton: 'btn btn-success'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin ingin menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var id=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'master/c_akomodasi/hapusData',
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('#simpletable').load(" #simpletable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

    ////Membuat FORMAT RUPIAH
    var hargaP    = document.getElementById("hargaP");
    hargaP.addEventListener('keyup', function(e){
        hargaP.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            rupiah 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

});