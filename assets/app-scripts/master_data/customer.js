$(document).ready(function(){

    $('#simpletable').DataTable();

    $("#kota_kabupaten").select2();
    $("#provinsi_sl").select2();

    $('#btn_update').attr('hidden',true);


    ////MEMBUAT FUNGSI GET DATA
    $('#simpletable').on('click','#btn_getEdit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'master/c_customer/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id_cs"]').val(data.id_cs);
                    $('[name="nama_cs"]').val(data.nama_cs);
                    $('[name="notelp_cs"]').val(data.notelp_cs);
                    $('[name="nohp_cs"]').val(data.nohp_cs);
                    $('[name="alamat_cs"]').val(data.alamat_cs);
                    $('#provinsi_sl').val(data.provinsi_cs).trigger('change');
                    $('#kota_kabupaten').val(data.kota_cs).trigger('change');
                    $('[name="nofax_cs"]').val(data.nofax_cs);
                    $('[name="email_cs"]').val(data.email_cs);
                    $('[name="jenis_cs"]').val(data.jenis_cs);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                });
            }
        });
        return false;
    });

    ////UPDATE DATA
    $('#btn_update').on('click',function(){
        var id_cs        = $('[name="id_cs"]').val();
        var nama_cs      = $('[name="nama_cs"]').val();
        var notelp_cs    = $('[name="notelp_cs"]').val();
        var nohp_cs      = $('[name="nohp_cs"]').val();
        var alamat_cs    = $('[name="alamat_cs"]').val();
        var provinsi_cs  = $('[name="provinsi_cs"]').val();
        var kota_cs      = $('[name="kota_cs"]').val();
        var nofax_cs     = $('[name="nofax_cs"]').val();
        var email_cs     = $('[name="email_cs"]').val();
        var jenis_cs     = $('[name="jenis_cs"]').val();
        $.ajax({
            type : "POST",
            url  : "master/c_customer/update_cs",
            dataType : "JSON",
            data : {
                id_cs:id_cs,
                nama_cs:nama_cs,
                provinsi_cs:provinsi_cs,
                notelp_cs:notelp_cs,
                kota_cs:kota_cs,
                nohp_cs:nohp_cs,
                nofax_cs:nofax_cs,
                alamat_cs:alamat_cs,
                email_cs:email_cs,
                jenis_cs:jenis_cs
            },
            success: function(data){
                Swal.fire(
                    'Data Berhasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="id_cs"]').val("");
                $('[name="nama_cs"]').val("");
                $('[name="notelp_cs"]').val("");
                $('[name="nohp_cs"]').val("");
                $('[name="alamat_cs"]').val("");
                $('#provinsi_sl').val(null).trigger('change');
                // $('#kota_kabupaten').val(null).trigger('change');
                $('[name="nofax_cs"]').val("");
                $('[name="email_cs"]').val("");
                $('[name="jenis_cs"]').val("");
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('#simpletable').load(" #simpletable ");
            }
        });
        return false;
    });

    ////MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
        var nama_cs      = $('[name="nama_cs"]').val();
        var notelp_cs    = $('[name="notelp_cs"]').val();
        var nohp_cs      = $('[name="nohp_cs"]').val();
        var alamat_cs    = $('[name="alamat_cs"]').val();
        var provinsi_cs  = $('[name="provinsi_cs"]').val();
        var kota_cs      = $('[name="kota_cs"]').val();
        var nofax_cs     = $('[name="nofax_cs"]').val();
        var email_cs     = $('[name="email_cs"]').val();
        var jenis_cs     = $('[name="jenis_cs"]').val();
        var atposition    = email_cs.indexOf("@");  
        var dotposition   = email_cs.lastIndexOf(".");  
        if(nama_cs==""){
            Swal.fire(
                'Nama Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(notelp_cs==""){
            Swal.fire(
                'No Telp Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(isNaN(notelp_cs)){
            Swal.fire(
                'Harap isi No Telp dengan Angka!',
                'tutup informasi',
                'warning'
            );
        }else if(nohp_cs==""){
            Swal.fire(
                'No HP Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(isNaN(nohp_cs)){
            Swal.fire(
                'Harap isi No HP dengan Angka!',
                'tutup informasi',
                'warning'
            );
        }else if(alamat_cs==""){
            Swal.fire(
                'Alamat Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(provinsi_cs==""){
            Swal.fire(
                'Provinsi Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(kota_cs==""){
            Swal.fire(
                'Kota/Kabupaten Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(nofax_cs==""){
            Swal.fire(
                'No Fax Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(isNaN(nofax_cs)){
            Swal.fire(
                'Harap isi No Fax dengan Angka!',
                'tutup informasi',
                'warning'
            );
        }else if(email_cs==""){
            Swal.fire(
                'Email Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(atposition<1 || dotposition<atposition+2 || dotposition+2>=email_cs.length){
            Swal.fire(
                'Format Email Salah!',
                'tutup informasi',
                'warning'
            );
        }else if(jenis_cs==""){
            Swal.fire(
                'Jenis Customer Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else{
            $.ajax({
                type : "POST",
                url : 'master/c_customer/simpan_cs',
                dataType : "JSON",
                data : {
                    nama_cs:nama_cs,
                    provinsi_cs:provinsi_cs,
                    notelp_cs:notelp_cs,
                    kota_cs:kota_cs,
                    nohp_cs:nohp_cs,
                    nofax_cs:nofax_cs,
                    alamat_cs:alamat_cs,
                    email_cs:email_cs,
                    jenis_cs:jenis_cs
                },
                success: function(data){
                    Swal.fire(
                        'Data Berhasil Disimpan!',
                        'tutup informasi',
                        'success'
                        );
                        $('[name="nama_cs"]').val("");
                        $('[name="notelp_cs"]').val("");
                        $('[name="nohp_cs"]').val("");
                        $('[name="alamat_cs"]').val("");
                        $('#provinsi_sl').val(null).trigger('change');
                        // $('#kota_kabupaten').val(null).trigger('change');
                        $('[name="nofax_cs"]').val("");
                        $('[name="email_cs"]').val("");
                        $('[name="jenis_cs"]').val("");
                        $('#btn_simpan').attr('hidden',false);
                        $('#btn_update').attr('hidden',true);
                        $('#simpletable').load(" #simpletable ");

                }
            });
            return false;
        }
    });

    ////MEMBUAT FUNGSI RESET FORM
    $('#btn_reset').on('click', function(){
        $('[name="nama_cs"]').val("");
        $('[name="notelp_cs"]').val("");
        $('[name="nohp_cs"]').val("");
        $('[name="alamat_cs"]').val("");
        $('#provinsi_sl').val(null).trigger('change');
        // $('#kota_kabupaten').val(0).trigger('change');
        $('[name="nofax_cs"]').val("");
        $('[name="email_cs"]').val("");
        $('[name="jenis_cs"]').val("");
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });

    ////MEMBUAT FUNGSI DELETE DATA
    $('#simpletable').on('click','#btn_hapus',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              cancelButton: 'btn btn-danger',
              confirmButton: 'btn btn-success'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin ingin menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var id_cs=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'master/c_customer/hapusData',
                    dataType : "JSON",
                    data : {id_cs:id_cs},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('#simpletable').load(" #simpletable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

    ////FUNGSI GET KOTA DARI PROVINSI
    $('#provinsi_sl').change(function(){
        var id=$(this).val();
        $.ajax({
            url : "master/c_customer/get_kotakabupaten",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'json',
            success: function(data){
                var html = '';
                var i;
                html += "<option value=''>Pilih Kota/Kabupaten</option>";
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].id+'>'+data[i].nama+'</option>';
                }
                $('#kota_kabupaten').html(html);
                 
            }
        });
    });

});