var base_url_img ='http://localhost/infinity-cctv/assets/images/barang/';
$(document).ready(function(){
        
    $("#group_barang").select2();
    $("#satuan_barang").select2();
    $("#pilih_barang_edit").select2();
    $('#tabel_barang').DataTable();
    $('#update_data').attr('hidden',true)

    $('#create_kode_barang').on('click', function(){
        var kode = $('[name="group_barang"]').val();
        $.ajax({
            type : 'GET',
            url : 'master/c_barang/get_kode',
            async : true,
            dataType : 'json',
            data : {kode:kode},
            success : function(data){
                if(kode==""){
                    $('[name="kode_barang"]').val("");
                }else{
                    $('[name="kode_barang"]').val(data);
                }
            }
        });
    });

    const flashData = $('.flash-data').data('flashdata');
    if(flashData=="berhasil"){
        Swal.fire('Data Barang Berhasil Di Simpan','','success');
    }else if(flashData=="gagal"){
        Swal.fire('Data Barang Gagal Di Simpan','','warning');       
    }

    $("#tabel_barang").on('click','.item_edit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "post",
            url : 'master/c_barang/getBarang',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="group_barang"]').val(data.kode_group).trigger('change');
                    $('[name="id_barang"]').val(data.id_barang);
                    $('[name="kode_barang"]').val(data.kode_barang);
                    $('[name="nama_barang"]').val(data.nama_barang);
                    $('[name="detail_barang"]').val(data.detail_barang);
                    $('[name="stok_barang"]').val(data.stok_barang);
                    $('[name="minimum_stok"]').val(data.minimum_stok);
                    $('[name="satuan_barang"]').val(data.id_satuan).trigger('change');
                    $('[name="harga_enduser"]').val(convertToRupiah(data.harga_enduser));
                    $('[name="harga_installer"]').val(convertToRupiah(data.harga_installer));
                    $('[name="hidden_foto"]').val(data.foto_barang);
                    $('#preview').attr('src', base_url_img+data.foto_barang);
                    $('#formact').attr('action','master/c_barang/update');

                    // $('#btn_update').attr('hidden',false); 
                    // $('#btn_simpan').attr('hidden',true); 
                });
                $("#default-Modal").modal('hide');
                $('#update_data').attr('hidden',false); 
                $('#simpan_data').attr('hidden',true); 
            }
        });
        return false;
    });


});



function convertToRupiah(angka)
{

    var rupiah = '';    
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) 
      if(i%3 == 0) rupiah += (angkarev.substr(i,3)+'.').replace('..',',');
    return rupiah.split('',rupiah.length-1).reverse().join('');

}


/* Tanpa Rupiah */
var tanpa_rupiah = document.getElementById('harga_enduser');
tanpa_rupiah.addEventListener('keyup', function(e)
{
	tanpa_rupiah.value = formatRupiah(this.value);
});

/* Tanpa Rupiah */
var bayar_pajak = document.getElementById('harga_installer');
bayar_pajak.addEventListener('keyup', function(e)
{
	bayar_pajak.value = formatRupiah(this.value);
});



/* Fungsi */
function formatRupiah(angka)
{
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split	= number_string.split(','),
		sisa 	= split[0].length % 3,
		rupiah 	= split[0].substr(0, sisa),
		ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
	
	return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	// return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}


function tampilkanPreview(gambar,idpreview){
//membuat objek gambar
var gb = gambar.files;
//loop untuk merender gambar
for (var i = 0; i < gb.length; i++){
//bikin variabel
    var gbPreview = gb[i];
    var imageType = /image.*/;
    var preview=document.getElementById(idpreview);
    var reader = new FileReader();
    if (gbPreview.type.match(imageType)) {
//jika tipe data sesuai
    preview.file = gbPreview;
    reader.onload = (function(element) {
    return function(e) {
        element.src = e.target.result;
    };
    })(preview);
//membaca data URL gambar
     reader.readAsDataURL(gbPreview);
    }else{
//jika tipe data tidak sesuai
    alert("Type file tidak sesuai. Khusus image.");
          }
      }
  }

