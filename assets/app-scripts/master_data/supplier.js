$(document).ready(function(){
    $('#btn_update').attr('hidden',true);

    ////MEMBUAT FUNGSI GET DATA
    $('#simpletable').on('click','#btn_getEdit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'master/c_supplier/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id_sup"]').val(data.id_sup);
                    $('[name="nama_sup"]').val(data.nama_sup);
                    $('[name="alamat_sup"]').val(data.alamat_sup);
                    $('[name="notelp_sup"]').val(data.notelp_sup);
                    $('[name="email_sup"]').val(data.email_sup);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                });
            }
        });
        return false;
    });

    ////UPDATE DATA
    $('#btn_update').on('click',function(){
        var id_sup        = $('[name="id_sup"]').val();
        var nama_sup      = $('[name="nama_sup"]').val();
        var alamat_sup    = $('[name="alamat_sup"]').val();
        var notelp_sup    = $('[name="notelp_sup"]').val();
        var email_sup     = $('[name="email_sup"]').val();
        $.ajax({
            type : "POST",
            url  : "master/c_supplier/update_sup",
            dataType : "JSON",
            data : {
                id_sup:id_sup,
                nama_sup:nama_sup,
                alamat_sup:alamat_sup,
                notelp_sup:notelp_sup,
                email_sup:email_sup
            },
            success: function(data){
                Swal.fire(
                    'Data Berhasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="id_sup"]').val("");
                $('[name="nama_sup"]').val("");
                $('[name="alamat_sup"]').val("");
                $('[name="notelp_sup"]').val("");
                $('[name="email_sup"]').val("");
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('#simpletable').load(" #simpletable ");
            }
        });
        return false;
    });

    ////MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
        var nama_sup      = $('[name="nama_sup"]').val();
        var alamat_sup    = $('[name="alamat_sup"]').val();
        var notelp_sup    = $('[name="notelp_sup"]').val();
        var email_sup     = $('[name="email_sup"]').val();
        var atposition    = email_sup.indexOf("@");  
        var dotposition   = email_sup.lastIndexOf(".");  
        if(nama_sup==""){
            Swal.fire(
                'Nama Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(alamat_sup==""){
            Swal.fire(
                'Alamat Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(notelp_sup==""){
            Swal.fire(
                'No Telepon Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(isNaN(notelp_sup)){
            Swal.fire(
                'Harap isi No Telp dengan Angka!',
                'tutup informasi',
                'warning'
            );
        }else if(email_sup==""){
            Swal.fire(
                'Email Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else if(atposition<1 || dotposition<atposition+2 || dotposition+2>=email_sup.length){
            Swal.fire(
                'Format Email Salah!',
                'tutup informasi',
                'warning'
            );
        }else{
            $.ajax({
                type : "POST",
                url : 'master/c_supplier/simpan_supp',
                dataType : "JSON",
                data : {
                    nama_sup:nama_sup,
                    alamat_sup:alamat_sup,
                    notelp_sup:notelp_sup,
                    email_sup:email_sup
                },
                success: function(data){
                    Swal.fire(
                        'Data Berhasil Disimpan!',
                        'tutup informasi',
                        'success'
                        );
                        $('[name="nama_sup"]').val("");
                        $('[name="alamat_sup"]').val("");
                        $('[name="notelp_sup"]').val("");
                        $('[name="email_sup"]').val("");
                        $('#btn_simpan').attr('hidden',false);
                        $('#btn_update').attr('hidden',true);
                        $('#simpletable').load(" #simpletable ");

                }
            });
            return false;
        }
    });

    ////MEMBUAT FUNGSI RESET FORM
    $('#btn_reset').on('click', function(){
        $('[name="nama_sup"]').val("");
        $('[name="alamat_sup"]').val("");
        $('[name="notelp_sup"]').val("");
        $('[name="email_sup"]').val("");
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });

    ////MEMBUAT FUNGSI DELETE DATA
    $('#simpletable').on('click','#btn_hapus',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              cancelButton: 'btn btn-danger',
              confirmButton: 'btn btn-success'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin ingin menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var id_sup=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'master/c_supplier/hapusData',
                    dataType : "JSON",
                    data : {id_sup:id_sup},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('#simpletable').load(" #simpletable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

});