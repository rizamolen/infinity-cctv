$(document).ready(function(){
    $('#btn_update').attr('hidden',true);

    ////MEMBUAT FUNGSI GET DATA
    $('#simpletable').on('click','#btn_getEdit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'master/c_akomodasi/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id"]').val(data.id);
                    $('[name="kota_tujuan"]').val(data.kota_tujuan);
                    $('[name="bbm_roda_2"]').val(data.bbm_roda_2);
                    $('[name="bbm_roda_4"]').val(data.bbm_roda_4);
                    $('[name="makan"]').val(data.makan);
                    $('[name="inap"]').val(data.inap);
                    $('#btn_update').attr('hidden',false); 
                    $('#btn_simpan').attr('hidden',true); 
                });
            }
        });
        return false;
    });

    ////UPDATE DATA
    $('#btn_update').on('click',function(){
        var id          = $('[name="id"]').val();
        var kota_tujuan = $('[name="kota_tujuan"]').val();
        var bbm_roda_2  = $('[name="bbm_roda_2"]').val();
        var bbm_roda_4  = $('[name="bbm_roda_4"]').val();
        var makan       = $('[name="makan"]').val();
        var inap        = $('[name="inap"]').val();
        $.ajax({
            type : "POST",
            url  : "master/c_akomodasi/edit",
            dataType : "JSON",
            data : {
                id:id,
                kota_tujuan:kota_tujuan,
                bbm_roda_2:bbm_roda_2,
                bbm_roda_4:bbm_roda_4,
                makan:makan,
                inap:inap
            },
            success: function(data){
                Swal.fire(
                    'Data Berhasil Diupdate!',
                    'tutup informasi',
                    'success'
                );
                $('[name="id"]').val("");
                $('[name="kota_tujuan"]').val("");
                $('[name="bbm_roda_2"]').val("");
                $('[name="bbm_roda_4"]').val("");
                $('[name="makan"]').val("");
                $('[name="inap"]').val("");
                $('#btn_simpan').attr('hidden',false);
                $('#btn_update').attr('hidden',true);  
                $('#simpletable').load(" #simpletable ");
            }
        });
        return false;
    });

    ////MEMBUAT FUNGSI MENYIMPAN DATA
    $('#btn_simpan').on('click',function(){
        var kota_tujuan = $('[name="kota_tujuan"]').val();
        var bbm_roda_2  = $('[name="bbm_roda_2"]').val();
        var bbm_roda_4  = $('[name="bbm_roda_4"]').val();
        var makan       = $('[name="makan"]').val();
        var inap        = $('[name="inap"]').val();
        if(kota_tujuan==""){
            Swal.fire(
                'Kota Tujuan Tidak Boleh Kosong!',
                'tutup informasi',
                'warning'
            );
        }else{
            $.ajax({
                type : "POST",
                url : 'master/c_akomodasi/simpan',
                dataType : "JSON",
                data : {
                    kota_tujuan:kota_tujuan,
                    bbm_roda_2:bbm_roda_2,
                    bbm_roda_4:bbm_roda_4,
                    makan:makan,
                    inap:inap
                },
                success: function(data){
                    Swal.fire(
                        'Data Berhasil Disimpan!',
                        'tutup informasi',
                        'success'
                        );
                        $('[name="kota_tujuan"]').val("");
                        $('[name="bbm_roda_2"]').val("");
                        $('[name="bbm_roda_4"]').val("");
                        $('[name="makan"]').val("");
                        $('[name="inap"]').val("");
                        $('#btn_simpan').attr('hidden',false);
                        $('#btn_update').attr('hidden',true);
                        $('#simpletable').load(" #simpletable ");
                }
            });
            return false;
        }
    });

    ////MEMBUAT FUNGSI RESET FORM
    $('#btn_reset').on('click', function(){
        $('[name="kota_tujuan"]').val("");
        $('[name="bbm_roda_2"]').val("");
        $('[name="bbm_roda_4"]').val("");
        $('[name="makan"]').val("");
        $('[name="inap"]').val("");
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });

    ////Membuat FORMAT RUPIAH
    var bbm2    = document.getElementById("bbm2");
    var bbm4    = document.getElementById("bbm4");
    var makan   = document.getElementById("makan");
    var inap    = document.getElementById("inap");
    bbm2.addEventListener('keyup', function(e){
        bbm2.value = formatRupiah(this.value, 'Rp. ');
    });
    bbm4.addEventListener('keyup', function(e){
        bbm4.value = formatRupiah(this.value, 'Rp. ');
    });
    makan.addEventListener('keyup', function(e){
        makan.value = formatRupiah(this.value, 'Rp. ');
    });
    inap.addEventListener('keyup', function(e){
        inap.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    ////MEMBUAT FUNGSI DELETE DATA
    $('#simpletable').on('click','#btn_hapus',function(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              cancelButton: 'btn btn-danger',
              confirmButton: 'btn btn-success'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Anda yakin ingin menghapus?',
            text: "data akan hilang permanen dari sistem!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var id=$(this).attr('data');
                $.ajax({
                    type : "POST",
                    url : 'master/c_akomodasi/hapusData',
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                        swalWithBootstrapButtons.fire(
                            'Di Hapus!',
                            'Data telah dihapus dari aplikasi',
                            'success'
                          )
                          $('#simpletable').load(" #simpletable ");
                    }
                    
                });      
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Batal',
                'Data batal dihapus :)',
                'error'
              )
            }
          })
    });

});