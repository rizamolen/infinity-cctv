var base_url_img ='http://localhost/infinity-cctv/assets/images/karyawan/';
$(document).ready(function(){
    $('#btn_update').attr('hidden',true);

    ////MEMBUAT FUNGSI GET DATA
    $('#simpletable').on('click','#btn_getEdit',function(){
        var id=$(this).attr('data');
        $.ajax({
            type : "GET",
            url : 'user/c_karyawan/getData',
            dataType : "JSON",
            data : {id:id},
            success: function(data){
                $.each(data,function(){
                    $('[name="id"]').val(data.id);
                    $('[name="kode_karyawan"]').val(data.kode_karyawan);
                    $('[name="nama"]').val(data.nama);
                    $('[name="tempat_lahir"]').val(data.tempat_lahir);
                    $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
                    $('[name="jabatan"]').val(data.jabatan);
                    $('[name="alamat"]').val(data.alamat);
                    $('[name="no_hp"]').val(data.no_hp);
                    $('[name="hidden_foto"]').val(data.gambar);
                    $('#preview').attr('src', base_url_img+data.gambar);
                    $('#formact').attr('action','user/c_karyawan/update');
                    $('#btn_update').attr('hidden',false);
                    $('#btn_simpan').attr('hidden',true);
                });
            }
        });
        return false;
    });

    ////MEMBUAT FUNGSI RESET FORM
    $('#btn_reset').on('click', function(){
        var pathFoto = 'http://localhost/infinity-cctv/assets/images/blank.png';
        $('[name="kode_karyawan"]').val("");
        $('[name="nama"]').val("");
        $('[name="tempat_lahir"]').val("");
        $('[name="tanggal_lahir"]').val("");
        $('[name="jabatan"]').val("");
        $('[name="alamat"]').val("");
        $('[name="no_hp"]').val("");
        $('[name="gambar"]').val("");
        $('#preview').attr("src",pathFoto);
        $('#preview').attr('hidden',false);
        $('#btn_simpan').attr('hidden',false);
        $('#btn_update').attr('hidden',true);  
    });
    
    const flashData1 = $('#simpan').data('flashdata');
    if(flashData1=="berhasil"){
        Swal.fire(
            'Data Karyawan Berhasil Disimpan!',
            'tutup informasi',
            'success'
            );
    }else if(flashData1=="gagal"){
        Swal.fire(
            'Data Karyawan Gagal Disimpan!',
            'tutup informasi',
            'error'
            );        
    }

    const flashData2 = $('#update').data('flashdata');
    if(flashData2=="berhasil"){
        Swal.fire(
            'Data Karyawan Berhasil Diupdate!',
            'tutup informasi',
            'success'
            );
    }else if(flashData2=="gagal"){
        Swal.fire(
            'Data Karyawan Gagal Diupdate!',
            'tutup informasi',
            'error'
            );        
    }

});

function tampilkanPreview(gambar,idpreview){
    //membuat objek gambar
    var gb = gambar.files;
    //loop untuk merender gambar
    for (var i = 0; i < gb.length; i++){
    //bikin variabel
        var gbPreview = gb[i];
        var imageType = /image.*/;
        var preview=document.getElementById(idpreview);
        var reader = new FileReader();
        if (gbPreview.type.match(imageType)) {
    //jika tipe data sesuai
        preview.file = gbPreview;
        reader.onload = (function(element) {
        return function(e) {
            element.src = e.target.result;
        };
        })(preview);
    //membaca data URL gambar
         reader.readAsDataURL(gbPreview);
        }else{
    //jika tipe data tidak sesuai
        alert("Type file tidak sesuai. Khusus image.");
              }
          }
}

// function confirmDialog() {
//     return confirm('Apakah anda yakin akan menghapus data ini?')
// }