-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2020 at 09:59 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipc`
--

-- --------------------------------------------------------

--
-- Table structure for table `akomodasi`
--

CREATE TABLE `akomodasi` (
  `id` int(11) NOT NULL,
  `kota_tujuan` varchar(30) NOT NULL,
  `bbm_roda_2` varchar(30) DEFAULT NULL,
  `bbm_roda_4` varchar(30) DEFAULT NULL,
  `makan` varchar(30) DEFAULT NULL,
  `inap` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akomodasi`
--

INSERT INTO `akomodasi` (`id`, `kota_tujuan`, `bbm_roda_2`, `bbm_roda_4`, `makan`, `inap`) VALUES
(5, 'Jambu Biji', 'Rp. 20.000', 'Rp. 50.000', 'Rp. 30.000', 'Rp. 250.000'),
(7, 'Banjarbaru', 'Rp. 45.000', '', 'Rp. 50.000', '');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(8) NOT NULL,
  `kode_group` varchar(8) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `detail_barang` varchar(150) NOT NULL,
  `stok_barang` int(11) NOT NULL,
  `minimum_stok` int(11) NOT NULL,
  `harga_enduser` decimal(11,2) NOT NULL,
  `harga_installer` decimal(11,2) NOT NULL,
  `foto_barang` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `kode_group`, `nama_barang`, `id_satuan`, `detail_barang`, `stok_barang`, `minimum_stok`, `harga_enduser`, `harga_installer`, `foto_barang`) VALUES
(1, 'PTI0001', 'PTI', 'Infinity DBC-2C-T4F', 1, 'Warranty: 3 Years All', 20, 5, '320000.00', '295000.00', ''),
(3, 'KBL0001', 'KBL', 'Inifinity Wire IA-C159P-CA37 (Rol300)', 2, 'Warranty: 3 Years All', 3, 2, '700000.00', '650000.00', 'images.jpeg'),
(4, 'KBL0002', 'KBL', 'Inifinity Wire IA-C159P-CA37 (Meter)', 4, 'No Warannty', 200, 100, '4500.00', '3500.00', 'coax_cable_infinity_web_icon7.'),
(5, 'PTI0002', 'PTI', 'Infinity 2MP', 1, 'Warranty : 3 Years All', 0, 5, '700000.00', '650000.00', 'images.jpeg'),
(6, 'PTI0003', 'PTI', 'Infinity DBC-2C', 1, '', 0, 5, '450000.00', '400000.00', 'images.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `conversi_satuan`
--

CREATE TABLE `conversi_satuan` (
  `id` int(11) NOT NULL,
  `id_barang` varchar(15) NOT NULL,
  `id_satuan_kecil` int(11) NOT NULL,
  `nilai_satuan_kecil` int(11) NOT NULL,
  `id_satuan_besar` int(11) NOT NULL,
  `nilai_satuan_besar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversi_satuan`
--

INSERT INTO `conversi_satuan` (`id`, `id_barang`, `id_satuan_kecil`, `nilai_satuan_kecil`, `id_satuan_besar`, `nilai_satuan_besar`) VALUES
(1, 'KBL0001', 4, 300, 2, 300);

-- --------------------------------------------------------

--
-- Table structure for table `conversi_satuan_harga`
--

CREATE TABLE `conversi_satuan_harga` (
  `id` int(11) NOT NULL,
  `id_group_barang` int(11) NOT NULL,
  `id_satuan_kecil` int(11) NOT NULL,
  `harga_kecil` int(11) NOT NULL,
  `id_satuan_besar` int(11) NOT NULL,
  `harga_besar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversi_satuan_harga`
--

INSERT INTO `conversi_satuan_harga` (`id`, `id_group_barang`, `id_satuan_kecil`, `harga_kecil`, `id_satuan_besar`, `harga_besar`) VALUES
(1, 2, 3, 6000, 2, 900000);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(25) NOT NULL,
  `no_telp_customer` varchar(15) NOT NULL,
  `no_hp_customer` varchar(15) NOT NULL,
  `alamat_customer` varchar(50) NOT NULL,
  `id_kabupaten` int(2) NOT NULL,
  `id_provinsi` int(2) NOT NULL,
  `fax_customer` varchar(15) NOT NULL,
  `email_customer` varchar(25) NOT NULL,
  `id_jenis_customer` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `no_telp_customer`, `no_hp_customer`, `alamat_customer`, `id_kabupaten`, `id_provinsi`, `fax_customer`, `email_customer`, `id_jenis_customer`) VALUES
(79, 'Riza Maulana', '05113365360', '085315161045', 'Jl. Kelayan A Gg.Kenari No.35', 94, 14, '05113365360', 'rizauniska@gmail.com', 'installer'),
(81, 'Laily Munada', '0511765760', '085248954498', 'Jl. Amplang Tenggiri', 28, 11, '0511765761', 'yahoo@gmail.com', 'enduser'),
(84, 'Yui', '05113277027', '08115080701', 'Jl. Merdeka', 364, 64, '05117277027', 'yui@gmail.com', 'installer');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_harga`
--

CREATE TABLE `daftar_harga` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jenis_customer` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `id` int(11) NOT NULL,
  `no_pembelian` varchar(50) NOT NULL,
  `id_barang` varchar(50) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_satuan` decimal(11,2) NOT NULL,
  `disc` decimal(11,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian`
--

INSERT INTO `detail_pembelian` (`id`, `no_pembelian`, `id_barang`, `id_satuan`, `qty`, `harga_satuan`, `disc`, `total`, `date_time`) VALUES
(1, 'LPB/03/2020/0001', 'KBL0001', 2, 25, '577272.73', '3.15', '13977215.98', '2020-03-11 13:15:29'),
(2, 'LPB/03/2020/0001', 'KBL0003', 2, 10, '672272.27', '4.05', '6450452.43', '2020-03-11 13:18:29'),
(3, 'LPB/03/2020/0002', '3', 2, 65, '450321.25', '20.00', '23416705.00', '2020-03-13 06:23:54'),
(4, 'LPB/03/2020/0002', '1', 1, 20, '456533.25', '10.00', '8217598.50', '2020-03-13 06:24:37'),
(5, 'LPB/03/2020/0003', '1', 1, 5, '450000.00', '10.00', '2025000.00', '2020-03-13 12:52:27'),
(7, 'LPB/03/2020/0003', '3', 2, 20, '375000.00', '10.00', '6750000.00', '2020-03-13 13:03:20'),
(8, 'LPB/03/2020/0004', '4', 4, 250, '6000.00', '20.00', '1200000.00', '2020-03-14 06:08:09'),
(9, 'LPB/03/2020/0005', '5', 1, 20, '375250.25', '40.00', '4503003.00', '2020-03-14 06:13:18'),
(10, 'LPB/03/2020/0006', '6', 1, 45, '324545.45', '54.40', '6659672.63', '2020-03-14 07:35:38');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian_oprasional`
--

CREATE TABLE `detail_pembelian_oprasional` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pembelian_oprasional`
--

INSERT INTO `detail_pembelian_oprasional` (`id`, `no_faktur`, `nama_barang`, `harga_barang`, `id_satuan`, `qty`, `total`) VALUES
(2, 'OPS/03/2020/0001', 'Steker', 250000, 1, 50, 12500000),
(3, 'OPS/03/2020/0001', 'llloio', 50000, 1, 5, 250000),
(4, 'OPS/03/2020/0001', 'CCTV Inn', 50000, 1, 1, 50000),
(5, 'OPS/03/2020/0002', 'Penharum', 45000, 1, 2, 90000),
(6, 'OPS/03/2020/0003', 'LEM', 4000, 1, 2, 8000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(25) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga` decimal(11,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `discon` decimal(11,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id`, `no_faktur`, `id_barang`, `harga`, `qty`, `discon`, `total`, `date_time`) VALUES
(1, 'ARS/INF/03/2020/0001', 3, '700000.00', 1, '10.00', '630000.00', '2020-03-13 14:01:15'),
(2, 'ARS/INF/03/2020/0001', 1, '320000.00', 2, '10.00', '576000.00', '2020-03-13 14:08:47'),
(3, 'ARS/INF/03/2020/0002', 1, '320000.00', 2, '0.00', '640000.00', '2020-03-14 03:18:44'),
(4, 'ARS/INF/03/2020/0002', 1, '320000.00', 2, '0.00', '640000.00', '2020-03-14 03:18:44'),
(5, 'ARS/INF/03/2020/0003', 1, '295000.00', 5, '0.00', '1475000.00', '2020-03-14 03:20:33'),
(6, 'ARS/INF/03/2020/0004', 4, '4500.00', 10, '0.00', '45000.00', '2020-03-14 04:08:02'),
(7, 'ARS/INF/03/2020/0005', 4, '4500.00', 10, '0.00', '45000.00', '2020-03-14 04:09:39'),
(8, 'ARS/INF/03/2020/0006', 4, '3500.00', 30, '0.00', '105000.00', '2020-03-14 04:10:41'),
(9, 'ARS/INF/03/2020/0007', 3, '700000.00', 1, '0.00', '700000.00', '2020-03-14 04:11:50'),
(10, 'ARS/INF/03/2020/0008', 3, '650000.00', 1, '0.00', '650000.00', '2020-03-14 04:13:16'),
(11, 'ARS/INF/03/2020/0009', 3, '700000.00', 1, '0.00', '700000.00', '2020-03-14 04:14:23'),
(12, 'ARS/INF/03/2020/0010', 4, '4500.00', 10, '0.00', '45000.00', '2020-03-14 04:15:53'),
(13, 'ARS/INF/03/2020/0011', 4, '3500.00', 20, '0.00', '70000.00', '2020-03-14 04:24:21'),
(14, 'ARS/INF/03/2020/0012', 3, '650000.00', 1, '0.00', '650000.00', '2020-03-14 04:25:59'),
(15, 'ARS/INF/03/2020/0013', 3, '700000.00', 1, '0.00', '700000.00', '2020-03-14 04:51:00'),
(16, 'ARS/INF/03/2020/0014', 3, '700000.00', 1, '0.00', '700000.00', '2020-03-14 04:52:51'),
(17, 'ARS/INF/03/2020/0015', 6, '400000.00', 2, '10.00', '720000.00', '2020-03-14 07:39:24'),
(18, 'ARS/INF/03/2020/0016', 6, '400000.00', 2, '0.00', '800000.00', '2020-03-14 07:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `det_penerimaan_barang`
--

CREATE TABLE `det_penerimaan_barang` (
  `no_faktur_penerimaan` varchar(50) NOT NULL,
  `kode_barang` int(8) NOT NULL,
  `qty` int(10) NOT NULL,
  `grup` varchar(100) NOT NULL,
  `harga_barang` int(20) NOT NULL,
  `diskon` int(10) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `sub_total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grup_barang`
--

CREATE TABLE `grup_barang` (
  `kode` varchar(10) NOT NULL,
  `nama_group` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grup_barang`
--

INSERT INTO `grup_barang` (`kode`, `nama_group`) VALUES
('AKS', 'AKSESORIS'),
('BBC', 'BABY CAM EZVIZ'),
('CMB', 'KAMERA BALCKSER'),
('CMH', 'KAMERA HIKVISIO'),
('CMI', 'KAMERA HD TURBO'),
('DBH', 'DVR HIKVISION'),
('DBI', 'DVR HD TURBO'),
('DBS', 'DVR BLACKSERIES'),
('DSC', 'DVR SCHNELL'),
('FPT', 'FINGER PRINT'),
('HDS', 'HARDDISK'),
('IPB', 'IP CAM BLACKSER'),
('IPH', 'IP CAM HIKVISIO'),
('IPI', 'IP HD TURBO'),
('JSA', 'JASA'),
('KBL', 'KABEL'),
('NVB', 'NVR BLACKSERIES'),
('NVH', 'NVR HIKVISION'),
('NVI', 'NVR HD TURBO'),
('po', 'kk'),
('PTH', 'PTZ HIKVISION'),
('PTI', 'KAMERA PTZ INFI'),
('SUM', 'SUMATO');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(4) NOT NULL,
  `jabatan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `jabatan`) VALUES
(1, 'Administrator'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `jasa_expedisi`
--

CREATE TABLE `jasa_expedisi` (
  `no_faktur` varchar(25) NOT NULL,
  `no_faktur_pembelian` varchar(25) NOT NULL,
  `no_jasa` varchar(25) NOT NULL,
  `expedisi` varchar(40) NOT NULL,
  `biaya` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasa_expedisi`
--

INSERT INTO `jasa_expedisi` (`no_faktur`, `no_faktur_pembelian`, `no_jasa`, `expedisi`, `biaya`, `id_karyawan`, `date_time`) VALUES
('EPD/03/2020/0001', 'LPB/03/2020/0001', 'AJ No. 04436', 'CV. ANGKASA', 524170, 1, '2020-03-11 13:21:16'),
('EPD/03/2020/0002', 'LPB/03/2020/0002', 'AJ No. 04436', 'CV. ANGKASA', 431500, 1, '2020-03-13 06:26:15'),
('EPD/03/2020/0003', 'LPB/03/2020/0003', 'AJ No. 04436', 'CV. ANGKASA', 325000, 1, '2020-03-13 13:07:26'),
('EPD/03/2020/0004', 'LPB/03/2020/0005', 'AJ No. 04436', 'CV. ANGKASA', 40000, 1, '2020-03-14 06:15:36'),
('EPD/03/2020/0005', 'LPB/03/2020/0006', 'AJ : 00260L', 'DUTA CAGO', 425000, 1, '2020-03-14 07:37:06');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id_kabupaten` int(2) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `id_provinsi` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `Alamat` varchar(150) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `tempat_lahir`, `tanggal_lahir`, `id_jabatan`, `Alamat`, `no_hp`, `foto`) VALUES
(1, 'Administrator', '', '0000-00-00', 0, '', '', 'administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `kotakabupaten`
--

CREATE TABLE `kotakabupaten` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `id_provinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kotakabupaten`
--

INSERT INTO `kotakabupaten` (`id`, `nama`, `id_provinsi`) VALUES
(9, 'KABUPATEN SIMEULUE', 11),
(10, 'KABUPATEN ACEH SINGKIL', 11),
(11, 'KABUPATEN ACEH SELATAN', 11),
(12, 'KABUPATEN ACEH TENGGARA', 11),
(13, 'KABUPATEN ACEH TIMUR', 11),
(14, 'KABUPATEN ACEH TENGAH', 11),
(15, 'KABUPATEN ACEH BARAT', 11),
(16, 'KABUPATEN ACEH BESAR', 11),
(17, 'KABUPATEN PIDIE', 11),
(18, 'KABUPATEN BIREUEN', 11),
(19, 'KABUPATEN ACEH UTARA', 11),
(20, 'KABUPATEN ACEH BARAT DAYA', 11),
(21, 'KABUPATEN GAYO LUES', 11),
(22, 'KABUPATEN ACEH TAMIANG', 11),
(23, 'KABUPATEN NAGAN RAYA', 11),
(24, 'KABUPATEN ACEH JAYA', 11),
(25, 'KABUPATEN BENER MERIAH', 11),
(26, 'KABUPATEN PIDIE JAYA', 11),
(27, 'KOTA BANDA ACEH', 11),
(28, 'KOTA SABANG', 11),
(29, 'KOTA LANGSA', 11),
(30, 'KOTA LHOKSEUMAWE', 11),
(31, 'KOTA SUBULUSSALAM', 11),
(32, 'KABUPATEN NIAS', 12),
(33, 'KABUPATEN MANDAILING NATAL', 12),
(34, 'KABUPATEN TAPANULI SELATAN', 12),
(35, 'KABUPATEN TAPANULI TENGAH', 12),
(36, 'KABUPATEN TAPANULI UTARA', 12),
(37, 'KABUPATEN TOBA SAMOSIR', 12),
(38, 'KABUPATEN LABUHAN BATU', 12),
(39, 'KABUPATEN ASAHAN', 12),
(40, 'KABUPATEN SIMALUNGUN', 12),
(41, 'KABUPATEN DAIRI', 12),
(42, 'KABUPATEN KARO', 12),
(43, 'KABUPATEN DELI SERDANG', 12),
(44, 'KABUPATEN LANGKAT', 12),
(45, 'KABUPATEN NIAS SELATAN', 12),
(46, 'KABUPATEN HUMBANG HASUNDUTAN', 12),
(47, 'KABUPATEN PAKPAK BHARAT', 12),
(48, 'KABUPATEN SAMOSIR', 12),
(49, 'KABUPATEN SERDANG BEDAGAI', 12),
(50, 'KABUPATEN BATU BARA', 12),
(51, 'KABUPATEN PADANG LAWAS UTARA', 12),
(52, 'KABUPATEN PADANG LAWAS', 12),
(53, 'KABUPATEN LABUHAN BATU SELATAN', 12),
(54, 'KABUPATEN LABUHAN BATU UTARA', 12),
(55, 'KABUPATEN NIAS UTARA', 12),
(56, 'KABUPATEN NIAS BARAT', 12),
(57, 'KOTA SIBOLGA', 12),
(58, 'KOTA TANJUNG BALAI', 12),
(59, 'KOTA PEMATANG SIANTAR', 12),
(60, 'KOTA TEBING TINGGI', 12),
(61, 'KOTA MEDAN', 12),
(62, 'KOTA BINJAI', 12),
(63, 'KOTA PADANGSIDIMPUAN', 12),
(64, 'KOTA GUNUNGSITOLI', 12),
(65, 'KABUPATEN KEPULAUAN MENTAWAI', 13),
(66, 'KABUPATEN PESISIR SELATAN', 13),
(67, 'KABUPATEN SOLOK', 13),
(68, 'KABUPATEN SIJUNJUNG', 13),
(69, 'KABUPATEN TANAH DATAR', 13),
(70, 'KABUPATEN PADANG PARIAMAN', 13),
(71, 'KABUPATEN AGAM', 13),
(72, 'KABUPATEN LIMA PULUH KOTA', 13),
(73, 'KABUPATEN PASAMAN', 13),
(74, 'KABUPATEN SOLOK SELATAN', 13),
(75, 'KABUPATEN DHARMASRAYA', 13),
(76, 'KABUPATEN PASAMAN BARAT', 13),
(77, 'KOTA PADANG', 13),
(78, 'KOTA SOLOK', 13),
(79, 'KOTA SAWAH LUNTO', 13),
(80, 'KOTA PADANG PANJANG', 13),
(81, 'KOTA BUKITTINGGI', 13),
(82, 'KOTA PAYAKUMBUH', 13),
(83, 'KOTA PARIAMAN', 13),
(84, 'KABUPATEN KUANTAN SINGINGI', 14),
(85, 'KABUPATEN INDRAGIRI HULU', 14),
(86, 'KABUPATEN INDRAGIRI HILIR', 14),
(87, 'KABUPATEN PELALAWAN', 14),
(88, 'KABUPATEN S I A K', 14),
(89, 'KABUPATEN KAMPAR', 14),
(90, 'KABUPATEN ROKAN HULU', 14),
(91, 'KABUPATEN BENGKALIS', 14),
(92, 'KABUPATEN ROKAN HILIR', 14),
(93, 'KABUPATEN KEPULAUAN MERANTI', 14),
(94, 'KOTA PEKANBARU', 14),
(95, 'KOTA D U M A I', 14),
(96, 'KABUPATEN KERINCI', 15),
(97, 'KABUPATEN MERANGIN', 15),
(98, 'KABUPATEN SAROLANGUN', 15),
(99, 'KABUPATEN BATANG HARI', 15),
(100, 'KABUPATEN MUARO JAMBI', 15),
(101, 'KABUPATEN TANJUNG JABUNG TIMUR', 15),
(102, 'KABUPATEN TANJUNG JABUNG BARAT', 15),
(103, 'KABUPATEN TEBO', 15),
(104, 'KABUPATEN BUNGO', 15),
(105, 'KOTA JAMBI', 15),
(106, 'KOTA SUNGAI PENUH', 15),
(107, 'KABUPATEN OGAN KOMERING ULU', 16),
(108, 'KABUPATEN OGAN KOMERING ILIR', 16),
(109, 'KABUPATEN MUARA ENIM', 16),
(110, 'KABUPATEN LAHAT', 16),
(111, 'KABUPATEN MUSI RAWAS', 16),
(112, 'KABUPATEN MUSI BANYUASIN', 16),
(113, 'KABUPATEN BANYU ASIN', 16),
(114, 'KABUPATEN OGAN KOMERING ULU SELATAN', 16),
(115, 'KABUPATEN OGAN KOMERING ULU TIMUR', 16),
(116, 'KABUPATEN OGAN ILIR', 16),
(117, 'KABUPATEN EMPAT LAWANG', 16),
(118, 'KABUPATEN PENUKAL ABAB LEMATANG ILIR', 16),
(119, 'KABUPATEN MUSI RAWAS UTARA', 16),
(120, 'KOTA PALEMBANG', 16),
(121, 'KOTA PRABUMULIH', 16),
(122, 'KOTA PAGAR ALAM', 16),
(123, 'KOTA LUBUKLINGGAU', 16),
(124, 'KABUPATEN BENGKULU SELATAN', 17),
(125, 'KABUPATEN REJANG LEBONG', 17),
(126, 'KABUPATEN BENGKULU UTARA', 17),
(127, 'KABUPATEN KAUR', 17),
(128, 'KABUPATEN SELUMA', 17),
(129, 'KABUPATEN MUKOMUKO', 17),
(130, 'KABUPATEN LEBONG', 17),
(131, 'KABUPATEN KEPAHIANG', 17),
(132, 'KABUPATEN BENGKULU TENGAH', 17),
(133, 'KOTA BENGKULU', 17),
(134, 'KABUPATEN LAMPUNG BARAT', 18),
(135, 'KABUPATEN TANGGAMUS', 18),
(136, 'KABUPATEN LAMPUNG SELATAN', 18),
(137, 'KABUPATEN LAMPUNG TIMUR', 18),
(138, 'KABUPATEN LAMPUNG TENGAH', 18),
(139, 'KABUPATEN LAMPUNG UTARA', 18),
(140, 'KABUPATEN WAY KANAN', 18),
(141, 'KABUPATEN TULANGBAWANG', 18),
(142, 'KABUPATEN PESAWARAN', 18),
(143, 'KABUPATEN PRINGSEWU', 18),
(144, 'KABUPATEN MESUJI', 18),
(145, 'KABUPATEN TULANG BAWANG BARAT', 18),
(146, 'KABUPATEN PESISIR BARAT', 18),
(147, 'KOTA BANDAR LAMPUNG', 18),
(148, 'KOTA METRO', 18),
(149, 'KABUPATEN BANGKA', 19),
(150, 'KABUPATEN BELITUNG', 19),
(151, 'KABUPATEN BANGKA BARAT', 19),
(152, 'KABUPATEN BANGKA TENGAH', 19),
(153, 'KABUPATEN BANGKA SELATAN', 19),
(154, 'KABUPATEN BELITUNG TIMUR', 19),
(155, 'KOTA PANGKAL PINANG', 19),
(156, 'KABUPATEN KARIMUN', 21),
(157, 'KABUPATEN BINTAN', 21),
(158, 'KABUPATEN NATUNA', 21),
(159, 'KABUPATEN LINGGA', 21),
(160, 'KABUPATEN KEPULAUAN ANAMBAS', 21),
(161, 'KOTA B A T A M', 21),
(162, 'KOTA TANJUNG PINANG', 21),
(163, 'KABUPATEN KEPULAUAN SERIBU', 31),
(164, 'KOTA JAKARTA SELATAN', 31),
(165, 'KOTA JAKARTA TIMUR', 31),
(166, 'KOTA JAKARTA PUSAT', 31),
(167, 'KOTA JAKARTA BARAT', 31),
(168, 'KOTA JAKARTA UTARA', 31),
(169, 'KABUPATEN BOGOR', 32),
(170, 'KABUPATEN SUKABUMI', 32),
(171, 'KABUPATEN CIANJUR', 32),
(172, 'KABUPATEN BANDUNG', 32),
(173, 'KABUPATEN GARUT', 32),
(174, 'KABUPATEN TASIKMALAYA', 32),
(175, 'KABUPATEN CIAMIS', 32),
(176, 'KABUPATEN KUNINGAN', 32),
(177, 'KABUPATEN CIREBON', 32),
(178, 'KABUPATEN MAJALENGKA', 32),
(179, 'KABUPATEN SUMEDANG', 32),
(180, 'KABUPATEN INDRAMAYU', 32),
(181, 'KABUPATEN SUBANG', 32),
(182, 'KABUPATEN PURWAKARTA', 32),
(183, 'KABUPATEN KARAWANG', 32),
(184, 'KABUPATEN BEKASI', 32),
(185, 'KABUPATEN BANDUNG BARAT', 32),
(186, 'KABUPATEN PANGANDARAN', 32),
(187, 'KOTA BOGOR', 32),
(188, 'KOTA SUKABUMI', 32),
(189, 'KOTA BANDUNG', 32),
(190, 'KOTA CIREBON', 32),
(191, 'KOTA BEKASI', 32),
(192, 'KOTA DEPOK', 32),
(193, 'KOTA CIMAHI', 32),
(194, 'KOTA TASIKMALAYA', 32),
(195, 'KOTA BANJAR', 32),
(196, 'KABUPATEN CILACAP', 33),
(197, 'KABUPATEN BANYUMAS', 33),
(198, 'KABUPATEN PURBALINGGA', 33),
(199, 'KABUPATEN BANJARNEGARA', 33),
(200, 'KABUPATEN KEBUMEN', 33),
(201, 'KABUPATEN PURWOREJO', 33),
(202, 'KABUPATEN WONOSOBO', 33),
(203, 'KABUPATEN MAGELANG', 33),
(204, 'KABUPATEN BOYOLALI', 33),
(205, 'KABUPATEN KLATEN', 33),
(206, 'KABUPATEN SUKOHARJO', 33),
(207, 'KABUPATEN WONOGIRI', 33),
(208, 'KABUPATEN KARANGANYAR', 33),
(209, 'KABUPATEN SRAGEN', 33),
(210, 'KABUPATEN GROBOGAN', 33),
(211, 'KABUPATEN BLORA', 33),
(212, 'KABUPATEN REMBANG', 33),
(213, 'KABUPATEN PATI', 33),
(214, 'KABUPATEN KUDUS', 33),
(215, 'KABUPATEN JEPARA', 33),
(216, 'KABUPATEN DEMAK', 33),
(217, 'KABUPATEN SEMARANG', 33),
(218, 'KABUPATEN TEMANGGUNG', 33),
(219, 'KABUPATEN KENDAL', 33),
(220, 'KABUPATEN BATANG', 33),
(221, 'KABUPATEN PEKALONGAN', 33),
(222, 'KABUPATEN PEMALANG', 33),
(223, 'KABUPATEN TEGAL', 33),
(224, 'KABUPATEN BREBES', 33),
(225, 'KOTA MAGELANG', 33),
(226, 'KOTA SURAKARTA', 33),
(227, 'KOTA SALATIGA', 33),
(228, 'KOTA SEMARANG', 33),
(229, 'KOTA PEKALONGAN', 33),
(230, 'KOTA TEGAL', 33),
(231, 'KABUPATEN KULON PROGO', 34),
(232, 'KABUPATEN BANTUL', 34),
(233, 'KABUPATEN GUNUNG KIDUL', 34),
(234, 'KABUPATEN SLEMAN', 34),
(235, 'KOTA YOGYAKARTA', 34),
(236, 'KABUPATEN PACITAN', 35),
(237, 'KABUPATEN PONOROGO', 35),
(238, 'KABUPATEN TRENGGALEK', 35),
(239, 'KABUPATEN TULUNGAGUNG', 35),
(240, 'KABUPATEN BLITAR', 35),
(241, 'KABUPATEN KEDIRI', 35),
(242, 'KABUPATEN MALANG', 35),
(243, 'KABUPATEN LUMAJANG', 35),
(244, 'KABUPATEN JEMBER', 35),
(245, 'KABUPATEN BANYUWANGI', 35),
(246, 'KABUPATEN BONDOWOSO', 35),
(247, 'KABUPATEN SITUBONDO', 35),
(248, 'KABUPATEN PROBOLINGGO', 35),
(249, 'KABUPATEN PASURUAN', 35),
(250, 'KABUPATEN SIDOARJO', 35),
(251, 'KABUPATEN MOJOKERTO', 35),
(252, 'KABUPATEN JOMBANG', 35),
(253, 'KABUPATEN NGANJUK', 35),
(254, 'KABUPATEN MADIUN', 35),
(255, 'KABUPATEN MAGETAN', 35),
(256, 'KABUPATEN NGAWI', 35),
(257, 'KABUPATEN BOJONEGORO', 35),
(258, 'KABUPATEN TUBAN', 35),
(259, 'KABUPATEN LAMONGAN', 35),
(260, 'KABUPATEN GRESIK', 35),
(261, 'KABUPATEN BANGKALAN', 35),
(262, 'KABUPATEN SAMPANG', 35),
(263, 'KABUPATEN PAMEKASAN', 35),
(264, 'KABUPATEN SUMENEP', 35),
(265, 'KOTA KEDIRI', 35),
(266, 'KOTA BLITAR', 35),
(267, 'KOTA MALANG', 35),
(268, 'KOTA PROBOLINGGO', 35),
(269, 'KOTA PASURUAN', 35),
(270, 'KOTA MOJOKERTO', 35),
(271, 'KOTA MADIUN', 35),
(272, 'KOTA SURABAYA', 35),
(273, 'KOTA BATU', 35),
(274, 'KABUPATEN PANDEGLANG', 36),
(275, 'KABUPATEN LEBAK', 36),
(276, 'KABUPATEN TANGERANG', 36),
(277, 'KABUPATEN SERANG', 36),
(278, 'KOTA TANGERANG', 36),
(279, 'KOTA CILEGON', 36),
(280, 'KOTA SERANG', 36),
(281, 'KOTA TANGERANG SELATAN', 36),
(282, 'KABUPATEN JEMBRANA', 51),
(283, 'KABUPATEN TABANAN', 51),
(284, 'KABUPATEN BADUNG', 51),
(285, 'KABUPATEN GIANYAR', 51),
(286, 'KABUPATEN KLUNGKUNG', 51),
(287, 'KABUPATEN BANGLI', 51),
(288, 'KABUPATEN KARANG ASEM', 51),
(289, 'KABUPATEN BULELENG', 51),
(290, 'KOTA DENPASAR', 51),
(291, 'KABUPATEN LOMBOK BARAT', 52),
(292, 'KABUPATEN LOMBOK TENGAH', 52),
(293, 'KABUPATEN LOMBOK TIMUR', 52),
(294, 'KABUPATEN SUMBAWA', 52),
(295, 'KABUPATEN DOMPU', 52),
(296, 'KABUPATEN BIMA', 52),
(297, 'KABUPATEN SUMBAWA BARAT', 52),
(298, 'KABUPATEN LOMBOK UTARA', 52),
(299, 'KOTA MATARAM', 52),
(300, 'KOTA BIMA', 52),
(301, 'KABUPATEN SUMBA BARAT', 53),
(302, 'KABUPATEN SUMBA TIMUR', 53),
(303, 'KABUPATEN KUPANG', 53),
(304, 'KABUPATEN TIMOR TENGAH SELATAN', 53),
(305, 'KABUPATEN TIMOR TENGAH UTARA', 53),
(306, 'KABUPATEN BELU', 53),
(307, 'KABUPATEN ALOR', 53),
(308, 'KABUPATEN LEMBATA', 53),
(309, 'KABUPATEN FLORES TIMUR', 53),
(310, 'KABUPATEN SIKKA', 53),
(311, 'KABUPATEN ENDE', 53),
(312, 'KABUPATEN NGADA', 53),
(313, 'KABUPATEN MANGGARAI', 53),
(314, 'KABUPATEN ROTE NDAO', 53),
(315, 'KABUPATEN MANGGARAI BARAT', 53),
(316, 'KABUPATEN SUMBA TENGAH', 53),
(317, 'KABUPATEN SUMBA BARAT DAYA', 53),
(318, 'KABUPATEN NAGEKEO', 53),
(319, 'KABUPATEN MANGGARAI TIMUR', 53),
(320, 'KABUPATEN SABU RAIJUA', 53),
(321, 'KABUPATEN MALAKA', 53),
(322, 'KOTA KUPANG', 53),
(323, 'KABUPATEN SAMBAS', 61),
(324, 'KABUPATEN BENGKAYANG', 61),
(325, 'KABUPATEN LANDAK', 61),
(326, 'KABUPATEN MEMPAWAH', 61),
(327, 'KABUPATEN SANGGAU', 61),
(328, 'KABUPATEN KETAPANG', 61),
(329, 'KABUPATEN SINTANG', 61),
(330, 'KABUPATEN KAPUAS HULU', 61),
(331, 'KABUPATEN SEKADAU', 61),
(332, 'KABUPATEN MELAWI', 61),
(333, 'KABUPATEN KAYONG UTARA', 61),
(334, 'KABUPATEN KUBU RAYA', 61),
(335, 'KOTA PONTIANAK', 61),
(336, 'KOTA SINGKAWANG', 61),
(337, 'KABUPATEN KOTAWARINGIN BARAT', 62),
(338, 'KABUPATEN KOTAWARINGIN TIMUR', 62),
(339, 'KABUPATEN KAPUAS', 62),
(340, 'KABUPATEN BARITO SELATAN', 62),
(341, 'KABUPATEN BARITO UTARA', 62),
(342, 'KABUPATEN SUKAMARA', 62),
(343, 'KABUPATEN LAMANDAU', 62),
(344, 'KABUPATEN SERUYAN', 62),
(345, 'KABUPATEN KATINGAN', 62),
(346, 'KABUPATEN PULANG PISAU', 62),
(347, 'KABUPATEN GUNUNG MAS', 62),
(348, 'KABUPATEN BARITO TIMUR', 62),
(349, 'KABUPATEN MURUNG RAYA', 62),
(350, 'KOTA PALANGKA RAYA', 62),
(351, 'KABUPATEN TANAH LAUT', 63),
(352, 'KABUPATEN KOTA BARU', 63),
(353, 'KABUPATEN BANJAR', 63),
(354, 'KABUPATEN BARITO KUALA', 63),
(355, 'KABUPATEN TAPIN', 63),
(356, 'KABUPATEN HULU SUNGAI SELATAN', 63),
(357, 'KABUPATEN HULU SUNGAI TENGAH', 63),
(358, 'KABUPATEN HULU SUNGAI UTARA', 63),
(359, 'KABUPATEN TABALONG', 63),
(360, 'KABUPATEN TANAH BUMBU', 63),
(361, 'KABUPATEN BALANGAN', 63),
(362, 'KOTA BANJARMASIN', 63),
(363, 'KOTA BANJAR BARU', 63),
(364, 'KABUPATEN PASER', 64),
(365, 'KABUPATEN KUTAI BARAT', 64),
(366, 'KABUPATEN KUTAI KARTANEGARA', 64),
(367, 'KABUPATEN KUTAI TIMUR', 64),
(368, 'KABUPATEN BERAU', 64),
(369, 'KABUPATEN PENAJAM PASER UTARA', 64),
(370, 'KABUPATEN MAHAKAM HULU', 64),
(371, 'KOTA BALIKPAPAN', 64),
(372, 'KOTA SAMARINDA', 64),
(373, 'KOTA BONTANG', 64),
(374, 'KABUPATEN MALINAU', 65),
(375, 'KABUPATEN BULUNGAN', 65),
(376, 'KABUPATEN TANA TIDUNG', 65),
(377, 'KABUPATEN NUNUKAN', 65),
(378, 'KOTA TARAKAN', 65),
(379, 'KABUPATEN BOLAANG MONGONDOW', 71),
(380, 'KABUPATEN MINAHASA', 71),
(381, 'KABUPATEN KEPULAUAN SANGIHE', 71),
(382, 'KABUPATEN KEPULAUAN TALAUD', 71),
(383, 'KABUPATEN MINAHASA SELATAN', 71),
(384, 'KABUPATEN MINAHASA UTARA', 71),
(385, 'KABUPATEN BOLAANG MONGONDOW UTARA', 71),
(386, 'KABUPATEN SIAU TAGULANDANG BIARO', 71),
(387, 'KABUPATEN MINAHASA TENGGARA', 71),
(388, 'KABUPATEN BOLAANG MONGONDOW SELATAN', 71),
(389, 'KABUPATEN BOLAANG MONGONDOW TIMUR', 71),
(390, 'KOTA MANADO', 71),
(391, 'KOTA BITUNG', 71),
(392, 'KOTA TOMOHON', 71),
(393, 'KOTA KOTAMOBAGU', 71),
(394, 'KABUPATEN BANGGAI KEPULAUAN', 72),
(395, 'KABUPATEN BANGGAI', 72),
(396, 'KABUPATEN MOROWALI', 72),
(397, 'KABUPATEN POSO', 72),
(398, 'KABUPATEN DONGGALA', 72),
(399, 'KABUPATEN TOLI-TOLI', 72),
(400, 'KABUPATEN BUOL', 72),
(401, 'KABUPATEN PARIGI MOUTONG', 72),
(402, 'KABUPATEN TOJO UNA-UNA', 72),
(403, 'KABUPATEN SIGI', 72),
(404, 'KABUPATEN BANGGAI LAUT', 72),
(405, 'KABUPATEN MOROWALI UTARA', 72),
(406, 'KOTA PALU', 72),
(407, 'KABUPATEN KEPULAUAN SELAYAR', 73),
(408, 'KABUPATEN BULUKUMBA', 73),
(409, 'KABUPATEN BANTAENG', 73),
(410, 'KABUPATEN JENEPONTO', 73),
(411, 'KABUPATEN TAKALAR', 73),
(412, 'KABUPATEN GOWA', 73),
(413, 'KABUPATEN SINJAI', 73),
(414, 'KABUPATEN MAROS', 73),
(415, 'KABUPATEN PANGKAJENE DAN KEPULAUAN', 73),
(416, 'KABUPATEN BARRU', 73),
(417, 'KABUPATEN BONE', 73),
(418, 'KABUPATEN SOPPENG', 73),
(419, 'KABUPATEN WAJO', 73),
(420, 'KABUPATEN SIDENRENG RAPPANG', 73),
(421, 'KABUPATEN PINRANG', 73),
(422, 'KABUPATEN ENREKANG', 73),
(423, 'KABUPATEN LUWU', 73),
(424, 'KABUPATEN TANA TORAJA', 73),
(425, 'KABUPATEN LUWU UTARA', 73),
(426, 'KABUPATEN LUWU TIMUR', 73),
(427, 'KABUPATEN TORAJA UTARA', 73),
(428, 'KOTA MAKASSAR', 73),
(429, 'KOTA PAREPARE', 73),
(430, 'KOTA PALOPO', 73),
(431, 'KABUPATEN BUTON', 74),
(432, 'KABUPATEN MUNA', 74),
(433, 'KABUPATEN KONAWE', 74),
(434, 'KABUPATEN KOLAKA', 74),
(435, 'KABUPATEN KONAWE SELATAN', 74),
(436, 'KABUPATEN BOMBANA', 74),
(437, 'KABUPATEN WAKATOBI', 74),
(438, 'KABUPATEN KOLAKA UTARA', 74),
(439, 'KABUPATEN BUTON UTARA', 74),
(440, 'KABUPATEN KONAWE UTARA', 74),
(441, 'KABUPATEN KOLAKA TIMUR', 74),
(442, 'KABUPATEN KONAWE KEPULAUAN', 74),
(443, 'KABUPATEN MUNA BARAT', 74),
(444, 'KABUPATEN BUTON TENGAH', 74),
(445, 'KABUPATEN BUTON SELATAN', 74),
(446, 'KOTA KENDARI', 74),
(447, 'KOTA BAUBAU', 74),
(448, 'KABUPATEN BOALEMO', 75),
(449, 'KABUPATEN GORONTALO', 75),
(450, 'KABUPATEN POHUWATO', 75),
(451, 'KABUPATEN BONE BOLANGO', 75),
(452, 'KABUPATEN GORONTALO UTARA', 75),
(453, 'KOTA GORONTALO', 75),
(454, 'KABUPATEN MAJENE', 76),
(455, 'KABUPATEN POLEWALI MANDAR', 76),
(456, 'KABUPATEN MAMASA', 76),
(457, 'KABUPATEN MAMUJU', 76),
(458, 'KABUPATEN MAMUJU UTARA', 76),
(459, 'KABUPATEN MAMUJU TENGAH', 76),
(460, 'KABUPATEN MALUKU TENGGARA BARAT', 81),
(461, 'KABUPATEN MALUKU TENGGARA', 81),
(462, 'KABUPATEN MALUKU TENGAH', 81),
(463, 'KABUPATEN BURU', 81),
(464, 'KABUPATEN KEPULAUAN ARU', 81),
(465, 'KABUPATEN SERAM BAGIAN BARAT', 81),
(466, 'KABUPATEN SERAM BAGIAN TIMUR', 81),
(467, 'KABUPATEN MALUKU BARAT DAYA', 81),
(468, 'KABUPATEN BURU SELATAN', 81),
(469, 'KOTA AMBON', 81),
(470, 'KOTA TUAL', 81),
(471, 'KABUPATEN HALMAHERA BARAT', 82),
(472, 'KABUPATEN HALMAHERA TENGAH', 82),
(473, 'KABUPATEN KEPULAUAN SULA', 82),
(474, 'KABUPATEN HALMAHERA SELATAN', 82),
(475, 'KABUPATEN HALMAHERA UTARA', 82),
(476, 'KABUPATEN HALMAHERA TIMUR', 82),
(477, 'KABUPATEN PULAU MOROTAI', 82),
(478, 'KABUPATEN PULAU TALIABU', 82),
(479, 'KOTA TERNATE', 82),
(480, 'KOTA TIDORE KEPULAUAN', 82),
(481, 'KABUPATEN FAKFAK', 91),
(482, 'KABUPATEN KAIMANA', 91),
(483, 'KABUPATEN TELUK WONDAMA', 91),
(484, 'KABUPATEN TELUK BINTUNI', 91),
(485, 'KABUPATEN MANOKWARI', 91),
(486, 'KABUPATEN SORONG SELATAN', 91),
(487, 'KABUPATEN SORONG', 91),
(488, 'KABUPATEN RAJA AMPAT', 91),
(489, 'KABUPATEN TAMBRAUW', 91),
(490, 'KABUPATEN MAYBRAT', 91),
(491, 'KABUPATEN MANOKWARI SELATAN', 91),
(492, 'KABUPATEN PEGUNUNGAN ARFAK', 91),
(493, 'KOTA SORONG', 91),
(494, 'KABUPATEN MERAUKE', 94),
(495, 'KABUPATEN JAYAWIJAYA', 94),
(496, 'KABUPATEN JAYAPURA', 94),
(497, 'KABUPATEN NABIRE', 94),
(498, 'KABUPATEN KEPULAUAN YAPEN', 94),
(499, 'KABUPATEN BIAK NUMFOR', 94),
(500, 'KABUPATEN PANIAI', 94),
(501, 'KABUPATEN PUNCAK JAYA', 94),
(502, 'KABUPATEN MIMIKA', 94),
(503, 'KABUPATEN BOVEN DIGOEL', 94),
(504, 'KABUPATEN MAPPI', 94),
(505, 'KABUPATEN ASMAT', 94),
(506, 'KABUPATEN YAHUKIMO', 94),
(507, 'KABUPATEN PEGUNUNGAN BINTANG', 94),
(508, 'KABUPATEN TOLIKARA', 94),
(509, 'KABUPATEN SARMI', 94),
(510, 'KABUPATEN KEEROM', 94),
(511, 'KABUPATEN WAROPEN', 94),
(512, 'KABUPATEN SUPIORI', 94),
(513, 'KABUPATEN MAMBERAMO RAYA', 94),
(514, 'KABUPATEN NDUGA', 94),
(515, 'KABUPATEN LANNY JAYA', 94),
(516, 'KABUPATEN MAMBERAMO TENGAH', 94),
(517, 'KABUPATEN YALIMO', 94),
(518, 'KABUPATEN PUNCAK', 94),
(519, 'KABUPATEN DOGIYAI', 94),
(520, 'KABUPATEN INTAN JAYA', 94),
(521, 'KABUPATEN DEIYAI', 94),
(522, 'KOTA JAYAPURA', 94);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `no_faktur` varchar(30) NOT NULL,
  `no_invoice` varchar(30) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `total_qty` int(11) NOT NULL,
  `total_bayar` decimal(11,2) NOT NULL,
  `pajak` decimal(11,2) NOT NULL,
  `grand_total` decimal(11,2) NOT NULL,
  `pembayaran` decimal(11,2) NOT NULL,
  `sisa_bayar` decimal(11,2) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`no_faktur`, `no_invoice`, `id_supplier`, `tanggal_beli`, `total_qty`, `total_bayar`, `pajak`, `grand_total`, `pembayaran`, `sisa_bayar`, `jatuh_tempo`, `id_karyawan`, `date_time`) VALUES
('LPB/03/2020/0001', 'INV-PTI-2020-00001301', 1, '2020-03-11', 35, '20427668.41', '2043203.41', '20427668.41', '0.00', '22470871.82', '2020-03-11', 1, '2020-03-11 13:23:33'),
('LPB/03/2020/0002', 'PTI-20-2020-0000325', 1, '2020-03-12', 85, '31634303.50', '316303.50', '31634303.50', '0.00', '31950607.00', '2020-03-31', 1, '2020-03-13 06:35:31'),
('LPB/03/2020/0003', '0998821', 1, '2020-03-13', 25, '8775000.00', '877500.00', '8775000.00', '9.65', '0.00', '2020-03-13', 1, '2020-03-13 13:08:20'),
('LPB/03/2020/0004', 'INV-PTI-2020-000032', 1, '2020-03-13', 250, '1200000.00', '120000.00', '1200000.00', '320000.00', '1000000.00', '2020-03-31', 1, '2020-03-14 06:08:46'),
('LPB/03/2020/0005', 'INV-PTI-2020-0000741', 1, '2020-03-13', 20, '4503003.00', '450300.00', '4503003.00', '953303.00', '4000000.00', '2020-03-31', 1, '2020-03-14 06:13:58'),
('LPB/03/2020/0006', 'INV-PTI-2020-0000300', 1, '2020-03-14', 45, '6659672.63', '665967.00', '6659672.63', '325639.63', '7000000.00', '2020-03-14', 1, '2020-03-14 07:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_oprasional`
--

CREATE TABLE `pembelian_oprasional` (
  `no_faktur` varchar(20) NOT NULL,
  `nota_beli` varchar(25) NOT NULL,
  `supplier` varchar(30) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `grand_total` int(11) NOT NULL,
  `dibayar` int(11) NOT NULL,
  `sisa_bayar` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_oprasional`
--

INSERT INTO `pembelian_oprasional` (`no_faktur`, `nota_beli`, `supplier`, `tanggal_beli`, `grand_total`, `dibayar`, `sisa_bayar`, `id_karyawan`, `date`) VALUES
('OPS/03/2020/0001', '321110', 'Indomaret', '2020-03-10', 12800000, 12800000, 0, 0, '2020-03-10 12:12:44'),
('OPS/03/2020/0002', '123', 'Indoemaret', '2020-03-12', 90000, 90000, 0, 0, '2020-03-13 06:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_barang`
--

CREATE TABLE `penerimaan_barang` (
  `no_faktur_penerimaan` varchar(50) NOT NULL,
  `tgl_faktur_penerimaan` date NOT NULL,
  `id_suppiler` int(8) NOT NULL,
  `total_harga` int(20) NOT NULL,
  `diskon` int(20) NOT NULL,
  `ppn` int(4) NOT NULL,
  `sub_total` int(20) NOT NULL,
  `pembayaran` int(20) NOT NULL,
  `hutang` int(20) NOT NULL,
  `tgl_tempo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `no_faktur` varchar(25) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `total_bayar` decimal(11,2) NOT NULL,
  `pajak` decimal(11,2) NOT NULL,
  `grand_total` decimal(11,2) NOT NULL,
  `pembayaran` decimal(11,2) NOT NULL,
  `sisa_bayar` decimal(11,2) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`no_faktur`, `id_customer`, `tanggal_beli`, `total_bayar`, `pajak`, `grand_total`, `pembayaran`, `sisa_bayar`, `jatuh_tempo`, `id_karyawan`, `date_time`) VALUES
('ARS/INF/03/2020/0001', 1, '2020-03-13', '1206000.00', '120600.00', '1206000.00', '326600.00', '1000000.00', '2020-03-31', 1, '2020-03-13 15:23:09'),
('ARS/INF/03/2020/0002', 1, '2020-03-20', '1280000.00', '1280000.00', '1280000.00', '2560000.00', '0.00', '2020-03-13', 1, '2020-03-14 03:19:27'),
('ARS/INF/03/2020/0003', 2, '2020-03-13', '1475000.00', '1475000.00', '1475000.00', '2950000.00', '0.00', '2020-03-13', 1, '2020-03-14 03:21:04'),
('ARS/INF/03/2020/0004', 1, '2020-03-13', '45000.00', '4500.00', '45000.00', '49500.00', '0.00', '2020-03-13', 1, '2020-03-14 04:08:19'),
('ARS/INF/03/2020/0005', 1, '2020-03-13', '45000.00', '4500.00', '45000.00', '49500.00', '0.00', '2020-03-13', 1, '2020-03-14 04:09:58'),
('ARS/INF/03/2020/0006', 2, '2020-03-13', '105000.00', '10500.00', '105000.00', '115500.00', '0.00', '2020-03-13', 1, '2020-03-14 04:11:05'),
('ARS/INF/03/2020/0007', 1, '2020-03-13', '700000.00', '70000.00', '700000.00', '770000.00', '0.00', '2020-03-13', 1, '2020-03-14 04:12:04'),
('ARS/INF/03/2020/0008', 2, '2020-03-13', '650000.00', '6500.00', '650000.00', '656500.00', '0.00', '2020-03-13', 1, '2020-03-14 04:13:37'),
('ARS/INF/03/2020/0009', 1, '2020-03-13', '700000.00', '70000.00', '700000.00', '770000.00', '0.00', '2020-03-13', 1, '2020-03-14 04:14:34'),
('ARS/INF/03/2020/0010', 1, '2020-03-13', '45000.00', '4500.00', '45000.00', '49500.00', '0.00', '2020-03-13', 1, '2020-03-14 04:16:08'),
('ARS/INF/03/2020/0011', 2, '2020-03-13', '70000.00', '7000.00', '70000.00', '77000.00', '0.00', '0000-00-00', 1, '2020-03-14 04:24:40'),
('ARS/INF/03/2020/0012', 2, '2020-03-06', '650000.00', '65000.00', '650000.00', '715000.00', '0.00', '0000-00-00', 1, '2020-03-14 04:26:09'),
('ARS/INF/03/2020/0013', 1, '2020-03-13', '700000.00', '70000.00', '700000.00', '770000.00', '0.00', '0000-00-00', 1, '2020-03-14 04:51:18'),
('ARS/INF/03/2020/0014', 1, '2020-03-13', '700000.00', '70000.00', '700000.00', '770000.00', '0.00', '0000-00-00', 1, '2020-03-14 04:53:01'),
('ARS/INF/03/2020/0015', 2, '2020-03-14', '720000.00', '72000.00', '720000.00', '792000.00', '0.00', '2020-03-14', 1, '2020-03-14 07:40:43');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_barang`
--

CREATE TABLE `penjualan_barang` (
  `no_faktur_pembelian` int(11) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `id_customer` int(8) NOT NULL,
  `total` int(20) NOT NULL,
  `ppn_1` int(20) NOT NULL,
  `ppn_2` int(20) NOT NULL,
  `diskon` int(20) NOT NULL,
  `total_pembayaran` int(20) NOT NULL,
  `hutang` int(20) NOT NULL,
  `pembayaran` int(20) NOT NULL,
  `tgl_tempo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(2) NOT NULL,
  `provinsi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `provinsi`) VALUES
(11, 'ACEH'),
(12, 'SUMATERA UTARA'),
(13, 'SUMATERA BARAT'),
(14, 'RIAU'),
(15, 'JAMBI'),
(16, 'SUMATERA SELATAN'),
(17, 'BENGKULU'),
(18, 'LAMPUNG'),
(19, 'KEPULAUAN BANGKA BELITUNG'),
(21, 'KEPULAUAN RIAU'),
(31, 'DKI JAKARTA'),
(32, 'JAWA BARAT'),
(33, 'JAWA TENGAH'),
(34, 'DI YOGYAKARTA'),
(35, 'JAWA TIMUR'),
(36, 'BANTEN'),
(51, 'BALI'),
(52, 'NUSA TENGGARA BARAT'),
(53, 'NUSA TENGGARA TIMUR'),
(61, 'KALIMANTAN BARAT'),
(62, 'KALIMANTAN TENGAH'),
(63, 'KALIMANTAN SELATAN'),
(64, 'KALIMANTAN TIMUR'),
(65, 'KALIMANTAN UTARA'),
(71, 'SULAWESI UTARA'),
(72, 'SULAWESI TENGAH'),
(73, 'SULAWESI SELATAN'),
(74, 'SULAWESI TENGGARA'),
(75, 'GORONTALO'),
(76, 'SULAWESI BARAT'),
(81, 'MALUKU'),
(82, 'MALUKU UTARA'),
(91, 'PAPUA BARAT'),
(94, 'PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `satuan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `satuan`) VALUES
(1, 'Pcs'),
(2, 'Roll'),
(4, 'Meter'),
(5, 'Box');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `telpon` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nama`, `alamat`, `telpon`, `email`) VALUES
(1, 'PT. Pasifik Teknologi Indonesia', 'Jl. Pangeran Jayakarta', '0216905333', 'care@pti.id');

-- --------------------------------------------------------

--
-- Table structure for table `user_app`
--

CREATE TABLE `user_app` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_app`
--

INSERT INTO `user_app` (`id`, `id_karyawan`, `status`, `username`, `password`) VALUES
(1, 1, 'AKTIF', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akomodasi`
--
ALTER TABLE `akomodasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `conversi_satuan`
--
ALTER TABLE `conversi_satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversi_satuan_harga`
--
ALTER TABLE `conversi_satuan_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `daftar_harga`
--
ALTER TABLE `daftar_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pembelian_oprasional`
--
ALTER TABLE `detail_pembelian_oprasional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grup_barang`
--
ALTER TABLE `grup_barang`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jasa_expedisi`
--
ALTER TABLE `jasa_expedisi`
  ADD PRIMARY KEY (`no_faktur`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id_kabupaten`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_provinsi` (`id_provinsi`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`no_faktur`);

--
-- Indexes for table `pembelian_oprasional`
--
ALTER TABLE `pembelian_oprasional`
  ADD PRIMARY KEY (`no_faktur`);

--
-- Indexes for table `penerimaan_barang`
--
ALTER TABLE `penerimaan_barang`
  ADD PRIMARY KEY (`no_faktur_penerimaan`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`no_faktur`);

--
-- Indexes for table `penjualan_barang`
--
ALTER TABLE `penjualan_barang`
  ADD PRIMARY KEY (`no_faktur_pembelian`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_app`
--
ALTER TABLE `user_app`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akomodasi`
--
ALTER TABLE `akomodasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `conversi_satuan`
--
ALTER TABLE `conversi_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `conversi_satuan_harga`
--
ALTER TABLE `conversi_satuan_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `daftar_harga`
--
ALTER TABLE `daftar_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `detail_pembelian_oprasional`
--
ALTER TABLE `detail_pembelian_oprasional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_app`
--
ALTER TABLE `user_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  ADD CONSTRAINT `kotakabupaten_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id_provinsi`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
